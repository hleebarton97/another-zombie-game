<div align="center">
  ![Pipeline Status](https://gitlab.com/hleebarton97/another-zombie-game/badges/master/pipeline.svg)

  # Another Zombie Game
  <img src="./images/another_zombie_game_logo_black.png" />

  ## Developed by Another Indie Studio
  <img src="./images/another_indie_studio_logo_black.png" />

</div>