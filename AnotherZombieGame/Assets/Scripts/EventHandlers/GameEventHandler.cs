using System;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class GameEventHandler {

    // Game<Type>Event
    public static event Action<int> GameNewRoundEvent;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Handle new round event.
    public static void NewRound (int round) {
        GameNewRoundEvent?.Invoke(round);
    }

}
