using System;
using UnityEngine;

public class PlayerEventHandler : MonoBehaviour {

    public static event Action PlayerDeathEvent;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Handle player death event.
    public static void PlayerDeath () {
        PlayerDeathEvent?.Invoke ();
    }
}
