using System;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class PerkAbilityEventHandler {

    // PerkAbility<Type><Action>Event
    public static event Action<Transform> PerkAbilityCamoActivatedEvent;
    public static event Action<Transform> PerkAbilityCamoDeactivatedEvent;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Handle camo perk ability event.
    public static void PerkAbilityCamoActivated (Transform baitTransform) {
        PerkAbilityCamoActivatedEvent?.Invoke(baitTransform);
    }

    // Handle camo perk ability end event.
    public static void PerkAbilityCamoDeactivated (Transform baitTransform) {
        PerkAbilityCamoDeactivatedEvent?.Invoke(baitTransform);
    }
}
