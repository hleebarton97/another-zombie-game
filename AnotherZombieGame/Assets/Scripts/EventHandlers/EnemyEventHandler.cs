using System;
using UnityEngine;

// Lee Barton
public class EnemyEventHandler : MonoBehaviour {

    // Enemy<Event>
    public static event Action<GameObject> EnemyZombieDeathEvent;
    public static event Action<GameObject> EnemyBossDeathEvent;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Enemy zombie has died.
    public static void EnemyZombieDeath (GameObject enemy) {
        EnemyZombieDeathEvent?.Invoke(enemy);
    }

    // Enemy boss has died.
    public static void EnemyBossDeath (GameObject enemy) {
        EnemyBossDeathEvent?.Invoke(enemy);
    }

}
