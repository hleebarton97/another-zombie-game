﻿using System;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class AnimationEventHandler : MonoBehaviour {

    // Animation<Entity><Action><Event>

    public static event Action<AnimationEventHandler> AnimationPlayerThrowingThrow;
    public static event Action<AnimationEventHandler> AnimationPlayerThrowingEnd;

    public static event Action<AnimationEventHandler> AnimationEnemyAttackingEnd;
    public static event Action<AnimationEventHandler> AnimationEnemyAttackingHit;

    public static event Action AnimationZombieBossStompAttackEnd;
    public static event Action AnimationZombieBossStompAttackHit;

    public static event Action AnimationZombieBossSpawnEnd;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Handle player's throwing animation when object should be thrown.
    public void ThrowingAnimationThrow () {
        AnimationPlayerThrowingThrow?.Invoke(this);
    }

    // Handle end of throwing animation logic.
    public void ThrowingAnimationEnd () {
        AnimationPlayerThrowingEnd?.Invoke(this);
    }

    // Handle end of enemy attacking animation logic.
    public void EnemyAttackingAnimationEnd () {
        AnimationEnemyAttackingEnd?.Invoke(this);
    }

    // Handle hit of enemy attacking animation logic.
    public void EnemyAttackingAnimationHit () {
        AnimationEnemyAttackingHit?.Invoke(this);
    }

    // Handle end of zombie boss stomp attack animation.
    public void ZombieBossStompAttackEnd () {
        AnimationZombieBossStompAttackEnd?.Invoke();
    }

    // Handle foot ground contact on stomp.
    public void ZombieBossStompAttackHit () {
        AnimationZombieBossStompAttackHit?.Invoke();
    }

    // Handle spawn climb up animation end completion.
    public void ZombieBossSpawnAnimationEnd () {
        AnimationZombieBossSpawnEnd?.Invoke();
    }
}
