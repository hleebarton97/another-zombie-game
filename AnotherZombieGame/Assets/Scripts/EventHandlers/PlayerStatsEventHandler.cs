﻿using System;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class PlayerStatsEventHandler : MonoBehaviour {

    // PlayerStats<Stat><Event>

    public static event Action<int> PlayerStatsBrainsUpdate;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Handle player's brain stat update.
    public static void BrainsStatUpdated (int total) {
        PlayerStatsBrainsUpdate?.Invoke(total);
    }
}
