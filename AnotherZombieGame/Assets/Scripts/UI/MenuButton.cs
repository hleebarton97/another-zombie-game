using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

// Novasloth Games LLC
// Lee Barton
public class MenuButton : NovaButton {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool IsEnabled { get; private set; } = true;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Image showHideImage;
    [SerializeField] private Color defaultColor = Color.white;
    [SerializeField] private Color clickedColor = Color.white;
    [SerializeField] private Color disabledColor;

    private Button button;
    private TextMeshProUGUI menuButtonTMP;

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        menuButtonTMP = GetComponentInChildren<TextMeshProUGUI>();
        button = GetComponent<Button>();
    }

    protected override void Init () {
        showHideImage.enabled = false;
    }

    public void Disable () {
        IsEnabled = false;
        button.interactable = false;
        menuButtonTMP.color = disabledColor;
    }

    public void Enable () {
        IsEnabled = true;
        button.interactable = true;
        menuButtonTMP.color = Color.white;
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public override void OnPointerEnter (PointerEventData eventData) {
        if (IsEnabled) {
            showHideImage.enabled = true;
        }
    }

    public override void OnPointerExit (PointerEventData eventData) {
        if (IsEnabled) {
            SetColor(defaultColor);
            clicked = false;
            showHideImage.enabled = false;
        }
    }

    public override void OnPointerClick (PointerEventData eventData) {
        if (IsEnabled) {
            clicked = !clicked;
            SetColor(clicked ? clickedColor : defaultColor);
            showHideImage.enabled = true;
        }
    }

    private void SetColor (Color color) {
        showHideImage.color = color;
    }
}
