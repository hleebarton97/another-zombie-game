using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


// Novasloth Games LLC
// Lee Barton
public class CharacterSelectionMenu : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool CharacterIsSelected { get { return !Util.IsNull(currentSelectedButton); } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject startButton;
    private MenuButton _MenuButtonStart;

    [SerializeField] private TextMeshProUGUI descriptionTMP;
    [SerializeField] private TextMeshProUGUI perkDescriptionTMP;
    private CharacterSelectButton currentSelectedButton = null;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        SetupReferences();
        Init();
    }

    private void Start () {
        _MenuButtonStart?.Disable();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        _MenuButtonStart = startButton.GetComponent<MenuButton>();
    }

    private void Init () {}

    public void OnCharacterSelectButtonClicked (CharacterSelectButton selectButton, bool clicked, string description, string perkDescription) {
        if (currentSelectedButton == selectButton) {
            currentSelectedButton = null;
            SetCharacterDescriptionText("", "");
            _MenuButtonStart?.Disable();
            return;
        }

        if (!Util.IsNull(currentSelectedButton)) {
            currentSelectedButton.DeselectButton();
        }

        currentSelectedButton = selectButton;
        SetCharacterDescriptionText(description, perkDescription);
        _MenuButtonStart?.Enable();
    }

    public void SetCharacterDescriptionText (string desc, string perkDesc) {
        if (string.IsNullOrEmpty(desc) && CharacterIsSelected) return;

        descriptionTMP.text = desc;
        perkDescriptionTMP.text = perkDesc;
    }

    // @TODO handle "DontDestroyOnLoad" Player prefab
    public void OnStartButtonClick () {
        if (CharacterIsSelected) {
            GameObject playerObject = Instantiate(currentSelectedButton.GetSelectedPlayerGameObject());
            DontDestroyOnLoad(playerObject);
            SceneHandler.Load(SceneHandler.Scene.DEV);
        }
    }

    public void OnBackButtonClick () {
        SceneHandler.Load(SceneHandler.Scene.START_MENU);
    }
}
