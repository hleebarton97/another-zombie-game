using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// Novasloth Games LLC
// Lee Barton
public abstract class NovaButton : MonoBehaviour,
    IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    protected bool clicked = false;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void Awake () {
        SetupReferences();
        Init();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected abstract void SetupReferences ();
    protected abstract void Init ();

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public abstract void OnPointerEnter (PointerEventData eventData);

    public abstract void OnPointerExit (PointerEventData eventData);

    public abstract void OnPointerClick (PointerEventData eventData);
}
