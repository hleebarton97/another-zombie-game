﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Novasloth;

// Novasloth Games LLC
// Lee Barton
// @TODO Reorganize methods with their specific HUD
public class HUD : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private const string TEXT_INFINITE_AMMO = "INFINITE";

    private const float HALF_HEALTH = 0.50f;
    private const float CRITICAL_HEALTH = 0.25f;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Sprite[] weaponImages;

    [Header("Menus")]
    [SerializeField] private GameObject selectedWeaponHUD;
    // [SerializeField] private GameObject purchasingMenu;

    [Header("Round")]
    [SerializeField] private GameObject roundHUD;
    [SerializeField] private TextMeshProUGUI roundText;
    private Animator roundAnimator;

    [Header("Stats")]
    [SerializeField] private GameObject brainsHUD;
    [SerializeField] private TextMeshProUGUI brainsText;

    [Header("Selected Weapon")]
    [SerializeField] private Image weaponImage;
    [SerializeField] private GameObject grenadeHUD;
    [SerializeField] private TextMeshProUGUI ammoText;
    [SerializeField] private TextMeshProUGUI grenadeAmmoText;

    [Header("Damage")]
    [SerializeField] private Image hitImage;
    [SerializeField] private Image hitCriticalImage;
    [SerializeField] private Color hitColor = new Color(1.0f, 1.0f, 1.0f, 0.1f);
    [SerializeField] private Color hitCriticalColor = new Color(1.0f, 1.0f, 1.0f, 0.25f);
    [SerializeField] private float hitImageFadeTime = 5.0f;
    private bool fadeFlashHit = true;

    [Header("Game Over")]
    [SerializeField] private GameObject gameOverUI;
    [SerializeField] private Image gameOverPanel;
    [SerializeField] private Color gameOverPanelStartAlpha;
    [SerializeField] private Color gameOverPanelEndAlpha;
    [SerializeField] private float gameOverUIDelay = 1.5f;
    [SerializeField] private float gameOverUIFadeInSpeed = 0.25f;
    private Timer gameOverFadeTimer;
    private bool startGameOverFade = false;

    [Header("Game Over Stats")]
    [SerializeField] private TextMeshProUGUI brainsStatText;
    [SerializeField] private TextMeshProUGUI killsStatText;
    [SerializeField] private TextMeshProUGUI bossKillsStatText;


    [SerializeField] private bool debug = false;

    private WeaponCursor _WeaponCursor;
    private WeaponsPurchasing _WeaponsPurchasing;
    private PlayerStats _PlayerStats;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.SetupReferences();
        this.Init();
        this.RegisterEventHandlers();
    }

    private void Update () {
        this.FadeHitFlash();
        this.gameOverFadeTimer.Tick();
    }

    private void LateUpdate () {
        if (this.startGameOverFade) {
            this.FadeInGameOverPanel();
        }
    }

    private void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.brainsText.text = "0";

        this.gameOverPanel.color = this.gameOverPanelStartAlpha;
        this.gameOverUI.SetActive(false);
        this.gameOverFadeTimer = new Timer(this.gameOverUIDelay, () => {
            if (this.debug) { Debug.Log("HUD :: Timer(() => Time to start fading...) "); }
            this.startGameOverFade = true;
            this.gameOverUI.SetActive(true);
            this.gameOverFadeTimer.Stop();
        });
    }

    private void SetupReferences () {
        // @PPI - Utilize 'GetComponentInParent' instead of searching via Tag
        GameObject player = Util.GetPlayerByTag();
        this._WeaponCursor = player.GetComponentInChildren<WeaponCursor>();
        this._WeaponsPurchasing = player.GetComponentInChildren<WeaponsPurchasing>();
        this._PlayerStats = player.GetComponent<PlayerStats>();
        this.roundAnimator = this.roundHUD.GetComponent<Animator>();
    }

    private void ShowGrenadeHUD () {
        this.grenadeHUD.SetActive(true);
    }

    /////////////////////////////////////////////////////////////////
    // D A M A G E   H U D   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void ShowDamageFlash (float percentHealth) {
        if (percentHealth <= HALF_HEALTH) {
            this.hitImage.color = this.hitCriticalColor;
            this.fadeFlashHit = false;
        } else {
            this.hitImage.color = this.hitColor;
            this.fadeFlashHit = true;
        }

        if (percentHealth <= CRITICAL_HEALTH) {
            this.hitCriticalImage.color = this.hitCriticalColor;
        }
    }

    public void ResetDamageFlash (float percentHealth) {
        if (!fadeFlashHit && percentHealth > HALF_HEALTH) {
            this.hitImage.color = this.hitColor;
            this.fadeFlashHit = true;
        }
    }

    private void FadeHitFlash () {
        if (fadeFlashHit) {
            this.hitImage.color = Color.Lerp(
                this.hitImage.color,
                Color.clear,
                this.hitImageFadeTime * Time.deltaTime
            );
        }

        this.hitCriticalImage.color = Color.Lerp(
            this.hitCriticalImage.color,
            Color.clear,
            this.hitImageFadeTime * Time.deltaTime
        );
    }

    /////////////////////////////////////////////////////////////////
    // R O U N D   H U D   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void ShowRoundHUD () {
        this.roundHUD.SetActive(true);
    }

    public void NewRound (int round) {
        this.roundText.text = round.ToString();
        this.ShowRoundHUD();
        this.roundAnimator.SetTrigger(MyAnimation.NEW_ROUND);
    }

    /*private void ListenForScrollWheel () {
        float delta = Input.GetAxis("Mouse ScrollWheel");
        if (delta > 0f) {
            this.DecrementViewWeaponIndex();
        } else if (delta < 0f ) {
            this.IncrementViewWeaponIndex();
        }
    }*/

    /////////////////////////////////////////////////////////////////
    // G A M E   O V E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void StartGameOver () {
        if (this.debug) { Debug.Log("INFO :: HUD :: StartGameOver()"); }
        this.gameOverFadeTimer.Start();
    }

    // @NTS left off here!
    private void FadeInGameOverPanel () {
        if (this.gameOverPanel.color.a < this.gameOverPanelEndAlpha.a) {
            if (this.debug) { Debug.Log("INFO :: HUD :: FadeInGameOverPanel()"); }
            this.gameOverPanelStartAlpha.a += this.gameOverUIFadeInSpeed * Time.deltaTime;
            this.gameOverPanel.color = this.gameOverPanelStartAlpha;
            return;
        }

        this.startGameOverFade = false;
        this.ShowGameOverMenu();
    }

    private void ShowGameOverMenu() {
        this.brainsStatText.text = this._PlayerStats.BrainsCollected + "";
        this.killsStatText.text = this._PlayerStats.ZombieKills + "";
        this.bossKillsStatText.text = this._PlayerStats.BossKills + "";

        foreach (Transform child in this.gameOverUI.transform) {
            child.gameObject.SetActive(true);
        }
    }

    private void HideUI () {
        // Reset cursor
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        // Hide boss healthbars
        HealthBar[] healthBars = FindObjectsOfType<HealthBar>();
        foreach (HealthBar healthBar in healthBars) {
            healthBar.Hide();
        }

        // Hide selected weapon
        this.selectedWeaponHUD.SetActive(false);
        this.grenadeHUD.SetActive(false);

        // Hide brains
        this.brainsHUD.SetActive(false);

        // Hide round
        this.roundHUD.SetActive(false);
    }

    // @TODO replace dev scene with current level scene
    public void OnGameOverPlayAgainButtonClick () {
        SceneHandler.Load(SceneHandler.Scene.DEV);
    }

    public void OnGameOverQuitButtonClick () {
        SceneHandler.Load(SceneHandler.Scene.START_MENU);
    }

    /////////////////////////////////////////////////////////////////
    // S E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void SetBrainsText (int brains) {
        this.brainsText.text = brains.ToString();
    }

    // Display the weapon image of the selected weapon.
    public void SetSelectedWeapon (Weapon _Weapon) {
        this.weaponImage.sprite = this.weaponImages[(int)_Weapon.WeaponType];
        // this.SetSelectedWeaponText(_Weapon.GetName());
        this.SetAmmoText(_Weapon.GetAmmo());
    }

    // Display new ammo count after weapon shoots.
    public void UpdateAmmoCountText (int ammo) {
        this.SetAmmoText(ammo);
    }

    // Display new ammo count after weapon shoots.
    public void UpdateGrenadeAmmoCountText (int ammo) {
        this.SetGrenadeAmmoText(ammo);
    }

    // Set the name to display of the selected weapon.
    /*private void SetSelectedWeaponText (string text) {
        this.weaponText.text = text;
    }*/

    // Generic helper method for setting ammo text.
    private void SetAmmoText (TextMeshProUGUI tmText, int ammo) {
        if (Weapons.IsInfiniteAmmo(ammo)) {
            tmText.text = TEXT_INFINITE_AMMO;
        } else {
            tmText.text = "" + ammo;
        }
    }

    // Set the ammo count text of the selected weapon.
    private void SetAmmoText (int ammo) {
        this.SetAmmoText(this.ammoText, ammo);
    }

    // Set the grenade ammo count text of the selected weapon.
    private void SetGrenadeAmmoText (int ammo) {
        this.SetAmmoText(this.grenadeAmmoText, ammo);
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent +=
            this.OnPlayerDeathEvent;
        PlayerStatsEventHandler.PlayerStatsBrainsUpdate +=
            this.OnPlayerStatsBrainsUpdated;
        PurchasingEventHandler.WeaponPurchasingStateUpdatedEvent +=
            this.OnPurchasingStateUpdated;
    }

    public void UnregisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent -=
            this.OnPlayerDeathEvent;
        PlayerStatsEventHandler.PlayerStatsBrainsUpdate -=
            this.OnPlayerStatsBrainsUpdated;
        PurchasingEventHandler.WeaponPurchasingStateUpdatedEvent -=
            this.OnPurchasingStateUpdated;
    }

    private void OnPlayerDeathEvent () {
        this.StartGameOver();
        this.HideUI();
    }

    private void OnPlayerStatsBrainsUpdated (int brains) {
        this.SetBrainsText(brains);
    }

    private void OnPurchasingStateUpdated (Weapons.Enum weapon, WeaponsPurchasing.State state) {
        if (this.debug) { Debug.Log("EVENT :: Purchasing State Updated :: HUD - " + weapon + ":" + state); }
        if (weapon == Weapons.Enum.GRENADE && state == WeaponsPurchasing.State.PURCHASED) { // @TEST
            this.ShowGrenadeHUD();
        }
    }
}
