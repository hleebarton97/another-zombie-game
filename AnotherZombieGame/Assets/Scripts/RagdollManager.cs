﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// <Studio Name>
// Lee Barton
public class RagdollManager : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float ragdollForce = 200f;

    private Rigidbody[] rigidBodies;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.SetupReferences();
        this.DoRagdoll(false);

    }

	/////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        this.rigidBodies = this.GetComponentsInChildren<Rigidbody>(true);
    }

    // Activate ragdoll effect on jointed rigid body.
    private void DoRagdoll (bool active) {
        foreach (Rigidbody rigidBody in rigidBodies) {
            if (!Util.IsNull(rigidBody)) {
                rigidBody.useGravity = active;
                rigidBody.isKinematic = !active;
            }
        }
    }

    public void DoRagdoll (GameObject gameObject, RaycastHit hit, float timeUntilDestroyed) {
        this.DisableCommonComponents(gameObject);
        this.DoRagdoll(true);
        this.ApplyForce(hit);

        Object.Destroy(gameObject, timeUntilDestroyed);
    }

    public void DoRagdoll (GameObject gameObject, float timeUntilDestroyed) {
        this.DisableCommonComponents(gameObject);
        this.DoRagdoll(true);

        Object.Destroy(gameObject, timeUntilDestroyed);
    }

    // Disable common components (NavMeshAgent, Animator ... )
    private void DisableCommonComponents (GameObject gameObject) {
        NavMeshAgent navAgent = gameObject.GetComponent<NavMeshAgent>();
        Animator animator = gameObject.GetComponent<Animator>();

        if (Util.IsNull(navAgent)) {
            navAgent = gameObject.GetComponentInChildren<NavMeshAgent>();
        }

        if (Util.IsNull(animator)) {
            animator = gameObject.GetComponentInChildren<Animator>();
        }

        if (!Util.IsNull(navAgent)) { navAgent.enabled = false; }
        if (!Util.IsNull(animator)) { animator.enabled = false; }
    }

    // Apply force to area shot.
    private void ApplyForce (RaycastHit hit) {
        hit.transform.gameObject.GetComponent<Rigidbody>().AddRelativeForce(
            this.ragdollForce * hit.transform.forward
        );
    }
}
