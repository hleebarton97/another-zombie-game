﻿using UnityEngine;
using Novasloth;

// Novasloth Games LLC
// Lee Barton
// @TODO Create spawner with object pooling
public class Spawner : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    private const bool DEV_MODE = true;
    // private const int ZOMBIE_COUNT_CONSTANT = 24; // @TODO replace inspector values

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public int EnemyCount { get { return enemyCount; } }
    public bool Spawning { get; private set; } = false;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private int roundSpawnCount;
    [SerializeField] private int currentSpawnCount;
    [SerializeField] private int enemyCount;
    [SerializeField] private float spawnDelay = 1.50f;

    [Header("Spawn Points")]
    [SerializeField] private Transform enemySpawnPoints;
    [SerializeField] private Transform zombieBossSpawnPointsParent;
    [SerializeField] private Transform spiderBossSpawnPointsParent;
    [SerializeField] private Transform perkMachineSpawnPoints;

    [Header("Enemies To Spawn")]
    [SerializeField] private GameObject enemyWalkerPrefab;
    [SerializeField] private GameObject enemyRunnerPrefab;

    [Header("Bosses To Spawn")]
    [SerializeField] private GameObject bossZombiePrefab;
    [SerializeField] private GameObject bossSpiderPrefab;

    [Header("Interactables To Spawn")]
    [SerializeField] private GameObject perkMachinePrefab;
    private PerkMachineSpawn _PerkMachineSpawn;
    private PerkMachineDestruct _PerkMachineDestruct;
    private bool perkMachineSpawned = false;

    [Header("Spawn Properties")]
    [SerializeField] private int minZombieCountConstant = 24;
    [Range(0.0f, 1.00f)]
    [SerializeField] private float chanceZombieSpawn = 0.50f;
    [Range(0.0f, 1.00f)]
    [SerializeField] private float chanceSpiderSpawn = 0.50f;

    private RoundSystem _RoundSystem;
    private Transform playerTransform;

    private Timer spawnDelayTimer;
    private bool timeToSpawn = false;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void OnValidate () {
        chanceSpiderSpawn = 1.00f - chanceZombieSpawn;
        chanceZombieSpawn = 1.00f - chanceSpiderSpawn;
    }

    // Start is called before the first frame update
    private void Awake () {
        RegisterEventHandlers();
        SetupReferences();
        Init();
    }

    private void Update () {
        spawnDelayTimer.Tick();
        CheckSpawn();
    }

    private void OnDestroy () {
        UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        GameObject roundSystem = Util.GetRoundSystemByTag();
        if (!Util.IsNull(roundSystem)) {
            _RoundSystem = roundSystem.GetComponent<RoundSystem>();
        }

        GameObject player = Util.GetPlayerByTag();

        if (Util.IsNull(player)) {
            Debug.LogError("Spawner :: SetupReferences() :: Player is NULL! Disabling...");
            enabled = false;
            return;
        }

        playerTransform = player.transform;
    }

    private void Init () {
        roundSpawnCount = 0;

        spawnDelayTimer = new Timer(spawnDelay, () => {
            timeToSpawn = true;
        });
    }

    private void CheckSpawn () {
        if (Spawning) {
            if (timeToSpawn) {
                // Debug.Log("INFO :: Spawner :: CheckSpawn - spawning enemy!");
                SpawnRandomEnemy();
                spawnDelayTimer.Reset();
                timeToSpawn = false;
            }

            bool reachedSpawnCount = currentSpawnCount == roundSpawnCount;
            if (reachedSpawnCount) {
                // Debug.Log("INFO :: Spawner :: Spawning complete!");
                Spawning = false;
                spawnDelayTimer.Pause();
                currentSpawnCount = 0;
            }
        }
    }

    /////////////////////////////////////////////////////////////////
    // S P A W N   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void SpawnPerkMachine () {
        perkMachineSpawned = true;
        Vector3 spawnPoint = GetRandomSpawnPoint(perkMachineSpawnPoints).position;
        spawnPoint.y = perkMachinePrefab.transform.position.y;

        GameObject perkMachine = Instantiate(perkMachinePrefab, spawnPoint, perkMachinePrefab.transform.rotation);
        _PerkMachineSpawn = perkMachine.GetComponent<PerkMachineSpawn>();
        _PerkMachineDestruct = perkMachine.GetComponent<PerkMachineDestruct>();
    }

    private void DespawnPerkMachine () {
        perkMachineSpawned = false;
        _PerkMachineDestruct.DoBreak();
        _PerkMachineSpawn = null;
        _PerkMachineDestruct = null;
    }

    public void SpawnEnemy (Enemy.Zombie enemyType) {
        switch (enemyType) {
            case Enemy.Zombie.WALKER: {
                SpawnWalkerEnemy();
                break;
            }
            case Enemy.Zombie.RUNNER: {
                SpawnRunnerEnemy();
                break;
            }
            default: {
                SpawnRandomEnemy();
                break;
            }
        }
    }

    public void SpawnRandomEnemy () {
        if (Util.GetChance(0.50f)) {
            SpawnWalkerEnemy();
        } else {
            SpawnRunnerEnemy();
        }
    }

    public void SpawnWalkerEnemy () {
        Vector3 spawnPoint = GetRandomEnemySpawnPoint().position;
        SpawnAndLookAt(enemyWalkerPrefab, spawnPoint);
    }

    public void SpawnRunnerEnemy () {
        Vector3 spawnPoint = GetRandomEnemySpawnPoint().position;
        SpawnAndLookAt(enemyRunnerPrefab, spawnPoint);
    }

    // Spawn specific enemy boss type
    public void SpawnBoss (Enemy.Boss bossType) {
        switch (bossType) {
            case Enemy.Boss.ZOMBIE: {
                SpawnZombieBoss();
                break;
            }
            case Enemy.Boss.SPIDER: {
                SpawnSpiderBoss();
                break;
            }
            default: {
                SpawnRandomBoss();
                break;
            }
        }
    }

    // Spawn random boss type
    public void SpawnRandomBoss () {
        SpawnBoss(
            Util.GetChance(chanceZombieSpawn) 
                ? Enemy.Boss.ZOMBIE 
                : Enemy.Boss.SPIDER
        );
    }

    // @PPI replace new Vector3
    public void SpawnZombieBoss () {
        Vector3 spawnPoint = GetRandomBossSpawnPoint(Enemy.Boss.ZOMBIE).position;
        spawnPoint.y = bossZombiePrefab.transform.position.y;

        SpawnAndLookAt(bossZombiePrefab, spawnPoint);
    }

    public void SpawnSpiderBoss () {
        Vector3 spawnPoint = GetRandomBossSpawnPoint(Enemy.Boss.SPIDER).position;
        SpawnAndLookAt(bossSpiderPrefab, spawnPoint);
    }

    private void SpawnAndLookAt (GameObject prefab, Vector3 spawnPoint) {
        Vector3 lookAtTarget = GetLookAtPosition(spawnPoint);
        GameObject enemy = Instantiate(prefab, spawnPoint, prefab.transform.rotation);
        enemy.transform.LookAt(lookAtTarget);

        currentSpawnCount++;
        enemyCount++;
    }

    // Spawn enemies based on current round.
    public void NewRoundSpawn (int round) {
        bool bossRound = (round % 10) == 0;
        bool perkRound = (round % 5) == 0;

        roundSpawnCount = CalculateSpawnCount(round);
        Debug.Log($"INFO :: Spawner :: {roundSpawnCount} zombies to spawn for round {round}");

        if (perkMachineSpawned) {
            DespawnPerkMachine();
        }

        if (bossRound) {
            SpawnRandomBoss();
            roundSpawnCount++;
        }

        if (perkRound) {
            SpawnPerkMachine();
        }

        // Start spawning
        Spawning = true;
        spawnDelayTimer.Start();
    }

    // Formula used to calculate how many enemies to spawn based on current round.
    // 0.09n^2 + 24 where n is the current round
    private int CalculateSpawnCount (int round) {
        return Mathf.FloorToInt((0.09f * (round * round)) + minZombieCountConstant);
    }

    /////////////////////////////////////////////////////////////////
    // G E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private Transform GetRandomSpawnPoint (Transform parentTransform) {
        int index = Random.Range(0, parentTransform.childCount);
        return parentTransform.GetChild(index);
    }

    private Transform GetRandomBossSpawnPoint (Enemy.Boss bossType) {
        switch (bossType) {
            case Enemy.Boss.ZOMBIE: {
                return GetRandomSpawnPoint(zombieBossSpawnPointsParent);
            }
            case Enemy.Boss.SPIDER: {
                return GetRandomSpawnPoint(spiderBossSpawnPointsParent);
            }
        }

        return null;
    }

    private Transform GetRandomEnemySpawnPoint () {
        return GetRandomSpawnPoint(enemySpawnPoints);
    }

    private Vector3 GetLookAtPosition (Vector3 spawnPosition) {
        return new Vector3(
            playerTransform.position.x,
            spawnPosition.y,
            playerTransform.position.z
        );
    }

    /////////////////////////////////////////////////////////////////
    // S E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // E V E N T S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        EnemyEventHandler.EnemyZombieDeathEvent += OnEnemyDeath;
        EnemyEventHandler.EnemyBossDeathEvent += OnEnemyDeath;

        PlayerEventHandler.PlayerDeathEvent += OnPlayerDeathEvent;
    }

    public void UnregisterEventHandlers () {
        EnemyEventHandler.EnemyZombieDeathEvent -= OnEnemyDeath;
        EnemyEventHandler.EnemyBossDeathEvent -= OnEnemyDeath;

        PlayerEventHandler.PlayerDeathEvent -= OnPlayerDeathEvent;
    }

    private void OnEnemyDeath (GameObject enemy) {
        enemyCount--;
        // Debug.Log($"INFO :: Spawner :: Enemy - {enemy} - has died, spawn count {this.enemyCount}");
    }

    private void OnPlayerDeathEvent () {
        enabled = false;
    }
}
