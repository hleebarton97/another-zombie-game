﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public interface ILaunchable {

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    void CheckLaunch (); // Logic to check if rocket should be launched.
    void Launch (); // Launch rocket.
    
    void ReloadToMax (); // Reload weapon to max ammo.
    bool IsFullAmmo (); // Check that the weapon is at full ammo capacity.
    void DoAmmo (); // Handle used ammo.

    /////////////////////////////////////////////////////////////////
    // G E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////
    
    Weapons.Enum GetWeaponType ();
    Weapons.Kind GetWeaponKind ();
    string GetName ();
    int GetAmmo ();
}
