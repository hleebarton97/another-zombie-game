using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public interface IPerkAbility {
    public void ActivateAbility ();
}
