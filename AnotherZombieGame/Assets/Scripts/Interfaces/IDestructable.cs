using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public interface IDestructable {
    public void DoDestruction (float multiplier, float distanceFromCenter, float radius);
}
