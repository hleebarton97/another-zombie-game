// Novasloth Games LLC
// Lee Barton
using UnityEngine;

public interface IAttackable {
    void Attacked (int damage);
    GameObject GetGameObject ();
}
