// Novasloth Games LLC
// Lee Barton
public interface IHealthBar {

    void UpdateHealthBar ();
    void HideHealthBar ();

}
