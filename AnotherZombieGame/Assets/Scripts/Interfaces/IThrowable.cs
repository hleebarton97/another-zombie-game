﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public interface IThrowable {

	/////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Logic to check if object should be thrown.
    void CheckThrow ();
    // Throw object.
    void Throw ();
}
