﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public interface IShootable {

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    void CheckShoot (); // Logic to check if weapon should be fired.
    void Shoot (); // Fire weapon.

    void CheckHit (RaycastHit hit, Quaternion rotation); // Check what was hit on shootable mask.
    void EnemyHit (GameObject obj, RaycastHit hit); // Enemy was hit, so damage enemy.
    void DoParticleEffectOnHit (GameObject effectPrefab, RaycastHit hit, Quaternion rotation); // Show particle effect on object hit.

    void ReloadToMax (); // Reload weapon to max ammo.
    bool IsFullAmmo (); // Check that the weapon is at full ammo capacity.
    void DoAmmo (); // Handle used ammo.
    void DoEffects (); // Activate particle effects & point light & line render.
    void DisableEffects (); // Disable line render and point light.

    /////////////////////////////////////////////////////////////////
    // G E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    string GetName ();
    int GetAmmo();

}
