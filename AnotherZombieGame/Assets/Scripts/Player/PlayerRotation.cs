﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class PlayerRotation : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float maxCameraRayDistance = 100f;

    private int groundMask;
    private Rigidbody playerRigidbody;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.RegisterEventHandlers();
        this.groundMask = LayerMask.GetMask( // @PPI - Just use (int)Layer.GROUND_MASK?
            LayerMask.LayerToName((int)Layer.GROUND_MASK)
        );
        this.playerRigidbody = this.GetComponent<Rigidbody>();
    }

    private void FixedUpdate () {
        LookAtCursor();
    }

    private void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Rotate the player to face the mouse cursor.
    private void LookAtCursor () {
        RaycastHit raycastHitGround;
        // If raycast hits GroundMask layer
        if (Rotation.MouseToCameraRayHitMaskLayer(
            Input.mousePosition,
            out raycastHitGround,
            this.maxCameraRayDistance,
            this.groundMask
        )) {
            this.playerRigidbody.MoveRotation(Rotation.RotateToCursor(
                this.transform.position,
                raycastHitGround
            ));
        }
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent += this.OnPlayerDeathEvent;
    }

    public void UnregisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent -= this.OnPlayerDeathEvent;
    }

    public void OnPlayerDeathEvent () {
        this.enabled = false;
    }
}
