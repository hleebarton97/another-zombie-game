﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class PlayerStats : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public int Brains { get { return this.brains; } }
    public int BrainsCollected { get { return this.brainsCollected;} }
    public int ZombieKills { get { return this.zombieKills; } }
    public int BossKills { get {  return this.bossKills; } }
    public int TotalScore { get {
            return BrainsCollected * ZombieKills * (BossKills * 5);
    }}

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private int brains;
    [SerializeField] private int zombieKills;
    [SerializeField] private int bossKills;

    int brainsCollected = 0;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Setup references and initailize variables.
    private void Awake () {
        this.RegisterEventHandlers();
        this.SetupReferences();
    }

    private void Start () {
        this.brainsCollected = this.brains;
        this.BrainsUpdatedEvent();
    }

    private void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {}

    public void AddBrains (int brains) {
        this.brainsCollected += brains;
        this.brains += brains;
        this.BrainsUpdatedEvent();
    }

    public void UseBrains (int brains) {
        this.brains -= brains;
        this.BrainsUpdatedEvent();
    }

    public bool TryUseBrains (int brains) {
        if (this.brains >= brains) {
            this.UseBrains(brains);
            return true;
        }
        Debug.Log($"INFO :: PlayerStats.TryUseBrains() :: Not Enough Brains For Purchase!");
        return false;
    }

    // Use brains for purchased weapon.
    public void UseBrains (Weapons.Enum weapon) => this.UseBrains(Weapons.GetWeaponPrice(weapon));

    private void BrainsUpdatedEvent () => PlayerStatsEventHandler.BrainsStatUpdated(this.brains);

    /////////////////////////////////////////////////////////////////
    // E V E N T S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        EnemyEventHandler.EnemyZombieDeathEvent +=
            this.OnEnemyZombieDeathEvent;
    }

    public void UnregisterEventHandlers () {
        EnemyEventHandler.EnemyZombieDeathEvent -=
            this.OnEnemyZombieDeathEvent;
    }

    public void OnEnemyZombieDeathEvent (GameObject enemy) {
        this.zombieKills++;
    }
}

