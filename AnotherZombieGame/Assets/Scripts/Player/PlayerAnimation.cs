﻿using UnityEngine;

// <Studio Name>
// Lee Barton
public class PlayerAnimation : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private Animator animator;

    private int rifleLayerIndex;
    private int throwableLayerIndex;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.animator = this.GetComponentInChildren<Animator>();
        this.rifleLayerIndex = this.animator.GetLayerIndex(MyAnimation.RIFLE_LAYER);
        this.throwableLayerIndex = this.animator.GetLayerIndex(MyAnimation.THROWABLE_LAYER);
    }

    private void Start () {
        this.RegisterEventHandlers();
    }

    private void FixedUpdate () {
        this.AnimateWeapon();
        this.AnimateMovement();
    }

    private void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Set animator controller layer for weapons.
    private void AnimateWeapon () {
        this.animator.SetBool(
            MyAnimation.HAS_PISTOL,
            Weapons.Instance.HasPistol
        );

        this.animator.SetBool(
            MyAnimation.HAS_RIFLE,
            Weapons.Instance.HasRifle
        );

        if (Weapons.Instance.HasRifle) {
            this.animator.SetLayerWeight(
                this.animator.GetLayerIndex(MyAnimation.RIFLE_LAYER),
                1.0f
            );
        } else if (Weapons.Instance.HasPistol) {
            this.animator.SetLayerWeight(
                this.animator.GetLayerIndex(MyAnimation.RIFLE_LAYER),
                0.0f
            );
        }
    }

    // Set animator controller variables.
    private void AnimateMovement () {
        this.animator.SetBool( // JoystickButton8 = Left Stick Button (LSB or L3)
            MyAnimation.IS_SPRINTING,
            Movement.Instance.PlayerIsSprinting
        );

        this.animator.SetBool(
            MyAnimation.IS_WALKING,
            Movement.IsWalking(Input.GetAxisRaw("Vertical"))
        );

        this.animator.SetBool(
            MyAnimation.IS_WALKING_BACK,
            Movement.IsWalkingBackward(Input.GetAxisRaw("Vertical"))
        );

        this.animator.SetBool(
            MyAnimation.IS_STRAFING_LEFT,
            Movement.IsStrafingLeft(Input.GetAxisRaw("Horizontal"))
        );

        this.animator.SetBool(
            MyAnimation.IS_STRAFING_RIGHT,
            Movement.IsStrafingRight(Input.GetAxisRaw("Horizontal"))
        );

        this.animator.SetBool(
            MyAnimation.IS_THROWING,
            Movement.Instance.PlayerIsThrowing
        );

        //
        this.animator.SetLayerWeight(
            this.throwableLayerIndex,
            (Movement.Instance.PlayerIsThrowing ? 1.0f : 0.0f)
        );
    }

    private void AnimateDeath () {
        this.animator.SetTrigger(
            MyAnimation.IS_DEAD
        );
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        // Register animation event handler
        AnimationEventHandler.AnimationPlayerThrowingEnd +=
            this.OnThrowingAnimationEnd;
        PlayerEventHandler.PlayerDeathEvent +=
            this.OnPlayerDeathEvent;
    }

    public void UnregisterEventHandlers () {
        AnimationEventHandler.AnimationPlayerThrowingEnd -=
            this.OnThrowingAnimationEnd;
        PlayerEventHandler.PlayerDeathEvent -=
           this.OnPlayerDeathEvent;
    }

    // Handle throwing animation end event.
    private void OnThrowingAnimationEnd (AnimationEventHandler handler) {
        Movement.Instance.PlayerIsThrowing = false;
    }

    private void OnPlayerDeathEvent () {
        this.AnimateDeath();
    }
}
