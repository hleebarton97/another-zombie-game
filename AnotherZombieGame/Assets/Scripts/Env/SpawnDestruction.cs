using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class SpawnDestruction : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] GameObject[] destructionObjects;
    
    [Header("Spawn Sphere")]
    [SerializeField] Vector3 offset = Vector3.zero;
    [SerializeField] float radius = 0.5f;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Calculate position with offset
    // Add adjustable radius
    private void OnDrawGizmosSelected () {
        #if UNITY_EDITOR
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(this.GetSphereOffsetPosition(), this.radius);
            Gizmos.color = Color.white;
        #endif
    }

    private void Awake () {
        
    }

    private void Start () {
        
    }

    private void Update () {
        
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void SpawnRandomObject () {
        if (this.destructionObjects.Length > 0) {
            GameObject objToSpawn = this.destructionObjects[Random.Range(0, this.destructionObjects.Length)];
            Vector3 spawnPoint = this.GetSpawnPointInSphere();
            GameObject obj = Instantiate(objToSpawn);
            obj.transform.position = spawnPoint;
        }
    }

    public void SpawnRandomObjects () {
        if (this.destructionObjects.Length > 0) {
            int spawnCount = Random.Range(0, this.destructionObjects.Length + 1);
            for (int i = 0; i < spawnCount; i++) {
                this.SpawnRandomObject();
            }
        }
    }

    private Vector3 GetSpawnPointInSphere () {
        Vector3 randPoint = ((Random.insideUnitSphere * this.radius) + this.GetSphereOffsetPosition());
        randPoint.y = Mathf.Abs(randPoint.y); // Force top half of sphere
        return randPoint;
    }

    private Vector3 GetSphereOffsetPosition () {
        return (this.transform.localPosition + this.offset);
    }
}
