using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class BreakOnDestroy : Destruct, IDestructable {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Destructable Properties")]
    [SerializeField] private bool hasHealth;
    [SerializeField] private float maxHealth = 100.0f;
    [SerializeField] private float health;

    [SerializeField] private float maxRangeToBreak = 1.5f;
    [SerializeField] private float damage = 100.0f;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.Init();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Init () {
        base.Init();
        this.health = this.maxHealth;
    }

    public void DoDestruction (float multiplier, float distanceFromCenter, float radius) {
        if (!this.isDestroyed) {
            if (distanceFromCenter <= this.maxRangeToBreak) {
                this.DoBreak();
            } else if (this.hasHealth) {
                float damageMultiplier = Util.CalculateExplosionDamageMultiplier(radius, distanceFromCenter);
                this.health -= (damageMultiplier * this.damage);
                this.DoDamageEffects();
                
                if (this.health <= 0) {
                    this.DoBreak();
                }
            }
        }
    }
}
