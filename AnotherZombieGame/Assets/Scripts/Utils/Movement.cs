﻿using UnityEngine;

// <Studio Name>
// Lee Barton
public sealed class Movement {

    /////////////////////////////////////////////////////////////////
    // I N S T A N C E
    /////////////////////////////////////////////////////////////////

    private static readonly Movement instance = new Movement();

    // Explicit static constructor to tell C# compiler to not mark
    // type as 'beforefieldinit'
    static Movement () {}
    private Movement () {}

    public static Movement Instance {
        get {
            return instance;
        }
    }

    /////////////////////////////////////////////////////////////////
    // S T A T I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public static float SPRINT_DIFF = 1.2f;

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public float PlayerCurrentSpeed { get; set; }
    public bool PlayerIsSprinting { get; set; }
    public bool PlayerIsThrowing { get; set; }

    public float HorizontalAxisMovement { get; set; }
    public float VerticalAxisMovement { get; set; }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Set the speed the player sprints at based on the set speed.
    public void SetSprintSpeed (float speed) {
        this.PlayerCurrentSpeed = speed + SPRINT_DIFF;
    }

    // Set the speed the player walks at based on the set speed.
    public void SetWalkSpeed (float speed) {
        this.PlayerCurrentSpeed = speed;
    }

    // Check if user is moving the player character
    public bool PlayerIsMoving () {
        bool horizontalMovement = this.HorizontalAxisMovement > 0 || this.HorizontalAxisMovement < 0;
        bool verticalMovement = this.VerticalAxisMovement > 0 || this.VerticalAxisMovement < 0;
        return horizontalMovement || verticalMovement;
    }

    /////////////////////////////////////////////////////////////////
    // S T A T I C   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Calculate vector movement based on horizontal and vertical axis input.
    public static Vector3 CalculateWithInput (float horizontalAxisInput, float verticalAxisInput, Vector3 currentPosition, float speed) {
        return currentPosition + new Vector3(
            horizontalAxisInput,
            0f,
            verticalAxisInput
        ).normalized * speed * Time.deltaTime;
    }

    // Determine if player is walking forward.
    public static bool IsWalking (float vertical) {
        return (vertical > 0f);
    }

    // Determine if player is walking backward.
    public static bool IsWalkingBackward (float vertical) {
        return (vertical < 0f);
    }

    // Determine if player is strafing left.
    public static bool IsStrafingLeft (float horizontal) {
        return (horizontal < 0f);
    }

    // Determine if player is strafing right.
    public static bool IsStrafingRight (float horizontal) {
        return (horizontal > 0f);
    }

    // JoystickButton8 = Left Stick Button (LSB or L3)
    // Determine if player is sprinting.
    public static bool IsSprinting (bool lShift, bool rShift) {
        return (lShift || rShift);
    }
}
