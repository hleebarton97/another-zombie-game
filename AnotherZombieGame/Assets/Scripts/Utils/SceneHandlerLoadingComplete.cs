using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class SceneHandlerLoadingComplete : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private bool firstUpdate = true;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Update () {
        if (this.firstUpdate) {
            this.firstUpdate = false;
            SceneHandler.LoadingComplete();
        }
    }
}
