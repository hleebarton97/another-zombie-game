﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class Rotation {

    // Check if a raycast hit a specific layer.
    public static bool MouseToCameraRayHitMaskLayer (Vector3 mousePos, out RaycastHit hitInfo, float rayMaxDistance, int layerMask) {
        Ray ray = Camera.main.ScreenPointToRay(mousePos); // Ray to camera from mouse cursor position
        return Physics.Raycast(ray, out hitInfo, rayMaxDistance, layerMask);
    }

    // Rotate object to mouse cursor.
    public static Quaternion RotateToCursor (Vector3 objPosition, RaycastHit hit) {
        Vector3 playerToMouseCursor = hit.point - objPosition;
        playerToMouseCursor.y = 0f;
        return Quaternion.LookRotation(playerToMouseCursor);
    }
}
