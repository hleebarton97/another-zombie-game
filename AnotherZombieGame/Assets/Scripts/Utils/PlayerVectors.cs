using UnityEngine;

// Lee Barton
public class PlayerVectors {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public Vector3 FuturePosition { get; set; }
    public Vector3 CurrentPosition { get; set; }
    public Vector3 PreviousPosition { get; set; }
    public Vector3 Velocity { get; set; }

    /////////////////////////////////////////////////////////////////
    // C O N S T R U C T O R
    /////////////////////////////////////////////////////////////////

    public PlayerVectors (
        Vector3 futurePosition,
        Vector3 currentPosition,
        Vector3 previousPosition,
        Vector3 velocity
    ) {
        this.FuturePosition = futurePosition;
        this.CurrentPosition = currentPosition;
        this.PreviousPosition = previousPosition;
        this.Velocity = velocity;
    }

    public PlayerVectors (Vector3 initPosition) {
        this.FuturePosition = initPosition;
        this.CurrentPosition = initPosition;
        this.PreviousPosition = initPosition;
        this.Velocity = Vector3.zero;
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public Vector3 CalculateVelocity () {
        this.Velocity = (this.CurrentPosition - this.PreviousPosition) / Time.deltaTime;
        return this.Velocity;
    }

    public void CalculateFuturePosition (Vector3 projectilePos, float projectileSpeed) {
        this.FuturePosition = this.CurrentPosition + (
            this.CalculateVelocity() * (
                (projectilePos - this.PreviousPosition).magnitude / projectileSpeed
            )
        );
    }
}
