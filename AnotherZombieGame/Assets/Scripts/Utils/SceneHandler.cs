using System;
using UnityEngine.SceneManagement;

// Novasloth Games LLC
// Lee Barton
public static class SceneHandler {

    public const string SCENE_DEV = "Dev";
    public const string SCENE_START_MENU = "StartMenu";
    public const string SCENE_CHARACTER_MENU = "CharacterSelectionMenu";
    public const string SCENE_LOADING = "Loading";

    public enum Scene {
        DEV,
        START_MENU,
        CHARACTER_MENU,
        LOADING
    }

    private static Action LoadingCompleteEvent;

    public static void Load (Scene Scene) {
        LoadingCompleteEvent = () => {
            SceneManager.LoadScene((int)Scene);
        };

        SceneManager.LoadScene((int)Scene.LOADING);
    }

    public static void LoadingComplete () {
        LoadingCompleteEvent?.Invoke();

        if (!Util.IsNull(LoadingCompleteEvent)) {
            LoadingCompleteEvent = null;
        }
    }

    public static string GetSceneNameFromType (Scene scene) {
        switch (scene) {
            case Scene.DEV:
                return SCENE_DEV;
            case Scene.START_MENU:
                return SCENE_START_MENU;
            case Scene.CHARACTER_MENU:
                return SCENE_CHARACTER_MENU;
            default:
                return SCENE_LOADING;
        }
    }

    public static Scene GetSceneTypeFromName (string scene) {
        switch (scene) {
            case SCENE_DEV:
                return Scene.DEV;
            case SCENE_START_MENU:
                return Scene.START_MENU;
            case SCENE_CHARACTER_MENU:
                return Scene.CHARACTER_MENU;
            default:
                return Scene.LOADING;
        }
    }
}
