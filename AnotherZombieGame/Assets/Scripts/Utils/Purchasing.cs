using System.Collections.Generic;
using UnityEngine;

public class Purchasing : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public enum State {
        LOCKED,
        PURCHASABLE,
        PURCHASED
    }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    protected List<State> purchasingState = new List<State>();

    // References
    protected PlayerStats _PlayerStats;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void Awake () {
        this.SetupReferences();
        this.Init();
        this.RegisterEventHandlers();
    }

    protected virtual void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void SetupReferences () {
        GameObject player = Util.GetPlayerByTag();
        this._PlayerStats = player.GetComponent<PlayerStats>();
    }

    protected virtual void Init () {}

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public virtual void RegisterEventHandlers () {
        PlayerStatsEventHandler.PlayerStatsBrainsUpdate +=
            this.OnPlayerStatsBrainsUpdated;
    }

    public virtual void UnregisterEventHandlers () {
        PlayerStatsEventHandler.PlayerStatsBrainsUpdate -=
            this.OnPlayerStatsBrainsUpdated;
    }

    protected virtual void OnPlayerStatsBrainsUpdated (int brains) {}
}
