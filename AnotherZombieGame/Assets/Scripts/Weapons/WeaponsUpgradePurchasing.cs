using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Codice.Client.Common.Connection.AskCredentialsToUser;

// Novasloth Games LLC
// Lee Barton
public class WeaponsUpgradePurchasing : Purchasing {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private List<int> weaponUpgradePrices = new List<int>();
    private Dictionary<(Weapons.Enum, WeaponUpgrade.Enum), int> purchasingStateIndexLookup = new Dictionary<(Weapons.Enum, WeaponUpgrade.Enum), int>();
    private Dictionary<Weapons.Enum, List<WeaponUpgrade.Enum>> weaponUpgradesLookup = new Dictionary<Weapons.Enum, List<WeaponUpgrade.Enum>>();

    // References
    private WeaponsPurchasing _WeaponsPurchasing;

    private Weapon[] weapons;
    private Grenade grenade;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        base.SetupReferences();

        this._WeaponsPurchasing = Util.GetPlayerByTag().GetComponentInChildren<WeaponsPurchasing>();
    }

    protected override void Init () {
        base.Init();

        int weaponCount = this.transform.childCount;
        if (weaponCount > 0) {
            this.weapons = this.GetComponentsInChildren<Weapon>(true);
            List<WeaponUpgrade.Enum> weaponUpgradeTypes;

            // Build state lookup
            int index = 0;
            foreach (Weapon weapon in weapons) {
                WeaponUpgrade[] weaponUpgrades = weapon.gameObject.GetComponents<WeaponUpgrade>();
                weaponUpgradeTypes = new List<WeaponUpgrade.Enum>();

                foreach (WeaponUpgrade weaponUpgrade in weaponUpgrades) {
                    this.purchasingStateIndexLookup.Add(
                        (weapon.WeaponType, weaponUpgrade.Type),
                        index
                    );

                    this.weaponUpgradePrices.Add(weaponUpgrade.Price);
                    this.purchasingState.Add(State.LOCKED);
                    weaponUpgradeTypes.Add(weaponUpgrade.Type);

                    PurchasingEventHandler.UpgradePurchasingStateUpdated(
                        weapon.WeaponType, weaponUpgrade.Type, State.LOCKED
                    );

                    index++;
                }

                this.weaponUpgradesLookup.Add(weapon.WeaponType, weaponUpgradeTypes);
            }

            this.grenade = this.transform.GetComponentInChildren<Grenade>();
            WeaponUpgrade[] grenadeUpgrades = this.grenade.gameObject.GetComponents<WeaponUpgrade>();
            weaponUpgradeTypes = new List<WeaponUpgrade.Enum>();

            foreach (WeaponUpgrade grenadeUpgrade in grenadeUpgrades) {
                this.purchasingStateIndexLookup.Add(
                    (this.grenade.WeaponType, grenadeUpgrade.Type),
                    index
                );

                this.weaponUpgradePrices.Add(grenadeUpgrade.Price);
                this.purchasingState.Add(State.LOCKED);
                weaponUpgradeTypes.Add(grenadeUpgrade.Type);

                PurchasingEventHandler.UpgradePurchasingStateUpdated(
                    this.grenade.WeaponType, grenadeUpgrade.Type, State.LOCKED
                );

                index++;
            }

            this.weaponUpgradesLookup.Add(grenade.WeaponType, weaponUpgradeTypes);
        }

        // Debug.Log("INFO :: WeaponsUpgradePurchasing :: Init() -> PurchasingEventHandler.PurchasingSetupComplete()");
        PurchasingEventHandler.PurchasingSetupComplete();
    }

    // Check if weapon upgrade is able to be purchased and purchase if true.
    public void PurchaseUpgrade (Weapons.Enum weaponType, WeaponUpgrade.Enum upgradeType) {
        bool weaponPurchased = this._WeaponsPurchasing.IsWeaponPurchased(weaponType);

        if (weaponPurchased) {
            int lookupIndex = this.purchasingStateIndexLookup[(weaponType, upgradeType)];

            State state = this.purchasingState[lookupIndex];
            int price = this.weaponUpgradePrices[lookupIndex];

            if (state == State.PURCHASABLE) {
                PurchasingEventHandler.UpgradePurchased(weaponType, upgradeType);
                this._PlayerStats.UseBrains(price);
            }
        }
    }

    public bool IsUpgradePurchased (Weapons.Enum weaponType, WeaponUpgrade.Enum upgradeType)
        => this.GetWeaponUpgradeState(weaponType, upgradeType) == State.PURCHASED;

    public bool IsUpgradePurchasable (Weapons.Enum weaponType, WeaponUpgrade.Enum upgradeType)
        => this.GetWeaponUpgradeState(weaponType, upgradeType) == State.PURCHASABLE;

    public bool IsUpgradeLocked (Weapons.Enum weaponType, WeaponUpgrade.Enum upgradeType)
        => this.GetWeaponUpgradeState(weaponType, upgradeType) == State.LOCKED;

    public List<WeaponUpgrade.Enum> GetWeaponUpgradeTypes (Weapons.Enum weaponType)
        => this.weaponUpgradesLookup[weaponType];

    public int GetWeaponUpgradePrice (Weapons.Enum weaponType, WeaponUpgrade.Enum upgradeType) {
        int lookupIndex = this.purchasingStateIndexLookup[(weaponType, upgradeType)];
        return this.weaponUpgradePrices[lookupIndex];
    }

    public State GetWeaponUpgradeState (Weapons.Enum weaponType, WeaponUpgrade.Enum upgradeType) {
        int lookupIndex = this.purchasingStateIndexLookup[(weaponType, upgradeType)];
        return this.purchasingState[lookupIndex];
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public override void RegisterEventHandlers () {
        base.RegisterEventHandlers();

        PurchasingEventHandler.UpgradePurchasingStateLockedEvent +=
            this.OnUpgradePurchaseStateLocked;
        PurchasingEventHandler.UpgradePurchasingStatePurchasableEvent +=
            this.OnUpgradePurchaseStatePurchasable;
        PurchasingEventHandler.UpgradePurchasingStatePurchasedEvent +=
            this.OnUpgradePurchaseStatePurchased;

    }

    public override void UnregisterEventHandlers () {
        base.UnregisterEventHandlers();

        PurchasingEventHandler.UpgradePurchasingStateLockedEvent -=
            this.OnUpgradePurchaseStateLocked;
        PurchasingEventHandler.UpgradePurchasingStatePurchasableEvent -=
            this.OnUpgradePurchaseStatePurchasable;
        PurchasingEventHandler.UpgradePurchasingStatePurchasedEvent -=
            this.OnUpgradePurchaseStatePurchased;
    }

    private void OnUpgradePurchaseStateLocked (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade) {
        int index = this.purchasingStateIndexLookup[(weapon, upgrade)];
        State state = this.purchasingState[index];

        if (state == State.PURCHASABLE) {
            this.purchasingState[index] = State.LOCKED;
            PurchasingEventHandler.UpgradePurchasingStateUpdated(weapon, upgrade, State.LOCKED);
        }
    }

    private void OnUpgradePurchaseStatePurchasable (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade) {
        int index = this.purchasingStateIndexLookup[(weapon, upgrade)];
        State state = this.purchasingState[index];

        if (state == State.LOCKED) {
            this.purchasingState[index] = State.PURCHASABLE;
            PurchasingEventHandler.UpgradePurchasingStateUpdated(weapon, upgrade, State.PURCHASABLE);
        }
    }

    private void OnUpgradePurchaseStatePurchased (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade) {
        int index = this.purchasingStateIndexLookup[(weapon, upgrade)];
        this.purchasingState[index] = State.PURCHASED;
        PurchasingEventHandler.UpgradePurchasingStateUpdated(weapon, upgrade, State.PURCHASED);
    }

    protected override void OnPlayerStatsBrainsUpdated (int brains) {
        foreach (KeyValuePair<(Weapons.Enum, WeaponUpgrade.Enum), int> entry in this.purchasingStateIndexLookup) {
            int price = this.weaponUpgradePrices[entry.Value];

            if (brains >= price) {
                PurchasingEventHandler.UpgradePurchasable(entry.Key.Item1, entry.Key.Item2);
                continue;
            }

            PurchasingEventHandler.UpgradeLocked(entry.Key.Item1, entry.Key.Item2);
        }
    }

}
