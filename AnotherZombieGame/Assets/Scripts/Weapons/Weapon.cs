﻿using System.Collections.Generic;
using UnityEngine;
using Novasloth;

// Novasloth Games LLC
// Lee Barton
public class Weapon : MonoBehaviour, IShootable, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public Weapons.Enum WeaponType { get { return weaponType; } }

    public bool FullAuto {
        get { return isAuto; }
        set { isAuto = value; }
    }

    public int MaxAmmo {
        get { return maxAmmo; }
        set { maxAmmo = value; }
    }

    public bool ExplosiveUpgrade { get; set; } = false;

    public Dictionary<WeaponUpgrade.Enum, WeaponUpgrade> UpgradeLookup {
        get { return upgradeLookup; }
    }

    /////////////////////////////////////////////////////////////////
    // I N T E R F A C E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Weapon Specific")]
    [SerializeField] protected string displayName;
    [SerializeField] protected Weapons.Enum weaponType;
    [SerializeField] protected Weapons.Kind weaponKind;

    [SerializeField] protected int maxAmmo = 100;
    [SerializeField] protected int ammo = 100;

    [SerializeField] protected float damageMultiplier = 1.0f;
    [SerializeField] protected float range = 100.0f;
    [SerializeField] protected float rateOfFire = 100.0f;
    [SerializeField] protected float spread = 0.5f;

    [SerializeField] protected bool isAuto = false;

    [Header("Muzzle")]
    [SerializeField] protected Transform muzzleTransform;

    [Header("Effects")]
    [SerializeField] protected float effectsDisplayTime = 0.2f;
    [SerializeField] protected float impactEffectsDisplayTime = 1.0f;
    [SerializeField] protected GameObject impactEffectPrefab;
    [SerializeField] protected GameObject walkerBloodEffectPrefab;
    [SerializeField] protected ParticleSystem muzzleFlash;
    [SerializeField] protected Light muzzleLight;
    [SerializeField] protected LineRenderer lineRender;
    [SerializeField] protected GameObject explosivePrefab;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    protected HUD _HUD;

    protected Transform playerTransform;

    protected Timer rofTimer;
    protected float shootingDelay;
    protected bool timeToShoot = false;

    protected bool playerIsBusy; // Interacting / Open menu

    // Upgrade lookup
    private Dictionary<WeaponUpgrade.Enum, WeaponUpgrade> upgradeLookup;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void Awake () {
        Init();
        SetupReferences();
        RegisterEventHandlers();
    }

    protected virtual void Start () {
        rofTimer.Start();
    }

    // Update is called once per frame
    private void Update () {
        // Timers
        rofTimer.TimerDelay = Weapons.RateOfFireToTimerDelay(rateOfFire);
        rofTimer.Tick();

        // Debug.DrawRay(muzzleTransform.position, playerTransform.forward, Color.green);
        // Shooting
        CheckShoot();

        // Effects
        DisableEffects();
    }

    private void OnDestroy () {
        UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void Init () {
        // Timers
        rofTimer = new Timer(Weapons.RateOfFireToTimerDelay(rateOfFire), () => {
            timeToShoot = true;
        });

        // Ammo
        ammo = maxAmmo;

        // Lookups
        upgradeLookup = new Dictionary<WeaponUpgrade.Enum, WeaponUpgrade>();
        WeaponUpgrade[] weaponUpgrades = GetComponents<WeaponUpgrade>();

        if (weaponUpgrades.Length > 0) {
            foreach (WeaponUpgrade weaponUpgrade in weaponUpgrades) {
                upgradeLookup.Add(weaponUpgrade.Type, weaponUpgrade);
            }
        }
    }

    protected virtual void SetupReferences () {
        // References
        playerTransform = GameObject.FindGameObjectWithTag(Tag.PLAYER).transform;
        _HUD = GameObject.FindGameObjectWithTag(Tag.HUD).GetComponent<HUD>();
    }

    // Check how and if weapon should be fired.
    public virtual void CheckShoot () {
        if (Weapons.IsNotEmpty(ammo) && !playerIsBusy) {
            bool isNotThrowing = !Movement.Instance.PlayerIsThrowing;
            bool canShoot = timeToShoot && isNotThrowing;

            if (isAuto) {
                if (Input.GetButton("Fire1") && canShoot) {
                    Shoot();
                }
            } else {
                if (Input.GetButtonDown("Fire1") && canShoot) {
                    Shoot();
                }
            }
        }
    }

    // Fire the weapon.
    public virtual void Shoot () {
        // Effects
        DoEffects();

        // Ammo
        DoAmmo();

        // Check hit
        RaycastHit weaponHit;
        Ray weaponRay = Weapons.CreateWeaponRay(
            spread,
            range,
            muzzleTransform.position,
            playerTransform
        );

        if (Weapons.RayHitShootableMaskLayer(weaponRay, out weaponHit, range)) {
            lineRender.SetPosition(1, weaponHit.point);
            CheckHit(weaponHit, Quaternion.LookRotation(weaponHit.normal));
        } else {
            lineRender.SetPosition(1, weaponRay.origin + (weaponRay.direction * range));
        }

        // Reset timer
        timeToShoot = false;
        rofTimer.Reset();
    }

    // Check what was hit on shootable mask.
    public void CheckHit (RaycastHit hit, Quaternion rotation) {
        GameObject objectHit = hit.collider.transform.root.gameObject;

        GameObject effectPrefab = impactEffectPrefab;
        if (objectHit.tag == Tag.ENEMY) {
            // @TODO check enemy type - do blood effect
            EnemyHit(objectHit, hit);
            effectPrefab = walkerBloodEffectPrefab;
        }

        DoParticleEffectOnHit(effectPrefab, hit, rotation);

        if (ExplosiveUpgrade) {
            DoExplosionOnHit(hit, rotation);
        }
    }

    // Enemy was hit.
    public void EnemyHit (GameObject obj, RaycastHit hit) {
        EnemyHealth enemyHealth = obj.GetComponent<EnemyHealth>();
        if (!Util.IsNull(enemyHealth)) {
            float modifiedDamage = Weapons.ModifiedDamage(damageMultiplier);
            // Debug.Log($"MODIFIED DAMAGE :: {modifiedDamage} :: {damageMultiplier}");
            enemyHealth.TakeDamage(
                Weapons.ModifiedDamage(damageMultiplier),
                hit
            );
        }
    }

    public void ReloadToMax () {
        ammo = maxAmmo;

        if (Weapons.IsEqual(weaponType, Weapons.Instance.SelectedWeapon)) {
            _HUD.UpdateAmmoCountText(ammo);
        }
    }

    public bool IsFullAmmo () {
        return Weapons.IsFullAmmo(ammo, maxAmmo);
    }

    public void DoParticleEffectOnHit (GameObject effectPrefab, RaycastHit hit, Quaternion rotation) {
        GameObject instantiatedEffect = Instantiate(effectPrefab, hit.point, rotation);
        Destroy(instantiatedEffect, impactEffectsDisplayTime);
    }

    public void DoExplosionOnHit (RaycastHit hit, Quaternion rotation) {
        Instantiate(explosivePrefab, hit.point, rotation);
    }

    public void DoEffects () {
        DoMuzzleFlash();
        DoMuzzleLight();
        DoLineRender();
    }

    public void DoAmmo () {
        if (!Weapons.IsInfiniteAmmo(maxAmmo)) {
            ammo--;
            _HUD.UpdateAmmoCountText(ammo);
        }
    }

    public virtual void DisableEffects () {
        if (Weapons.IsTimeToDisableEffects(rofTimer.CurrentTime, rofTimer.TimerDelay, effectsDisplayTime)) {
            muzzleLight.enabled = false;
            lineRender.enabled = false;
        }
    }

    private void DoMuzzleFlash () {
        muzzleFlash.Stop();
        muzzleFlash.Play();
    }

    private void DoMuzzleLight () {
        muzzleLight.enabled = true;
    }

    protected virtual void DoLineRender () {
        lineRender.enabled = true;
        lineRender.SetPosition(0, muzzleTransform.position);
    }

    /////////////////////////////////////////////////////////////////
    // G E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public Weapons.Enum GetWeaponType () {
        return weaponType;
    }

    public Weapons.Kind GetWeaponKind () {
        return weaponKind;
    }

    public string GetName () {
        return displayName.ToUpper();
    }

    public int GetAmmo () {
        return ammo;
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        InteractableEventHandler.InteractableInteracting +=
            OnInteractableInteracting;

        PurchasingEventHandler.UpgradePurchasingStateUpdatedEvent +=
            OnUpgradePurchasingStateUpdated;

        PlayerEventHandler.PlayerDeathEvent +=
            OnPlayerDeathEvent;
    }

    public void UnregisterEventHandlers () {
        InteractableEventHandler.InteractableInteracting -=
            OnInteractableInteracting;

        PurchasingEventHandler.UpgradePurchasingStateUpdatedEvent -=
            OnUpgradePurchasingStateUpdated;

        PlayerEventHandler.PlayerDeathEvent -=
            OnPlayerDeathEvent;
    }

    private void OnInteractableInteracting (bool isInteracting) {
        playerIsBusy = isInteracting;
    }

    private void OnUpgradePurchasingStateUpdated (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade, Purchasing.State state) {
        bool isWeapon = (weapon == weaponType);
        bool upgradePurchased = (state == Purchasing.State.PURCHASED);

        if (isWeapon && upgradePurchased) {
            WeaponUpgrade weaponUpgrade = upgradeLookup[upgrade];

            if (!weaponUpgrade.enabled) {
                weaponUpgrade.enabled = true;
            }
        }
    }

    private void OnPlayerDeathEvent () {
        enabled = false;
    }
}
