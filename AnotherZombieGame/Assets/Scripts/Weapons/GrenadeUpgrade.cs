using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GrenadeUpgrade : WeaponUpgrade {

    private Grenade _Grenade;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        this._Grenade = this.gameObject.GetComponent<Grenade>();
    }

    protected override void OnEnable () {
        this._Upgrade.DoUpgrade(this._Grenade);
    }

    protected override void OnDisable () {
        this._Upgrade.UndoUpgrade(this._Grenade);
    }

}
