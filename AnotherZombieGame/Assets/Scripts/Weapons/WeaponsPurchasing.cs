﻿using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class WeaponsPurchasing : Purchasing, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    // State
    // private State[] weaponsPurchasedState;

    // References
    private HUD _HUD;
    private WeaponsContainer _WeaponsContainer;

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Init () {
        base.Init ();

        this.purchasingState = new List<State>() {
            State.PURCHASED, // Default pistol
            State.LOCKED,
            State.LOCKED,
            State.LOCKED,
            State.LOCKED,
            State.LOCKED,
            State.LOCKED,
            State.LOCKED
        };

        for (int i = 1; i < this.purchasingState.Count; i++) {
            PurchasingEventHandler.WeaponPurchasingStateUpdated(
                (Weapons.Enum)i,
                this.purchasingState[i]
            );
        }

        PurchasingEventHandler.PurchasingSetupComplete();
    }

    protected override void SetupReferences () {
        base.SetupReferences();

        GameObject player = Util.GetPlayerByTag();
        this._HUD = Util.GetHUDByTag().GetComponent<HUD>();
        this._WeaponsContainer = player.GetComponentInChildren<WeaponsContainer>();
    }

    // Check if specified weapon is purchasable and purchase if true.
    public void PurchaseWeapon (Weapons.Enum weapon) {
        bool notPurchased = !this.IsWeaponPurchased(weapon);
        bool purchasable = Weapons.IsPurchasable(weapon, this._PlayerStats.Brains);

        if (notPurchased && purchasable) {
            PurchasingEventHandler.WeaponPurchased(weapon);
            this._PlayerStats.UseBrains(weapon);

            if (weapon != Weapons.Enum.GRENADE) {
                this._WeaponsContainer.SelectPurchasedWeapon(weapon);
            }
        }
    }

    // Get the purchased state of a specified weapon.
    public bool IsWeaponPurchased (Weapons.Enum weapon) {
        return Weapons.IsPurchased(weapon, this.purchasingState);
    }

    public bool IsWeaponPurchasable (Weapons.Enum weapon) {
        return Weapons.IsPurchasable(weapon, this.purchasingState);
    }

    public bool IsWeaponLocked (Weapons.Enum weapon) {
        return Weapons.IsLocked(weapon, this.purchasingState);
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public override void RegisterEventHandlers () {
        base.RegisterEventHandlers();

        PurchasingEventHandler.WeaponPurchasingStateLockedEvent +=
            this.OnPurchaseStateLocked;
        PurchasingEventHandler.WeaponPurchasingStatePurchasableEvent +=
            this.OnPurchaseStatePurchasable;
        PurchasingEventHandler.WeaponPurchasingStatePurchasedEvent +=
            this.OnPurchaseStatePurchased;
    }

    public override void UnregisterEventHandlers () {
        base.UnregisterEventHandlers();

        PurchasingEventHandler.WeaponPurchasingStateLockedEvent -=
            this.OnPurchaseStateLocked;
        PurchasingEventHandler.WeaponPurchasingStatePurchasableEvent -=
            this.OnPurchaseStatePurchasable;
        PurchasingEventHandler.WeaponPurchasingStatePurchasedEvent -=
            this.OnPurchaseStatePurchased;
    }

    private void OnPurchaseStateLocked (Weapons.Enum weapon) {
        State weaponState = this.purchasingState[(int)weapon];

        if (weaponState == State.PURCHASABLE) {
            this.purchasingState[(int)weapon] = State.LOCKED;
            PurchasingEventHandler.WeaponPurchasingStateUpdated(weapon, State.LOCKED);
        }
    }

    private void OnPurchaseStatePurchasable (Weapons.Enum weapon) {
        State weaponState = this.purchasingState[(int)weapon];

        if (weaponState == State.LOCKED) {
            this.purchasingState[(int)weapon] = State.PURCHASABLE;
            PurchasingEventHandler.WeaponPurchasingStateUpdated(weapon, State.PURCHASABLE);
        }
    }

    private void OnPurchaseStatePurchased (Weapons.Enum weapon) {
        this.purchasingState[(int)weapon] = State.PURCHASED;
        PurchasingEventHandler.WeaponPurchasingStateUpdated(weapon, State.PURCHASED);
    }

    // @PPI - Casting int to Enum, then Enum to int.
    // Possibly only need int value.
    protected override void OnPlayerStatsBrainsUpdated (int brains) {
        for (int i = Weapons.GetWeaponPricesLength() - 1; i >= 1; i--) {
            int price = Weapons.GetWeaponPrice(i);
            Weapons.Enum weapon = (Weapons.Enum)i;

            if (brains >= price) {
                PurchasingEventHandler.WeaponPurchasable(weapon);
                continue;
            }

            PurchasingEventHandler.WeaponLocked(weapon);
        }
    }
}
