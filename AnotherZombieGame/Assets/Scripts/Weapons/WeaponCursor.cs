﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class WeaponCursor : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    #pragma warning disable 0649
    [SerializeField] private Texture2D cursorTexture;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        // Cursor.lockState = CursorLockMode.Locked;
        // Cursor.visible = false;
        this.SetCrosshair();
    }

    private void OnEnable () {
        this.SetCrosshair();
    }

    public void ResetCursor () {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public void SetCrosshair () {
        Cursor.SetCursor(cursorTexture, new Vector2(10, 50), CursorMode.ForceSoftware);
    }

    public void ToggleCrosshair (bool toggle) {
        if (toggle) {
            this.SetCrosshair();
        } else {
            this.ResetCursor();
        }
    }
}
