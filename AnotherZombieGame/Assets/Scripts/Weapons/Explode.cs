﻿using UnityEngine;
using Novasloth;

// Novasloth Games LLC
// Lee Barton
public class Explode : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public GameObject ClusterUpgrade { get; set; } = null;

    // @TODO just replace this with Weapons Singleton
    public float RadiusUpgrade { get; set; } = 1.0f; // Multiplier

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float explosionDelay = 5.0f;
    [SerializeField] private float explosionRadius = 5.0f;
    [SerializeField] private float explosionForce = 1000.0f;
    [SerializeField] private float explosionDamageMultiplier = 4.0f;
    [SerializeField] private bool explodeOnCollision = false;
    [SerializeField] private int clusterCount = 5;

    [SerializeField] private GameObject explosionEffect;
    [SerializeField] private float effectDisplayTime = 3.0f;

    private Timer timer;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        timer = new Timer(explosionDelay, () => {
            DoExplosion();
            Destroy(gameObject);
        });
    }

    private void Start () {
        timer.Start();
    }

    private void Update () {
        timer.Tick();
    }

    private void OnCollisionEnter (Collision collision) {
        if (explodeOnCollision && enabled) {
            DoExplosion();
            Destroy(gameObject);
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void DoExplosionAtPosition (Vector3 position) {
        transform.position = position;
        DoExplosion();
    }

    private void DoExplosion () {
        DoEffects();
        DoDamage();
    }

    private void DoEffects () {
        GameObject effect = Instantiate(explosionEffect, transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));
        Destroy(effect, effectDisplayTime);
    }

    private void DoDamage () {
        float radius = GetExplosionRadius();

        Collider[] colliders = Physics.OverlapSphere(
            transform.position,
            radius
        );

        foreach (Collider collider in colliders) {
            GameObject objectInRange = collider.transform.gameObject;
            float distanceFromCenter = Vector3.Distance(transform.position, objectInRange.transform.position);

            if (distanceFromCenter <= radius) {
                // Check for enemy
                if (objectInRange.tag == Tag.ENEMY) { // Do damage
                    EnemyHealth _EnemyHealth = objectInRange.GetComponent<EnemyHealth>();
                    if (!Util.IsNull(_EnemyHealth)) {
                        Debug.Log("Explode :: _EnemyHealth :: Before TakeExplosionDamage...");
                        _EnemyHealth.TakeExplosionDamage(
                            Weapons.ModifiedDamage(explosionDamageMultiplier + Weapons.Instance.ExplosionDamageMultiplier),
                            distanceFromCenter,
                            radius,
                            objectInRange
                        );
                    }
                } else { // Check for destructables
                    IDestructable _Destructable = objectInRange.GetComponentInParent<IDestructable>();
                    if (!Util.IsNull(_Destructable)) {
                        _Destructable.DoDestruction(
                            Weapons.ModifiedDamage(explosionDamageMultiplier + Weapons.Instance.ExplosionDamageMultiplier),
                            distanceFromCenter,
                            radius
                        );
                    }
                }
            }
        }

        CheckClusterExplosion();
        SimulateExplosion();
    }

    private void CheckClusterExplosion () {
        if (!Util.IsNull(ClusterUpgrade)) {
            Vector3 spawnPos;
            for (int i = 0; i < clusterCount; i++) {
                spawnPos = transform.position + (Random.insideUnitSphere * 1.20f);

                if (spawnPos.y <= 0.1f) {
                    spawnPos.Set(spawnPos.x, 0.25f, spawnPos.z);
                }

                GameObject clusterNade = Instantiate(ClusterUpgrade, spawnPos, ClusterUpgrade.transform.rotation);

                Explode _Explode = clusterNade.GetComponent<Explode>();
                if (!Util.IsNull(_Explode)) {
                    _Explode.RadiusUpgrade = RadiusUpgrade;
                }
            }
        }
    }

    private void SimulateExplosion () {
        Physics.SyncTransforms();

        float radius = GetExplosionRadius();

        Collider[] colliders = Physics.OverlapSphere(
            transform.position,
            radius
        );

        foreach (Collider collider in colliders) {
            Rigidbody rigidbody = collider.GetComponent<Rigidbody>();
            if (rigidbody != null) {
                rigidbody.AddExplosionForce(
                    explosionForce,
                    transform.position,
                    radius
                );
            }
        }
    }

    private float GetExplosionRadius () {
        // Debug.Log($"Explosion Radius: {explosionRadius} * RadiusUpgrade: {RadiusUpgrade} = {explosionRadius * RadiusUpgrade} ... Instance: {Weapons.Instance.ExplosionRadiusMultiplier}");
        return (explosionRadius * RadiusUpgrade);
    }
}
