﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class GrenadeThrow : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private new Rigidbody rigidbody;
    private Transform playerTransform;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.SetupReferences();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        this.rigidbody = this.GetComponent<Rigidbody>();

        GameObject player = Util.GetPlayerByTag();
        if (!Util.IsNull(player)) {
            this.playerTransform = player.transform;
        }
    }

    public void Throw () {
        this.rigidbody.AddForce(this.playerTransform.forward * 2000);
    }
}
