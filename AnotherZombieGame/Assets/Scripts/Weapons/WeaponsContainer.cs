﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class WeaponsContainer : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Transform playerHand;
    [SerializeField] private Transform playerLeftHand;
    [SerializeField] private Weapons.Enum selectedWeapon;

    private Vector3 containerLocalPosition;
    private Quaternion containerLocalRotation;

    private HUD _HUD;
    private WeaponsPurchasing _WeaponsPurchasing;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.RegisterEventHandlers();
        this.Init();
        this.SetupReferences();
    }

    private void Start () {
        this.UpdateRelatedWeaponComponents(Weapons.Enum.PISTOL);
    }

    private void Update () {
        this.ListenForWeaponSelection();
    }

    private void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.transform.SetParent(this.playerHand);
        this.containerLocalPosition = this.transform.localPosition;
        this.containerLocalRotation = this.transform.localRotation;
    }

    private void SetupReferences () {
        GameObject HUDGameObject = GameObject.FindGameObjectWithTag(Tag.HUD);
        if (!Util.IsNull(HUDGameObject)) {
            _HUD = HUDGameObject.GetComponent<HUD>();
        }

        _WeaponsPurchasing = GetComponent<WeaponsPurchasing>();
    }

    // Get the specific child weapon gameobject.
    private GameObject GetWeaponByType (Weapons.Enum weapon) {
        return this.transform.GetChild((int)weapon).gameObject;
    }

    // Handle weapon switching with NUM keys.
    // @PPI - Use switch statement
    private void ListenForWeaponSelection () {
        // Key 1
        if (Input.GetKeyDown(KeyCode.Alpha1)) { // PISTOL
            this.SelectWeapon(Weapons.Enum.PISTOL);
        }
        // Key 2
        if (Input.GetKeyDown(KeyCode.Alpha2)) { // ADVANCED PISTOL
            this.SelectWeapon(Weapons.Enum.ADVANCED_PISTOL);
        }
        // Key 3
        if (Input.GetKeyDown(KeyCode.Alpha3)) { // ASSAULT RIFLE
            this.SelectWeapon(Weapons.Enum.ASSAULT_RIFLE);
        }
        // Key 4
        if (Input.GetKeyDown(KeyCode.Alpha4)) { // SHOTGUN
            this.SelectWeapon(Weapons.Enum.SHOTGUN);
        }
        // Key 5
        if (Input.GetKeyDown(KeyCode.Alpha5)) { // SMG
            this.SelectWeapon(Weapons.Enum.SUBMACHINE_GUN);
        }
        // Key 6
        if (Input.GetKeyDown(KeyCode.Alpha6)) { // LMG
            this.SelectWeapon(Weapons.Enum.LIGHT_MACHINE_GUN);
        }
        // Key 8
        if (Input.GetKeyDown(KeyCode.Alpha8)) { // ROCKET LAUNCHER
            this.SelectWeapon(Weapons.Enum.ROCKET_LAUNCHER);
        }
    }

    // Select the specified weapon if possible.
    private void SelectWeapon (Weapons.Enum weapon) {
        bool notAlreadySelected = !Weapons.IsEqual(this.selectedWeapon, weapon);
        bool isPurchased = this._WeaponsPurchasing.IsWeaponPurchased(weapon);

        if (notAlreadySelected && isPurchased) {
            this.GetWeaponByType(this.selectedWeapon).SetActive(false);
            GameObject weaponObject = this.GetWeaponByType(weapon);
            weaponObject.SetActive(true);
            this.selectedWeapon = weapon;
            Weapons.Instance.SelectedWeapon = weapon;
            this.UpdateRelatedWeaponComponents(weapon);
        }
    }

    // Update related components - HUD, Animation, etc.
    private void UpdateRelatedWeaponComponents (Weapons.Enum type) {
        // @PPI Use reflection to use single variable of two possible types to call HUD and holdingKind methods?
        Weapon _Weapon = GetWeaponByType(type).GetComponent<Weapon>();

        Weapons.Instance.SetPlayerIsHoldingKind(
            Util.IsNull(_Weapon) ? Weapons.Kind.PISTOL : _Weapon.GetWeaponKind()
        );

        if (!Util.IsNull(_HUD)) {
            _HUD.SetSelectedWeapon(_Weapon);
        }
    }

    // Switch to a recently purchased weapon.
    public void SelectPurchasedWeapon (Weapons.Enum weaponPurchased) {
        this.SelectWeapon(weaponPurchased);
    }

    // Get the currently selected weapon.
    public Weapon GetEquippedWeapon () {
        return this.transform.GetChild((int)this.selectedWeapon).GetComponent<Weapon>();
    }

    public Grenade GetGrenade () {
        return this.transform.GetChild((int)Weapons.Enum.GRENADE).GetComponent<Grenade>();
    }

    // Reload the currently equipped weapon
    public bool ReloadWeapon () {
        Weapon weapon = this.GetEquippedWeapon();
        return this.ReloadWeapon(weapon);
    }

    public bool ReloadWeapon (Weapon weapon) {
        if (weapon.IsFullAmmo()) { return false; }

        weapon.ReloadToMax();

        return true;
    }

    // Reload all weapons
    // @PPI - Get all weapon scripts on load of game instead of
    // every time we reload
    public bool ReloadAllWeapons () {
        bool reloaded = false;
        foreach (Transform weaponTransform in this.transform) {
            Weapon weapon = weaponTransform.GetComponent<Weapon>();

            if (Util.IsNull(weapon)) {
                weapon = weaponTransform.GetComponent<RocketLauncher>();
            }

            if (Util.IsNull(weapon)) {
                continue;
            }

            reloaded = this.ReloadWeapon(weapon);
        }

        return reloaded;
    }

    public bool ReloadGrenade () {
        Grenade grenade = this.GetGrenade();

        if (grenade != null) {
            if (grenade.IsFullAmmo()) {
                return false;
            } else {
                grenade.ReloadToMax();
                return true;
            }
        }
        return false;
    }

    public void SetParentToRightHand () {
        this.transform.SetParent(this.playerHand);
        this.transform.localPosition = this.containerLocalPosition;
        this.transform.localRotation = this.containerLocalRotation;
    }

    // Switch parent to left hand while throwing.
    public void SetParentToLeftHand () {
        this.transform.SetParent(this.playerLeftHand);
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent +=
            this.OnPlayerDeathEvent;
    }

    public void UnregisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent -=
            this.OnPlayerDeathEvent;
    }

    private void OnPlayerDeathEvent () {
        this.GetEquippedWeapon().gameObject.SetActive(false);
        this.enabled = false;
    }
}
