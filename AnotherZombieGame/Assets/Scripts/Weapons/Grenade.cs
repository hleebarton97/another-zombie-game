﻿using UnityEngine;
using Novasloth;
using System.Collections.Generic;

// Novasloth Games LLC
// Lee Barton
public class Grenade : MonoBehaviour, IThrowable, IEventRegisterable {

    private const Weapons.Enum WEAPON_TYPE = Weapons.Enum.GRENADE;

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public Weapons.Enum WeaponType { get { return WEAPON_TYPE; } }

    public int MaxAmmo {
        get { return maxAmmo; }
        set { maxAmmo = value; }
    }

    public bool ExplosiveRadiusUpgrade { get { return Weapons.Instance.ExplosionRadiusMultiplier > 1.0f; } }
    public bool ClusterUpgrade { get; set; } = false;

    public Dictionary<WeaponUpgrade.Enum, WeaponUpgrade> UpgradeLookup {
        get { return upgradeLookup; }
    }

    public List<WeaponUpgrade.Enum> Upgrades { get; private set; } = new List<WeaponUpgrade.Enum>();

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    public static Weapons.Enum TYPE = Weapons.Enum.GRENADE;
    public static Weapons.Kind KIND = Weapons.Kind.THROWABLE;

    /////////////////////////////////////////////////////////////////
    // I N T E R F A C E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject grenade;
    [SerializeField] private Transform playerHand;

    [SerializeField] private int maxAmmo = 5;
    [SerializeField] private int ammo = 5;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private bool playerIsBusy;

    private HUD _HUD;
    private WeaponsPurchasing _WeaponsPurchasing;
    private WeaponsContainer _WeaponsContainer;

    // Upgrade lookup
    private Dictionary<WeaponUpgrade.Enum, WeaponUpgrade> upgradeLookup;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        Init();
        SetupReferences();
    }

    private void Start () {
        RegisterEventHandlers();
        _HUD.UpdateGrenadeAmmoCountText(ammo);
    }

    // Update is called once per frame
    private void Update () {
        CheckThrow();
    }

    private void OnDestroy () {
        UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        // Ammo
        ammo = maxAmmo;

        // Lookups
        upgradeLookup = new Dictionary<WeaponUpgrade.Enum, WeaponUpgrade>();
        WeaponUpgrade[] weaponUpgrades = GetComponents<WeaponUpgrade>();

        if (weaponUpgrades.Length > 0) {
            foreach (WeaponUpgrade weaponUpgrade in weaponUpgrades) {
                Upgrades.Add(weaponUpgrade.Type);
                upgradeLookup.Add(weaponUpgrade.Type, weaponUpgrade);
            }
        }
    }

    private void SetupReferences () {
        GameObject player = Util.GetPlayerByTag();

        _HUD = player.GetComponentInChildren<HUD>();
        _WeaponsPurchasing = player.GetComponentInChildren<WeaponsPurchasing>();
        _WeaponsContainer = player.GetComponentInChildren<WeaponsContainer>();
    }

    public void CheckThrow () {
        if (Input.GetButtonDown("Fire2")) {
            bool notEmpty = Weapons.IsNotEmpty(ammo);
            bool isPurchased = _WeaponsPurchasing.IsWeaponPurchased(TYPE);
            if (isPurchased && notEmpty && !playerIsBusy) {
                Throw();
            }
        }
    }

    public void Throw () {
        if (!Movement.Instance.PlayerIsThrowing) {
            Movement.Instance.PlayerIsThrowing = true; // Start throwing animation
            _WeaponsContainer.SetParentToLeftHand();
            DoAmmo();
        }
    }

    private void InstantiateAndThrowGrenade () {
        GameObject newGrenade = Instantiate(
            grenade,
            playerHand.transform.position,
            playerHand.transform.rotation
        );

        Explode _Explode = newGrenade.GetComponent<Explode>();
        if (ExplosiveRadiusUpgrade || ClusterUpgrade) {
            if (!Util.IsNull(_Explode)) {
                _Explode.RadiusUpgrade = Weapons.Instance.ExplosionRadiusMultiplier;
            }
        }

        if (ClusterUpgrade) {
            if (!Util.IsNull(_Explode)) {
                _Explode.ClusterUpgrade = grenade;
            }
        }

        newGrenade.GetComponent<GrenadeThrow>().Throw();
    }

    public bool IsFullAmmo () {
        return Weapons.IsFullAmmo(ammo, maxAmmo);
    }

    public void ReloadToMax () {
        ammo = maxAmmo;
        _HUD.UpdateGrenadeAmmoCountText(ammo);
    }

    private void DoAmmo () {
        if (!Weapons.IsInfiniteAmmo(maxAmmo)) {
            ammo--;
            _HUD.UpdateGrenadeAmmoCountText(ammo);
        }
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        AnimationEventHandler.AnimationPlayerThrowingThrow +=
            OnThrowingAnimationThrow;
        AnimationEventHandler.AnimationPlayerThrowingEnd +=
            OnThrowingAnimationEnd;

        InteractableEventHandler.InteractableInteracting +=
            OnInteractableInteracting;

        PurchasingEventHandler.UpgradePurchasingStateUpdatedEvent +=
            OnUpgradePurchasingStateUpdated;

        PlayerEventHandler.PlayerDeathEvent +=
            OnPlayerDeathEvent;
    }

    public void UnregisterEventHandlers () {
        AnimationEventHandler.AnimationPlayerThrowingThrow -=
            OnThrowingAnimationThrow;
        AnimationEventHandler.AnimationPlayerThrowingEnd -=
            OnThrowingAnimationEnd;

        InteractableEventHandler.InteractableInteracting -=
            OnInteractableInteracting;

        PurchasingEventHandler.UpgradePurchasingStateUpdatedEvent -=
            OnUpgradePurchasingStateUpdated;

        PlayerEventHandler.PlayerDeathEvent -=
            OnPlayerDeathEvent;
    }

    // Handle throwing animation throw event.
    private void OnThrowingAnimationThrow (AnimationEventHandler handler) {
        InstantiateAndThrowGrenade();
    }

    private void OnThrowingAnimationEnd (AnimationEventHandler handler) {
        _WeaponsContainer.SetParentToRightHand();
    }

    private void OnInteractableInteracting (bool isInteracting) {
        playerIsBusy = isInteracting;
    }

    private void OnUpgradePurchasingStateUpdated (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade, Purchasing.State state) {
        bool isWeapon = (weapon == WEAPON_TYPE);
        bool upgradePurchased = (state == Purchasing.State.PURCHASED);

        if (isWeapon && upgradePurchased) {
            WeaponUpgrade weaponUpgrade = upgradeLookup[upgrade];

            if (!weaponUpgrade.enabled) {
                weaponUpgrade.enabled = true;
            }
        }
    }

    private void OnPlayerDeathEvent () {
        enabled = false;
    }
}
