﻿using UnityEngine;
using Novasloth;

// Novasloth Games LLC
// Lee Barton
public class RocketLauncher : Weapon {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Rocket Specific")]
    [SerializeField] private float force = 2000f;
    [SerializeField] private GameObject rocketPrefab;
    [SerializeField] private Transform rocketPosition;

    private Rocket _Rocket;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Start () {
        base.Start();
        CheckSpawnRocket();
    }

    private void Update () {
        HandleTimer();
        CheckShoot();
    }

    private void LateUpdate () {
        CheckSpawnRocket();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        base.SetupReferences();
        playerTransform = GameObject.FindGameObjectWithTag(Tag.PLAYER).transform;
        _HUD = GameObject.FindGameObjectWithTag(Tag.HUD).GetComponent<HUD>();
        _Rocket = null;
    }

    private void CheckSpawnRocket () {
        // bool isTimeToLaunch = Util.TimerCheck(launchDelayTimer, launchDelay);
        bool rocketNotSpawned = Util.IsNull(_Rocket);
        bool hasAmmo = Weapons.IsNotEmpty(ammo);
        if (rocketNotSpawned && timeToShoot && hasAmmo) {
            GameObject newRocket = Instantiate(rocketPrefab, rocketPosition);
            _Rocket = newRocket.GetComponent<Rocket>();
            timeToShoot = false;
        }
    }

    private void HandleTimer() {
        rofTimer.TimerDelay = Weapons.RateOfFireToTimerDelay(rateOfFire);
        rofTimer.Tick();
    }

    public override void CheckShoot () {
        bool notEmpty = Weapons.IsNotEmpty(ammo);
        bool notNull = !Util.IsNull(_Rocket);
        bool notBusy = !playerIsBusy;

        // Debug.Log($"Not Empty: {notEmpty}, Not Null: {notNull}, Not Busy: {notBusy}");
        if (notEmpty && notNull && notBusy) {
            bool isNotThrowing = !Movement.Instance.PlayerIsThrowing;
            if (isAuto) {
                if (Input.GetButton("Fire1") && timeToShoot && isNotThrowing) {
                    Shoot();
                }
            } else {
                // Debug.Log($"Timer: {rofTimer.CurrentTime}, Delay: {rofTimer.TimerDelay}");
                // Debug.Log($"Time To Shoot: {timeToShoot}");
                if (Input.GetButtonDown("Fire1") && timeToShoot && isNotThrowing) {
                    Shoot();
                }
            }
        }
    }

    public override void Shoot () {
        // DoEffects?
        DoAmmo();
        _Rocket.Fire(
            spread,
            force,
            playerTransform,
            Weapons.Instance.ExplosionRadiusMultiplier
        );
        _Rocket = null;
    }
}
