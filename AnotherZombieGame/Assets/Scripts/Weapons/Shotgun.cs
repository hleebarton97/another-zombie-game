﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class Shotgun : Weapon {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Shotgun Specific")]
    [SerializeField] private int projectileCount = 8;

    private LineRenderer[] lineRenderers;

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Init () {
        base.Init();
        this.SetupBuckshotLineRenderers();
    }

    public override void Shoot () {
        if (Weapons.IsNotEmpty(ammo)) {
            // Effects
            this.DoEffects();
            // Ammo
            this.DoAmmo();

            // Check hit
            RaycastHit weaponHit;
            Ray weaponRay;
        
            for (int i = 0; i < projectileCount; i++) {
                weaponRay = Weapons.CreateWeaponRay(
                    this.spread,
                    this.range,
                    this.muzzleTransform.position,
                    this.playerTransform
                );

                if (Weapons.RayHitShootableMaskLayer(weaponRay, out weaponHit, this.range)) {
                    this.lineRenderers[i].SetPosition(1, weaponHit.point);
                    this.CheckHit(weaponHit, Quaternion.LookRotation(weaponHit.normal));
                } else {
                    this.lineRenderers[i].SetPosition(1, weaponRay.origin + (weaponRay.direction * this.range));
                }
            }

            // Reset timer
            this.timeToShoot = false;
            this.rofTimer.Reset();
        }
    }

    private void SetupBuckshotLineRenderers () {
        this.lineRenderers = new LineRenderer[this.projectileCount];
        this.lineRenderers[0] = this.lineRender;
        for (int i = 1; i < this.lineRenderers.Length; i++) {
            LineRenderer line = (new GameObject("line" + i)).AddComponent<LineRenderer>();
            line.transform.SetParent(this.muzzleTransform);
            line.useWorldSpace = true;
            line.material = this.lineRender.material;
            line.startWidth = this.lineRender.startWidth;
            line.endWidth = this.lineRender.endWidth;
            line.enabled = false;
            this.lineRenderers[i] = line;
        }
    }

    public override void DisableEffects () {
        if (Weapons.IsTimeToDisableEffects(this.rofTimer.CurrentTime, this.rofTimer.TimerDelay, this.effectsDisplayTime)) {
            this.muzzleLight.enabled = false;
            foreach (LineRenderer line in this.lineRenderers) {
                line.enabled = false;
            }
        }
    }

    protected override void DoLineRender () {
        foreach (LineRenderer line in this.lineRenderers) {
            line.enabled = true;
            line.SetPosition(0, this.muzzleTransform.position);
        }
    }
}
