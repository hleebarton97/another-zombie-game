﻿using NUnit.Framework;
using UnityEngine;

// <Studio Name>
// Lee Barton 
public class RotationTests {

    private Vector3 inputMousePos;
    private float maxDistance;
    private int layerMask;

    [SetUp]
    public void Init () {
        this.maxDistance = 100f;
        this.layerMask = LayerMask.GetMask(
            LayerMask.LayerToName((int)Layer.GROUND_MASK)
        );
    }

    //
    [Test, Description("Test that Rotation check for ray from mouse to camera hits specified layer mask.")]
    [Author("Lee Barton")]
    public void MouseToCameraRayHitMaskLayer_Hit() {
        RaycastHit raycastHitInfo;
        this.inputMousePos = new Vector3(500f, 200f, 0f);

        bool actualHitResult = Rotation.MouseToCameraRayHitMaskLayer(
            this.inputMousePos,
            out raycastHitInfo,
            this.maxDistance,
            this.layerMask
        );

        Assert.That(actualHitResult, Is.True);
    }

    //
    [Test, Description("Test that Rotation check for ray from mouse to camera does not hit specified layer mask.")]
    [Author("Lee Barton")]
    public void MouseToCameraRayHitMaskLayer_Does_Not_Hit () {
        RaycastHit raycastHitInfo;
        this.inputMousePos = new Vector3(5000f, 5000f, 0f);

        bool actualHitResult = Rotation.MouseToCameraRayHitMaskLayer(
            this.inputMousePos,
            out raycastHitInfo,
            this.maxDistance,
            this.layerMask
        );

        Assert.That(actualHitResult, Is.False);
    }

    //
    [Test, Description("Test that Rotation calculates a rotation towards the mouse cursor.")]
    [Author("Lee Barton")]
    public void RotateToCursor_Calculates_Rotation_Facing_Cursor () {
        RaycastHit raycastHitInfo;
        this.inputMousePos = new Vector3(500f, 200f, 0f);
        Rotation.MouseToCameraRayHitMaskLayer(
            this.inputMousePos,
            out raycastHitInfo,
            this.maxDistance,
            this.layerMask
        );

        Vector3 position = new Vector3(0f, 0f, 0f);
        Vector3 playerToMouseCursor = raycastHitInfo.point - position;
        
        Quaternion expectedQuaternion = Quaternion.LookRotation(playerToMouseCursor);
        Quaternion actualQuaternion = Rotation.RotateToCursor(position, raycastHitInfo);

        Assert.That(
            Mathf.Approximately(Mathf.Abs(Quaternion.Dot(actualQuaternion, expectedQuaternion)), 1.0f), // Adjust for floating point precision errors
            Is.True
        );
    }
}
