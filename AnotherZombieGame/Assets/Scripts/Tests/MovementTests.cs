﻿using System;
using NUnit.Framework;
using UnityEngine;

// <Studio Name>
// Lee Barton
public class MovementTests {

    private const float SPEED = 5f;

    private Vector3 currentPos;

    [SetUp]
    public void Init () {
        currentPos = new Vector3(1f, 1f, 1f);
    }

    //
    [Test, Description("Test that Movement calculation returns vector along x axis.")]
    [Author("Lee Barton")]
    public void CalculateWithInput_Horizontal() {
        Vector3 expectedMovement = this.currentPos + new Vector3(1f, 0f, 0f).normalized * SPEED * Time.deltaTime;
        Vector3 actualMovement = Movement.CalculateWithInput(1f, 0f, this.currentPos, SPEED);

        Assert.That(actualMovement, Is.EqualTo(expectedMovement));
    }

    //
    [Test, Description("Test that Movement calculation returns vector along z axis.")]
    [Author("Lee Barton")]
    public void CalculateWithInput_Vertical () {
        Vector3 expectedMovement = this.currentPos + new Vector3(0f, 0f, 1f).normalized * SPEED * Time.deltaTime;
        Vector3 actualMovement = Movement.CalculateWithInput(0f, 1f, this.currentPos, SPEED);

        Assert.That(actualMovement, Is.EqualTo(expectedMovement));
    }

    //
    [Test, Description("Test that Movement updates player sprint speed.")]
    [Author("Lee Barton")]
    public void SetSprintSpeed () {
        float expectedSpeed = SPEED + Movement.SPRINT_DIFF;
        float expectedDiff = Movement.SPRINT_DIFF;

        Movement.Instance.SetSprintSpeed(SPEED);
        float actualSpeed = Movement.Instance.PlayerCurrentSpeed;
        float actualDiff = actualSpeed - SPEED;

        Assert.That(actualSpeed, Is.EqualTo(expectedSpeed));
        Assert.That(Math.Round(actualDiff, 2), Is.EqualTo(Math.Round(expectedDiff, 2)));
    }

    //
    [Test, Description("Test that Movement walking test returns true when positive vertical input axis.")]
    [Author("Lee Barton")]
    public void Walking_Is_True () {
        Assert.That(Movement.IsWalking(1f), Is.True);
    }

    //
    [Test, Description("Test that Movement walking test returns false when not positive vertical input axis.")]
    [Author("Lee Barton")]
    public void Walking_Is_False () {
        Assert.That(Movement.IsWalking(0f), Is.False);
    }

    //
    [Test, Description("Test that Movement walking backward test returns true when negative vertical input axis.")]
    [Author("Lee Barton")]
    public void WalkingBackward_Is_True () {
        Assert.That(Movement.IsWalkingBackward(-1f), Is.True);
    }

    //
    [Test, Description("Test that Movement walking backward test returns true when not negative vertical input axis.")]
    [Author("Lee Barton")]
    public void WalkingBackward_Is_False () {
        Assert.That(Movement.IsWalkingBackward(0f), Is.False);
    }

    //
    [Test, Description("Test that Movement strafing left test returns true when negative horizontal input axis.")]
    [Author("Lee Barton")]
    public void StrafingLeft_Is_True () {
        Assert.That(Movement.IsStrafingLeft(-1f), Is.True);
    }

    //
    [Test, Description("Test that Movement strafing left test returns false when not negative horizontal input axis.")]
    [Author("Lee Barton")]
    public void StrafingLeft_Is_False () {
        Assert.That(Movement.IsStrafingLeft(0f), Is.False);
    }

    //
    [Test, Description("Test that Movement strafing right test returns true when positive horizontal input axis.")]
    [Author("Lee Barton")]
    public void StrafingRight_Is_True () {
        Assert.That(Movement.IsStrafingRight(1f), Is.True);
    }

    //
    [Test, Description("Test that Movement strafing right test returns false when not positive horizontal input axis.")]
    [Author("Lee Barton")]
    public void StrafingRight_Is_False () {
        Assert.That(Movement.IsStrafingRight(0f), Is.False);
    }

    //
    [Test, Description("Test that Movement sprinting test returns true when lShift is held down.")]
    [Author("Lee Barton")]
    public void Sprinting_With_lShift_Is_True () {
        Assert.That(Movement.IsSprinting(true, false), Is.True);
    }

    //
    [Test, Description("Test that Movement sprinting test returns true when rShift is held down.")]
    [Author("Lee Barton")]
    public void Sprinting_With_rShift_Is_True () {
        Assert.That(Movement.IsSprinting(false, true), Is.True);
    }

    //
    [Test, Description("Test that Movement sprinting test returns false when either shift key is not held down.")]
    [Author("Lee Barton")]
    public void Sprinting_Is_False () {
        Assert.That(Movement.IsSprinting(false, false), Is.False);
    }
}
