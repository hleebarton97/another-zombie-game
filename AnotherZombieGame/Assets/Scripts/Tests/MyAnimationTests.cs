﻿using NUnit.Framework;

// <Studio Name>
// Lee Barton
public class MyAnimationTests {

    // Movement
    private const string IS_WALKING = "IsWalking";
    private const string IS_WALKING_BACK = "IsWalkingBack";
    private const string IS_STRAFING_LEFT = "IsStrafingLeft";
    private const string IS_STRAFING_RIGHT = "IsStrafingRight";
    private const string IS_SPRINTING = "IsSprinting";

    private const string IS_SLOW = "IsSlow";

    // Type
    private const string HAS_PISTOL = "HasPistol";
    private const string HAS_RIFLE = "HasRifle";

    /*[Test, Description("Test that chance of animation type is TRUE.")]
    [Author("Lee Barton")]
    public void TypeBasedOnChance_True () {
        float chance = 1.0f;

        Assert.That(
            MyAnimation.TypeBasedOnChance(chance),
            Is.True
        );
    }

    [Test, Description("Test that chance of animation type is FALSE.")]
    [Author("Lee Barton")]
    public void TypeBasedOnChance_False () {
        float chance = 0.0f;

        Assert.That(
            MyAnimation.TypeBasedOnChance(chance),
            Is.False
        );
    }*/

    [Test, Description("Test that MyAnimation controller variable for walking matches.")]
    [Author("Lee Barton")]
    public void IS_WALKING_MATCHES () {
        Assert.That(
            MyAnimation.IS_WALKING,
            Is.EqualTo(IS_WALKING)
        );
    }

    [Test, Description("Test that MyAnimation controller variable for walking back matches.")]
    [Author("Lee Barton")]
    public void IS_WALKING_BACK_MATCHES () {
        Assert.That(
            MyAnimation.IS_WALKING_BACK,
            Is.EqualTo(IS_WALKING_BACK)
        );
    }

    [Test, Description("Test that MyAnimation controller variable for strafing left matches.")]
    [Author("Lee Barton")]
    public void IS_STRAFING_LEFT_MATCHES () {
        Assert.That(
            MyAnimation.IS_STRAFING_LEFT,
            Is.EqualTo(IS_STRAFING_LEFT)
        );
    }

    [Test, Description("Test that MyAnimation controller variable for strafing right matches.")]
    [Author("Lee Barton")]
    public void IS_STRAFING_RIGHT_MATCHES () {
        Assert.That(
            MyAnimation.IS_STRAFING_RIGHT,
            Is.EqualTo(IS_STRAFING_RIGHT)
        );
    }

    [Test, Description("Test that MyAnimation controller variable for sprinting matches.")]
    [Author("Lee Barton")]
    public void IS_SPRINTING_MATCHES () {
        Assert.That(
            MyAnimation.IS_SPRINTING,
            Is.EqualTo(IS_SPRINTING)
        );
    }

    [Test, Description("Test that MyAnimation controller variable for enemy slow movement.")]
    [Author("Lee Barton")]
    public void IS_SLOW_MATCHES () {
        Assert.That(
            MyAnimation.IS_SLOW,
            Is.EqualTo(IS_SLOW)
        );
    }

    [Test, Description("Test that MyAnimation controller variable for equipping a pistol matches.")]
    [Author("Lee Barton")]
    public void HAS_PISTOL_MATCHES () {
        Assert.That(
            MyAnimation.HAS_PISTOL,
            Is.EqualTo(HAS_PISTOL)
        );
    }

    [Test, Description("Test that MyAnimation controller variable for equipping a rifle matches.")]
    [Author("Lee Barton")]
    public void HAS_RIFLE_MATCHES () {
        Assert.That(
            MyAnimation.HAS_RIFLE,
            Is.EqualTo(HAS_RIFLE)
        );
    }
}
