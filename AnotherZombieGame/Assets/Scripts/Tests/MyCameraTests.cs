﻿using NUnit.Framework;
using UnityEngine;

// <Studio Name>
// Lee Barton
public class MyCameraTests {

    private Vector3 targetPos;
    private Vector3 camPos;
    private Vector3 camOffset;

    [SetUp]
    public void Init () {
        targetPos = new Vector3(0f, 0f, 0f);
        camPos = new Vector3(0f, 10f, -10f);
        camOffset = camPos - targetPos;
    }

    //
    [Test, Description("Test that MyCamera calculates the new smoothed position for the camera.")]
    [Author("Lee Barton")]
    public void CalculateNewPosition_Returns_Next_Smoothed_Position() {
        MyCamera.Instance.Smoothing = true;
        MyCamera.Instance.SmoothingMultiplier = 5f;

        Vector3 expectedNewPos = Vector3.Lerp(
            this.camPos,
            (this.targetPos + this.camOffset),
            (MyCamera.Instance.SmoothingMultiplier * Time.deltaTime)
        );

        Vector3 actualNewPos = MyCamera.Instance.CalculateNewPosition(
            this.camPos,
            this.targetPos,
            this.camOffset
        );
        
        Assert.That(actualNewPos, Is.EqualTo(expectedNewPos));
    }

    //
    [Test, Description("Test that MyCamera calculates the new, not smoothed, position for the camera.")]
    [Author("Lee Barton")]
    public void CalculateNewPosition_Returns_Next_Position () {
        MyCamera.Instance.Smoothing = false;

        Vector3 expectedNewPos = this.targetPos + this.camOffset;
        Vector3 actualNewPos = MyCamera.Instance.CalculateNewPosition(
            this.camPos,
            this.targetPos,
            this.camOffset
        );

        Assert.That(actualNewPos, Is.EqualTo(expectedNewPos));
    }

    //
    [Test, Description("Test that MyCamera updates smoothing boolean property to true when false.")]
    [Author("Lee Barton")]
    public void UpdateSmoothing_Sets_Smoothing_To_True () {
        MyCamera.Instance.Smoothing = false;

        MyCamera.Instance.UpdateSmoothing(true);

        Assert.That(MyCamera.Instance.Smoothing, Is.True);
    }

    //
    [Test, Description("Test that MyCamera updates smoothing boolean property to false when true.")]
    [Author("Lee Barton")]
    public void UpdateSmoothing_Sets_Smoothing_To_False () {
        MyCamera.Instance.Smoothing = true;

        MyCamera.Instance.UpdateSmoothing(false);

        Assert.That(MyCamera.Instance.Smoothing, Is.False);
    }

    //
    [Test, Description("Test that MyCamera does not update smoothing boolean property to same value.")]
    [Author("Lee Barton")]
    public void UpdateSmoothing_Does_Not_Update_Value () {
        MyCamera.Instance.Smoothing = true;

        MyCamera.Instance.UpdateSmoothing(true);

        Assert.That(MyCamera.Instance.Smoothing, Is.True);
    }

    //
    [Test, Description("Test that MyCamera updates smoothing multiplier float property.")]
    [Author("Lee Barton")]
    public void UpdateSmoothingMultiplier_Updates_Value () {
        float expectedValue = 2.0f;

        MyCamera.Instance.SmoothingMultiplier = 1.5f;
        MyCamera.Instance.UpdateSmoothingMultiplier(2.0f);

        Assert.That(MyCamera.Instance.SmoothingMultiplier, Is.EqualTo(expectedValue));
    }

    //
    [Test, Description("Test that MyCamera does not update smoothing multiplier when value is the same.")]
    [Author("Lee Barton")]
    public void UpdateSmoothingMultiplier_Does_Not_Update_Value () {
        float expectedValue = 2.0f;

        MyCamera.Instance.SmoothingMultiplier = 2.0f;
        MyCamera.Instance.UpdateSmoothingMultiplier(2.0f);

        Assert.That(MyCamera.Instance.SmoothingMultiplier, Is.EqualTo(expectedValue));
    }
}
