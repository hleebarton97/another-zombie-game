﻿using NUnit.Framework;

// <Studio Name>
// Lee Barton
public class EnemyTests {

    [Test, Description("Test that enemy is dead when health is at 0.")]
    [Author("Lee Barton")]
    public void IsDead_Health_At_Zero_True () {
        float health = 0.0f;
        Assert.That(
            Enemy.IsDead(health),
            Is.True
        );
    }

    [Test, Description("Test that enemy is dead when health is below 0.")]
    [Author("Lee Barton")]
    public void IsDead_Health_Below_Zero_True () {
        float health = -10.0f;
        Assert.That(
            Enemy.IsDead(health),
            Is.True
        );
    }

    [Test, Description("Test that enemy is NOT dead when health above 0.")]
    [Author("Lee Barton")]
    public void IsDead_False () {
        float health = 1.0f;
        Assert.That(
            Enemy.IsDead(health),
            Is.False
        );
    }

    [Test, Description("Test that damage dealt to enemy is correct.")]
    [Author("Lee Barton")]
    public void CalculateDamage () {
        float damage = 10.0f;
        float multiplier = 2.0f;

        float expectedDamage = multiplier * damage;
        float actualDamage = Enemy.CalculateDamage(damage, multiplier);

        Assert.That(actualDamage, Is.EqualTo(expectedDamage));
    }

    [Test, Description("Test that health after dealing damage is correct.")]
    [Author("Lee Barton")]
    public void HealthAfterDamage () {
        float health = 100.0f;
        float damage = 10.0f;
        float multiplier = 2.0f;

        float totalDamage = Enemy.CalculateDamage(damage, multiplier);

        float expectedHealth = health - totalDamage;
        float actualHealth = Enemy.HealthAfterDamage(health, damage, multiplier);

        Assert.That(actualHealth, Is.EqualTo(expectedHealth));
    }
}
