﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class Brains : Drop {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Brains Value")]
    [SerializeField] private int value;

    [SerializeField] private int minimumValue = 50;
    [SerializeField] private int maximumValue = 500;

    [Header("Brains Size")]
    [SerializeField] private float minimumSize = 0.5f;
    [SerializeField] private float maximumSize = 2.5f;
    [SerializeField] private int sizes = 5;

    private PlayerStats _PlayerStats;

	/////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Setup brains object.
    protected override void Init () {
        base.Init();
        this.SetValue();
        this.SetSize();
    }

    protected override void SetupReferences () {
        base.SetupReferences();

        GameObject player = Util.GetPlayerByTag();
        if (!Util.IsNull(player)) {
            this._PlayerStats = player.GetComponent<PlayerStats>();
        }
        
    }

    // Handle player collision with brains.
    protected override void HandleCollision (Collider collider) {
        if (collider.gameObject.tag == Tag.PLAYER) {
            _PlayerStats.AddBrains(this.value);
            // @TODO pickup sound effect
            Destroy(this.gameObject);
        }
    }

    /////////////////////////////////////////////////////////////////
    // S E T T E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Set brains to random value between minimum and maximum.
    private void SetValue () {
        float chance = Random.Range(0.0f, 1.0f);
        int lower = ((this.maximumValue - this.minimumValue) / 2) + this.minimumValue;
        
        if (chance <= 0.75f) {
            this.value = Random.Range(this.minimumValue, (lower + 1));
        } else {
            this.value = Random.Range(lower, (this.maximumValue + 1));
        }
    }

    //

    // Size size of brain based on value ranges.
    private void SetSize () {
        float sizeInterval = (this.maximumSize - this.minimumSize) / (this.sizes - 1);
        float valInterval = (this.maximumValue - this.minimumValue) / this.sizes;
        float maxValRange = this.minimumValue;
        float minValRange;
        float currentSize = this.minimumSize;

        for (int i = 0; i < this.sizes; i++) {
            minValRange = maxValRange;
            maxValRange = (minValRange + valInterval);

            if (this.value >= minValRange && this.value <= maxValRange) {
                this.transform.localScale = new Vector3(
                    currentSize,
                    currentSize,
                    currentSize
                );

                this.yPositionOffset = currentSize * Y_OFFSET_AT_ZERO;
                break;
            }

            currentSize += sizeInterval;
        }
    }
}
