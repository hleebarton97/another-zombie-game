using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class BossAmmo : Ammo {

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void UseAmmo (WeaponsContainer _WeaponsContainer) {
        bool reloadedAllWeapons = _WeaponsContainer.ReloadAllWeapons();
        bool reloadedGrenades = _WeaponsContainer.ReloadGrenade();

        if (reloadedAllWeapons || reloadedGrenades) {
            Destroy(this.gameObject);
        }
    }
}
