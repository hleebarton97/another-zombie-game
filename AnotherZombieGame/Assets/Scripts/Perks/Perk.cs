using UnityEngine;

// Lee Barton
// Novasloth Games LLC
public class Perk : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public enum Enum {
        QUIX,
        RNR,
        PAINDAY,
        OH_HURRY,
        SUIT,
        CAMO,
        KAREN,
        BRO
    }

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool IsActive { get { return isActive; } }
    public Enum PerkType { get { return _Perk.Type; } }
    public string Title { get { return _Perk.Title; } }
    public int Price { get { return _Perk.Price; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Perk")]
    [SerializeField] protected PerkSO _Perk;
    [SerializeField] protected bool isActive = false;

    private bool isActuallyActive = false;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected void Awake () {
        if (Util.IsNull(_Perk)) {
            Debug.LogError("Perk :: PerkSO is NULL! Disabling Perk...");
            enabled = false;
            return;
        }

        _Perk.Init();
    }

    protected void Update () {
        if (isActive && !isActuallyActive) {
            Activate();
        }

        if (!isActive && isActuallyActive) {
            Deactivate();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void Activate () {
        isActuallyActive = true;
        isActive = true;

        PerkEventHandler.Activated(this);

        _Perk.Activate();
    }

    public void Deactivate () {
        isActuallyActive = false;
        isActive = false;

        PerkEventHandler.Deactivated(this);

        _Perk.Deactivate();
    }

    public void Purchased () {
        Activate();
    }

    /////////////////////////////////////////////////////////////////
    // S T A T I C   H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public static bool IsPurchasable (Perk perk, int brains)
        => (brains >= perk.Price);
}
