using Novasloth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class CamoPerkAbility : MonoBehaviour, IPerkAbility, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private const KeyCode KEY_CODE_PERK_ACTIVATION = KeyCode.F;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private PerkSO playerPerkSO;
    [SerializeField] private GameObject baitPrefab;
    [SerializeField] private float baitThrowForce = 250.0f;
    [SerializeField] private float baitActiveTime = 5.0f;
    [SerializeField] private float abilityDelay = 5.0f;
    [SerializeField] private bool abilityPerRound = false;

    private GameObject baitInstance;
    private Bait _Bait;
    private Rigidbody baitRigidbody;
    private Timer baitTimer;
    private Timer abilityDelayTimer;

    private bool canUseAbility = true;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        RegisterEventHandlers();
        Init();
    }

    private void Start () {
        TryCreateBait();
        baitTimer.Start();
    }

    private void Update () {
        if (Input.GetKeyDown(KEY_CODE_PERK_ACTIVATION) && canUseAbility) {
            ActivateAbility();
        }

        baitTimer.Tick();
        abilityDelayTimer.Tick();
    }

    private void OnDestroy () {
        UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        if (Util.IsNull(playerPerkSO)) {
            Debug.LogError("CamoPerkAbility :: playerPerkSO is NULL! Disabling CamoPerkAbility...");
            enabled = false;
        }

        abilityDelayTimer = new Timer(abilityDelay, () => {
            if (!abilityPerRound) {
                canUseAbility = true;
                abilityDelayTimer.Stop();
            }
        });

        baitTimer = new Timer(baitActiveTime, () => {
            ResetBait();
            abilityDelayTimer.Start();
        });

        playerPerkSO.Init();
    }

    public void ActivateAbility () {
        canUseAbility = false;
        ThrowBait();
        PerkAbilityEventHandler.PerkAbilityCamoActivated(_Bait.BaitTransform);
        baitTimer.Reset();
    }

    private void TryCreateBait () {
        if (Util.IsNull(baitInstance)) {
            baitInstance = Instantiate(baitPrefab);
            _Bait = baitInstance.GetComponent<Bait>();
            baitRigidbody = baitInstance.GetComponent<Rigidbody>();
        }

        ResetBait();
    }

    public void ResetBait () {
        baitInstance.transform.parent = transform;
        baitInstance.SetActive(false);

        baitInstance.transform.localPosition = baitPrefab.transform.position;
        baitInstance.transform.localRotation = baitPrefab.transform.rotation;

        PerkAbilityEventHandler.PerkAbilityCamoDeactivated(_Bait.BaitTransform);
    }

    public void ThrowBait () {
        baitInstance.SetActive(true);
        baitInstance.transform.parent = null;

        baitRigidbody.AddForce(transform.forward * baitThrowForce);
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        GameEventHandler.GameNewRoundEvent +=
            OnGameNewRoundEvent;
    }

    public void UnregisterEventHandlers () {
        GameEventHandler.GameNewRoundEvent -=
           OnGameNewRoundEvent;
    }

    private void OnGameNewRoundEvent (int round) {
        if (abilityPerRound) {
            canUseAbility = true;
        }
    }
}