using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/PlayerInfo")]
public class PlayerInfoSO : ScriptableObject {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public GameObject Prefab { get { return playerPrefab; } }
    public string Name { get { return playerName;  } }
    public string Description { get { return playerDescription; } }
    public string PerkDescription { get { return playerPerkDescription; } }
    public Image Icon { get { return playerIcon; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private string playerName;
    [SerializeField, TextArea] private string playerDescription;
    [SerializeField, TextArea] private string playerPerkDescription;
    [SerializeField] private Image playerIcon;
    
}
