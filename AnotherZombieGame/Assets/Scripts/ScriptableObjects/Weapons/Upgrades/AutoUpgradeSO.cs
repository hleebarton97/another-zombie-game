using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Novasloth/Upgrades/AutoUpgrade")]
public class AutoUpgradeSO : UpgradeSO {

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Set weapon to fully automatic
    public override void DoUpgrade (Weapon _Weapon) {
        _Weapon.FullAuto = true;
    }

    // Set weapon back to semi automatic
    public override void UndoUpgrade (Weapon _Weapon) {
        _Weapon.FullAuto = false;
    }
}
