using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Upgrades/DamageUpgrade")]
public class DamageUpgradeSO : UpgradeSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Range(0.0f, 1.0f)]
    [SerializeField] public float percentIncrease;

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Increase damage done by %
    public override void DoUpgrade (Weapon _Weapon) {
        Weapons.Instance.DamageModifier += this.percentIncrease;
    }

    // Decrease damage done by %
    public override void UndoUpgrade (Weapon _Weapon) {
        Weapons.Instance.DamageModifier -= this.percentIncrease;
    }
}
