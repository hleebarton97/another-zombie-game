using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Novasloth/Upgrades/RecoilUpgrade")]
public class RecoilUpgradeSO : UpgradeSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Range(0.0f, 1.0f)]
    [SerializeField] public float percentDecrease;

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Decrease Recoil (Spread) done by %
    public override void DoUpgrade (Weapon _Weapon) {
        Weapons.Instance.SpreadModifier += percentDecrease;
    }

    // Decrease Recoail (Spread) by %
    public override void UndoUpgrade (Weapon _Weapon) {
        Weapons.Instance.SpreadModifier -= percentDecrease;
    }
}
