using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Novasloth/Upgrades/AmmoGrenadeUpgrade")]
public class AmmoGrenadeUpgradeSO : UpgradeSO {

    private const int INFINITE_AMMO = 9999;

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Overload do infinite ammo

    public override void DoUpgrade (Grenade _Grenade) {
        _Grenade.MaxAmmo += INFINITE_AMMO;
    }

    // Overload undo infinite ammo
    public override void UndoUpgrade (Grenade _Grenade) {
        _Grenade.MaxAmmo -= INFINITE_AMMO;
    }
}
