using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Novasloth/Upgrades/ExplosiveRadiusUpgrade")]
public class ExplosiveRadiusUpgradeSO : UpgradeSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Range(0.0f, 3.0f)]
    [SerializeField] public float percentIncrease;

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Overload for increased radius for weapon

    public override void DoUpgrade (Weapon _Weapon) {
        Weapons.Instance.ExplosionRadiusMultiplier += percentIncrease;
    }

    // Overload for undoing increased radius for weapon
    public override void UndoUpgrade (Weapon _Weapon) {
        Weapons.Instance.ExplosionRadiusMultiplier -= percentIncrease;
    }


    // Overload for increased radius for grenade

    public override void DoUpgrade (Grenade _Grenade) {
        Weapons.Instance.ExplosionRadiusMultiplier += percentIncrease;
    }

    // Overload for undoing increased radius for grenade
    public override void UndoUpgrade (Grenade _Grenade) {
        Weapons.Instance.ExplosionRadiusMultiplier -= percentIncrease;
    }
}
