using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Novasloth/Upgrades/ExplosiveClusterUpgrade")]
public class ExplosiveClusterUpgradeSO : UpgradeSO {

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Overload for cluster bombs on Grenade

    public override void DoUpgrade (Grenade _Grenade) {
        _Grenade.ClusterUpgrade = true;
    }

    // Overload for cluster bombs on Grenade
    public override void UndoUpgrade (Grenade _Grenade) {
        _Grenade.ClusterUpgrade = false;
    }
}
