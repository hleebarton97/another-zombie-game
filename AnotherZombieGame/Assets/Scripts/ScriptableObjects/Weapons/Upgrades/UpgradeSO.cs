using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public abstract class UpgradeSO : ScriptableObject {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public int Price { get { return this.price; } }
    public WeaponUpgrade.Enum Type { get { return this.type; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private int price;
    [SerializeField] private WeaponUpgrade.Enum type;

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public virtual void DoUpgrade (Weapon _Weapon) {}
    public virtual void UndoUpgrade (Weapon _Weapon) {}
    public virtual void DoUpgrade (Grenade _Grenade) {}
    public virtual void UndoUpgrade (Grenade _Grenade) {}
}
