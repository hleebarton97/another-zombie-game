using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Novasloth/Upgrades/AmmoUpgrade")]
public class AmmoUpgradeSO : UpgradeSO {

    private const int INFINITE_AMMO = 9999;

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Do infinite ammo
    public override void DoUpgrade (Weapon _Weapon) {
        _Weapon.MaxAmmo += INFINITE_AMMO;
    }

    // Undo infinite ammo
    public override void UndoUpgrade (Weapon _Weapon) {
        _Weapon.MaxAmmo -= INFINITE_AMMO;
    }

}
