using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Perks/Suit")]
public class SuitPerkSO : PerkSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Range(0.0f, 1.0f)]
    [SerializeField] private float weaponSpreadPercentDecrease = 1.0f;

    public override void Init () {
        Type = Perk.Enum.SUIT;
        Description = "Suit's passive perk removes all weapon spread";
    }

    public override void Activate () {
        Weapons.Instance.SpreadModifier = weaponSpreadPercentDecrease;
    }

    public override void Deactivate () {
        Debug.LogWarning("SuitPerkSO :: Deactivate :: Suit's passive perk should not be deactivated!");
    }
}
