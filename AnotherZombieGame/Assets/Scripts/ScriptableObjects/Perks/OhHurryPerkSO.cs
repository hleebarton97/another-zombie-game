using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Perks/OhHurry")]
public class OhHurryPerkSO : PerkSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float rofIncrease = 0.50f; // 50%

    public override void Init () {
        Type = Perk.Enum.OH_HURRY;
        Description = $"Increase rate of fire for all weapons by {rofIncrease * 100}%";
    }

    public override void Activate () {
        Weapons.Instance.RateOfFireModifier += rofIncrease;
    }

    public override void Deactivate () {
        Weapons.Instance.RateOfFireModifier -= rofIncrease;
    }
}
