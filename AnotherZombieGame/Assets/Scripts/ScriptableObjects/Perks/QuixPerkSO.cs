using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Perks/Quix")]
public class QuixPerkSO : PerkSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float sprintSpeedIncrease = 0.10f; // 10%

    private PlayerMovement _PlayerMovement;

    public override void Init () {
        _PlayerMovement = Util.GetPlayerByTag().GetComponent<PlayerMovement>();

        Type = Perk.Enum.QUIX;
        Description = $"Increase movement speed by {sprintSpeedIncrease * 100}%";
    }

    public override void Activate () {
        if (Util.IsNull(_PlayerMovement)) {
            Debug.LogError("QuixPerkSO :: Activate :: _PlayerMovement component is NULL!");
            return;
        }

        _PlayerMovement.IncreaseSpeedByPercent(sprintSpeedIncrease);
    }

    public override void Deactivate () {
        if (Util.IsNull(_PlayerMovement)) {
            Debug.LogError("QuixPerkSO :: Deactivate :: _PlayerMovement component is NULL!");
            return;
        }

        _PlayerMovement.DecreaseSpeedByPercent(sprintSpeedIncrease);
    }
}
