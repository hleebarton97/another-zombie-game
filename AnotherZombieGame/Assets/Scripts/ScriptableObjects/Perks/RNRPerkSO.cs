using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Perks/RNR")]
public class RNRPerkSO : PerkSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float healthIncrease = 0.50f; // 50%

    private PlayerHealth _PlayerHealth;

    public override void Init () {
        _PlayerHealth = Util.GetPlayerByTag().GetComponent<PlayerHealth>();
        Type = Perk.Enum.RNR;
        Description = $"Increase max health by {healthIncrease * 100}%";
    }

    public override void Activate () {
        if (Util.IsNull(_PlayerHealth)) {
            Debug.LogError("RNRPerkSO :: Activate :: _PlayerHealth component is NULL!");
            return;
        }

        _PlayerHealth.IncreaseHealthByPercent(healthIncrease);
    }

    public override void Deactivate () {
        if (Util.IsNull(_PlayerHealth)) {
            Debug.LogError("RNRPerkSO :: Deactivate :: _PlayerHealth component is NULL!");
            return;
        }

        _PlayerHealth.DecreaseHealthByPercent(healthIncrease);
    }
}
