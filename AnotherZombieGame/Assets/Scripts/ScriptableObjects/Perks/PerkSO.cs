using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Novasloth Games LLC
// Lee Barton
public abstract class PerkSO : ScriptableObject {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public Perk.Enum Type {
        get { return perkType; }
        protected set { perkType = value; }
    }
    public string Title { get { return title; } }
    public string Description {
        get { return description; }
        protected set { description = value; }
    }
    public int Price { get { return price; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Perk.Enum perkType;
    [SerializeField] private string title;
    [SerializeField] private string description;
    [SerializeField] private int price;
    [SerializeField] private Image icon;

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public abstract void Init ();
    public abstract void Activate ();
    public abstract void Deactivate ();
}
