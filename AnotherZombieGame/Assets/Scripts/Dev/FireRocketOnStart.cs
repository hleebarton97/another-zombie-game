using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRocketOnStart : MonoBehaviour {

    [SerializeField] private float force = 1000.0f;
    [SerializeField] private float radius = 2.225f;
    [SerializeField] private Transform target;

    private void Start () {
        Rocket _Rocket = this.GetComponent<Rocket>();
        _Rocket.Fire(
            0.0f,
            force,
            transform,
            radius
        );
    }
}
