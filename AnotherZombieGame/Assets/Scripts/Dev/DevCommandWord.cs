using System;

// Lee Barton
public class DevCommandWord : DevCommand {

    private Action command;

    public DevCommandWord (string commandId, string commandDescription, string commandFormat, Action command)
        : base(commandId, commandDescription, commandFormat) {
            this.command = command;
    }

    public void Invoke () {
        this.command.Invoke();
    }

}

public class DevCommandWord<T1> : DevCommand {

    private Action<T1> command;

    public DevCommandWord (string commandId, string commandDescription, string commandFormat, Action<T1> command)
        : base(commandId, commandDescription, commandFormat) {
            this.command = command;
    }

    public void Invoke (T1 value) {
        this.command.Invoke(value);
    }
}

public class DevCommandWord<T1, T2> : DevCommand {

    private Action<T1, T2> command;

    public DevCommandWord (string commandId, string commandDescription, string commandFormat, Action<T1, T2> command)
        : base(commandId, commandDescription, commandFormat) {
            this.command = command;
    }

    public void Invoke (T1 value, T2 value2) {
        this.command.Invoke(value, value2);
    }
}


