// Lee Barton
public abstract class DevCommand {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public string CommandId { get { return this.commandId; } }
    public string CommandDescription { get { return this.commandDescription; } }
    public string CommandFormat { get { return this.commandFormat; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private string commandId;
    private string commandDescription;
    private string commandFormat;

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public DevCommand (string commandId, string commandDescription, string commandFormat) {
        this.commandId = commandId;
        this.commandDescription = commandDescription;
        this.commandFormat = commandFormat;
    }
}
