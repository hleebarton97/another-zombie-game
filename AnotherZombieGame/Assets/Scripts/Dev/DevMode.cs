﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// <Studio Name>
// Lee Barton
public class DevMode : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    #pragma warning disable 0649
    [SerializeField] private Spawner spawner;
    [SerializeField] private GameObject UIPanel;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject controlPanel;

    [Header("Purchase Weapons At Start")]
    [SerializeField] private bool purchaseDeagle = false;
    [SerializeField] private bool purchaseAR = false;
    [SerializeField] private bool purchaseShotgun = false;
    [SerializeField] private bool purchaseSMG = false;
    [SerializeField] private bool purchaseLMG = false;
    [SerializeField] private bool purchaseGrenade = false;
    [SerializeField] private bool purchaseRL = false;
    Dictionary<Weapons.Enum, bool> doPurchase = new Dictionary<Weapons.Enum, bool>();

    private GameObject playerObject;
    private WeaponsPurchasing _WeaponsPurchasing;

    private bool navMeshEnabled;

    // UI
    private bool isPaused;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Start is called before the first frame update
    private void Awake () {
        Init();
        SetupReferences();
    }

    private void Start () {
        PurchaseWeaponsAtStart();
    }

    // Update is called once per frame
    private void Update () {
        ListenForDevInput();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        navMeshEnabled = true;
        doPurchase.Add(Weapons.Enum.ADVANCED_PISTOL, purchaseDeagle);
        doPurchase.Add(Weapons.Enum.ASSAULT_RIFLE, purchaseAR);
        doPurchase.Add(Weapons.Enum.SHOTGUN, purchaseShotgun);
        doPurchase.Add(Weapons.Enum.SUBMACHINE_GUN, purchaseSMG);
        doPurchase.Add(Weapons.Enum.LIGHT_MACHINE_GUN, purchaseLMG);
        doPurchase.Add(Weapons.Enum.GRENADE, purchaseGrenade);
        doPurchase.Add(Weapons.Enum.ROCKET_LAUNCHER, purchaseRL);
    }

    private void SetupReferences () {
        playerObject = GameObject.FindGameObjectWithTag(Tag.PLAYER);

        if (Util.IsNull(playerObject)) {
            Debug.LogError("DevMode :: SetupReferences() :: Player is NULL! Disabling...");
            enabled = false;
            return;
        }

        _WeaponsPurchasing = playerObject.GetComponentInChildren<WeaponsPurchasing>();
    }

    private void ListenForDevInput () {
        // Spawn enemy
        //if (Input.GetKeyDown(KeyCode.UpArrow)) {
        //    spawner.SpawnEnemy();
        //}
        // Disable nav agents on all enemies
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            DisableNavAgents();
        }
        // Show game controls & dev controls
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (isPaused) {
                ResumeGame();
            } else {
                PauseGame();
            }
        }
    }

    // Purchase weapons at start if specified.
    private void PurchaseWeaponsAtStart () {
        foreach (Weapons.Enum weapon in Enum.GetValues(typeof(Weapons.Enum))) {
            if (weapon == Weapons.Enum.PISTOL || weapon == Weapons.Enum.ALIEN_BLASTER) {
                continue;
            }

            bool purchase;
            if (doPurchase.TryGetValue(weapon, out purchase)) {
                if (purchase) {
                    _WeaponsPurchasing.PurchaseWeapon(weapon);
                }
            }
        }
    }

    // Disable animator and nav agents of all enemies in scene.
    private void DisableNavAgents () {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(Tag.ENEMY);
        foreach (GameObject enemy in enemies) {
            enemy.GetComponent<EnemyMovement>().ToggleNavAgent(!navMeshEnabled);
            enemy.GetComponentInChildren<Animator>().enabled = !navMeshEnabled;
        }
        navMeshEnabled = !navMeshEnabled;
    }

    private void ResetCursor () {
        WeaponCursor weaponCursor = GameObject.FindGameObjectWithTag(Tag.PLAYER).GetComponentInChildren<WeaponCursor>();
        if (isPaused) {
            weaponCursor.SetCrosshair();
        } else {
            weaponCursor.ResetCursor();
        }
    }

    private void PauseGame () {
        UIPanel.SetActive(true);
        Time.timeScale = 0f;
        ResetCursor();
        isPaused = true;
    }

    public void ResumeGame () {
        UIPanel.SetActive(false);
        Time.timeScale = 1f;
        ResetCursor();
        isPaused = false;
    }

    public void QuitGame () {
        Application.Quit();
    }

    public void ShowControlsMenu () {
        controlPanel.SetActive(true);
        pausePanel.SetActive(false);
    }

    public void HideControlsMenu () {
        pausePanel.SetActive(true);
        controlPanel.SetActive(false);
    }
}
