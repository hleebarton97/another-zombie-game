﻿// Novasloth Games LLC
// Lee Barton
// @TODO @PPI Replace strings with int enum values
public static class Tag {

    public static string RESPAWN = "Respawn";
    public static string FINISH = "Finish";
    public static string EDITOR_ONLY = "EditorOnly";
    public static string MAIN_CAMERA = "MainCamera";
    public static string PLAYER = "Player";
    public static string GAME_CONTROLLER = "GameController";
    public static string GROUND_MASK = "GroundMask";
    public static string ENEMY = "Enemy";
    public static string HUD = "HUD";
    public static string NO_SNAP = "NoSnap";
    public static string SPAWNER  = "Spawner";
    public static string ROUND_SYSTEM = "RoundSystem";
}
