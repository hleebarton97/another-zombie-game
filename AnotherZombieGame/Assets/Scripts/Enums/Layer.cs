﻿// Novasloth Games LLC
// Lee Barton
public enum Layer {
    DEFAULT,            // 0
    TRANSPORT_FX,       // 1
    IGNORE_RAYCAST,     // 2
    BUILT_IN_LAYER_3,   // 3
    WATER,              // 4
    UI,                 // 5
    BUILT_IN_LAYER_6,   // 6
    BUILT_IN_LAYER_7,   // 7
    POST_PROCESSING,    // 8
    GROUND_MASK,        // 9
    SHOOTABLE           // 10
}
