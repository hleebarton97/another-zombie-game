using UnityEngine;

// Lee Barton
public class ZombieBossAttack : EnemyAttack, IEventRegisterable {

    private const int MAX_STOMPS = 3;

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool PlayerInRange { get { return this.inRangeToAtack; } }
    public bool AttackPlayer { get; set; } = false;
    public bool DoingStompAttack { get; set; } = false;
    public bool DoingStompAttackProjectiles { get; private set; } = false;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Zombie Boss Attack Properties")]
    [SerializeField] private float stompProjectileSpeed = 10.0f;

    [Header("Zombie Boss Particle Effects")]
    [SerializeField] private GameObject stompDirtPrefabPE;
    [SerializeField] private Transform stompDirtSpawnTransform;
    [SerializeField] private GameObject toPlayerProjectile;
    [SerializeField] private GameObject toMidProjectile;
    [SerializeField] private GameObject toFutureProjectile;


    [SerializeField] private bool debug = false;

    private ZombieBossAnimation _ZombieBossAnimation;
    private EnemyHealth _Health;

    private Transform playerTransform;
    private PlayerVectors _PlayerVectors;
    private Vector3 midTargetPosition;

    private Vector3 playerTarget;
    private Vector3 midTarget;
    private Vector3 futureTarget;

    private bool doingToPlayerProjectile;
    private bool doingToMidProjectile;
    private bool doingToFutureProjectile;

    private int stompsCount = 0;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        RegisterEventHandlers();
        SetupReferences();
        Init();
    }

    protected override void Start () {
        attackTimer.Start();
    }

    private void FixedUpdate () {
        CalculateStompProjectilePath();
    }

    protected override void Update () {
        attackTimer.Tick();
        DoStompProjectiles();
    }

    private void LateUpdate () {
        RenderDebugGizmos();
    }

    private void OnDisable () {
        if (DoingStompAttackProjectiles) {
            DoingStompAttackProjectiles = false;
            ResetToPlayerProjectile();
            ResetToMidProjectile();
            ResetToFutureProjectile();

        }
    }

    protected override void OnTriggerEnter (Collider other) {
        if (Util.IsPlayer(other)) {
            inRangeToAtack = true;
        }
    }
    protected override void OnTriggerExit (Collider other) {
        if (Util.IsPlayer(other)) {
            inRangeToAtack = false;
        }
    }


    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        _Health = GetComponent<EnemyHealth>();
        _ZombieBossAnimation = GetComponentInChildren<ZombieBossAnimation>();

        GameObject player = Util.GetPlayerByTag();
        playerTransform = player.transform;
    }

    protected override void Init () {
        attackTimer = new Novasloth.Timer(attackDelay, () => {
            timeToAttack = true;
        });

        _PlayerVectors = new PlayerVectors(playerTransform.position);
        midTargetPosition = playerTransform.position;
    }

    protected override void CheckAttack () {
        bool isAlive = !_Health.IsDead();
        bool canAttack = AttackPlayer && timeToAttack && isAlive && !DoingStompAttack;

        if (canAttack) {
            DoStompAttack();
            ResetTimer();
        }
    }

    public override void DoAttack () {}

    private void DoStompAttack () {
        DoingStompAttack = true;
        _ZombieBossAnimation.StompAttack();
        stompsCount++;
    }

    public void ResetStompsCount () {
        stompsCount = 0;
    }

    // @TODO Set time to destroy as a constant
    private void SpawnStompEffects () {
        GameObject stompDirtPE = Instantiate(stompDirtPrefabPE, stompDirtSpawnTransform);
        stompDirtPE.transform.parent = null;
        stompDirtPE.transform.rotation = Quaternion.Euler(0, 0, 0);
        Destroy(stompDirtPE, 1.0f);
    }

    private void GetStompTargets () {
        playerTarget = _PlayerVectors.CurrentPosition;
        midTarget = midTargetPosition;
        futureTarget = _PlayerVectors.FuturePosition;
    }

    private void SetupStompProjectiles () {
        DoingStompAttackProjectiles = true;
        doingToPlayerProjectile = true;
        doingToMidProjectile = true;
        doingToFutureProjectile = true;

        toPlayerProjectile.SetActive(true);
        toPlayerProjectile.transform.parent = null;

        toMidProjectile.SetActive(true);
        toMidProjectile.transform.parent = null;

        toFutureProjectile.SetActive(true);
        toFutureProjectile.transform.parent = null;
    }

    private void DoStompProjectiles () {
        if (DoingStompAttackProjectiles) {
            if (doingToPlayerProjectile) { HandleToPlayerProjectile(); }
            if (doingToMidProjectile) { HandleToMidProjectile(); }
            if (doingToFutureProjectile) { HandleToFutureProjectile(); }

            if (!doingToPlayerProjectile && !doingToMidProjectile && !doingToFutureProjectile) {
                DoingStompAttackProjectiles = false;
            }
        }
    }

    private void HandleToPlayerProjectile () {
        if (Util.ReachedTarget(playerTarget, toPlayerProjectile.transform.position)) {
            ResetToPlayerProjectile();
        } else {
            toPlayerProjectile.transform.position = Vector3.MoveTowards(
                toPlayerProjectile.transform.position,
                playerTarget,
                stompProjectileSpeed * Time.deltaTime
            );
        }
    }

    private void HandleToMidProjectile () {
        if (Util.ReachedTarget(midTarget, toMidProjectile.transform.position)) {
            ResetToMidProjectile();
        } else {
            toMidProjectile.transform.position = Vector3.MoveTowards(
                toMidProjectile.transform.position,
                midTarget,
                stompProjectileSpeed * Time.deltaTime
            );
        }
    }

    private void HandleToFutureProjectile () {
        if (Util.ReachedTarget(futureTarget, toFutureProjectile.transform.position)) {
            ResetToFutureProjectile();
        } else {
            toFutureProjectile.transform.position = Vector3.MoveTowards(
                toFutureProjectile.transform.position,
                futureTarget,
                stompProjectileSpeed * Time.deltaTime
            );
        }
    }

    private void ResetToPlayerProjectile () {
        doingToPlayerProjectile = false;
        ResetProjectile(toPlayerProjectile);
    }

    private void ResetToMidProjectile () {
        doingToMidProjectile = false;
        ResetProjectile(toMidProjectile);
    }

    private void ResetToFutureProjectile () {
        doingToFutureProjectile = false;
        ResetProjectile(toFutureProjectile);
    }

    private void ResetProjectile (GameObject projectile) {
        if (!Util.IsNull(projectile)) {
            projectile.SetActive(false);
            projectile.transform.parent = stompDirtSpawnTransform;
            projectile.transform.position = stompDirtSpawnTransform.position;
        }
    }

    private void StompAttackComplete () {
        DoingStompAttack = false;
        ResetTimer();
    }

    private void CalculateStompProjectilePath () {
        _PlayerVectors.CurrentPosition = playerTransform.position;

        _PlayerVectors.CalculateFuturePosition(
            stompDirtSpawnTransform.position,
            stompProjectileSpeed
        );

        // Calculate position between future and current position
        midTargetPosition = (_PlayerVectors.CurrentPosition + _PlayerVectors.FuturePosition) / 2;

        if (!Movement.Instance.PlayerIsMoving()) {
            midTargetPosition = Vector3.left + _PlayerVectors.CurrentPosition;
            _PlayerVectors.FuturePosition = Vector3.right + _PlayerVectors.CurrentPosition;
        }

        _PlayerVectors.PreviousPosition = _PlayerVectors.CurrentPosition;
    }

    public void StopAttacking () {
        AttackPlayer = false;
        DoingStompAttack = false;
    }

    public bool HasReachedMaxStomps () => stompsCount >= MAX_STOMPS;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        AnimationEventHandler.AnimationZombieBossStompAttackEnd +=
            OnStompAttackAnimationEnd;
        AnimationEventHandler.AnimationZombieBossStompAttackHit +=
            OnStompAttackAnimationHit;

    }

    public void UnregisterEventHandlers () {
        AnimationEventHandler.AnimationZombieBossStompAttackEnd -=
            OnStompAttackAnimationEnd;
        AnimationEventHandler.AnimationZombieBossStompAttackHit -=
            OnStompAttackAnimationHit;
    }

    private void OnStompAttackAnimationEnd () {
        StompAttackComplete();
    }

    private void OnStompAttackAnimationHit () {
        SpawnStompEffects();
        GetStompTargets();
        SetupStompProjectiles();
    }

    /////////////////////////////////////////////////////////////////
    //
    /////////////////////////////////////////////////////////////////

    // Initialize debug objects & data


    // Render debug gizmos from late update
    private void RenderDebugGizmos () {
        if (debug) {
            // To player's current position
            Debug.DrawLine(
                stompDirtSpawnTransform.position,
                playerTransform.position,
                Color.green
            );

            // To mid position
            Debug.DrawLine(
                stompDirtSpawnTransform.position,
                midTargetPosition,
                Color.green
            );

            // To player's future position
            Debug.DrawLine(
                stompDirtSpawnTransform.position,
                _PlayerVectors.FuturePosition,
                Color.green
            );
        }
    }

}
