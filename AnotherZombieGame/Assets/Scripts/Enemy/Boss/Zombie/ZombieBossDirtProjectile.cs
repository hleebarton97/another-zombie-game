using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class ZombieBossDirtProjectile : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    // @TODO make damage scale with round
    [SerializeField] private int damage = 50;
    [SerializeField] private float force = 550.0f;
    [SerializeField] private float forceRadius = 1.0f;
    private GameObject player;

    private PlayerHealth _PlayerHealth;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.SetupReferences();
    }

    // @TODO add rigidbody component and replace OnTriggerEnter with OnCollisionEnter
    // to add force at point of contact.
    private void OnTriggerEnter (Collider other) {
        if (!other.isTrigger) {
            if (other.tag == Tag.PLAYER) {
                // @TODO damage player
                this._PlayerHealth.Attacked(this.damage);
            } else {
                Rigidbody rigidbody = other.GetComponent<Rigidbody>();
                if (!Util.IsNull(rigidbody)) {
                    rigidbody.AddExplosionForce(this.force, this.transform.position, this.forceRadius);
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        this.player = Util.GetPlayerByTag();
        this._PlayerHealth = this.player.GetComponent<PlayerHealth>();
    }
}
