using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
namespace Novasloth.ZombieBoss {
    public class ZombieBossAI : MonoBehaviour {

        /////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        /////////////////////////////////////////////////////////////////

        [SerializeField] private Transform spawnPointsParent;

        private StateMachine _StateMachine;
        private ZombieBossMovement _ZombieBossMovement;
        private ZombieBossAnimation _ZombieBossAnimation;
        private ZombieBossAttack _ZombieBossAttack;
        private ZombieBossHealth _ZombieBossHealth;

        /////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        /////////////////////////////////////////////////////////////////

        private void Awake () {
            this.SetupReferences();
            this.SetupState();
        }

        private void Update () {
            this._StateMachine.Tick();
        }

        /////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        private void SetupReferences () {
            this._ZombieBossMovement = this.GetComponent<ZombieBossMovement>();
            this._ZombieBossAnimation = this.GetComponent<ZombieBossAnimation>();
            this._ZombieBossAttack = this.GetComponent<ZombieBossAttack>();
            this._ZombieBossHealth = this.GetComponent<ZombieBossHealth>();
        }

        private void SetupState () {
            this._StateMachine = new StateMachine();

            IState spawn = new Spawn(this);
            IState moveToPlayer = new MoveToPlayer(this);
            IState stompAttack = new StompAttack(this);
            IState death = new Death(this);

            // @TODO add position check for spawn state
            this._StateMachine.AddTransition(spawn, moveToPlayer,
                () => this._ZombieBossAnimation.SpawnAnimationComplete
            );

            this._StateMachine.AddTransition(moveToPlayer, stompAttack,
                () => this._ZombieBossMovement.ReachedPlayer() && this._ZombieBossAttack.PlayerInRange
            );

            this._StateMachine.AddTransition(stompAttack, moveToPlayer,
                () => this._ZombieBossAttack.HasReachedMaxStomps() && !this._ZombieBossAttack.PlayerInRange
            );

            this._StateMachine.AddAnyTransition(death,
                () => this._ZombieBossHealth.IsDead()
            );

            this._StateMachine.SetState(spawn);
        }

        /////////////////////////////////////////////////////////////////
        // I S T A T E   H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        // Spawn
        public void StartSpawn () {
            this._ZombieBossAnimation.TriggerSpawnStart();
        }

        public void StopSpawn () {
            this._ZombieBossMovement.SetOriginPosition();
        }

        // MoveToPlayer
        public void StartMovingToPlayer () {
            this._ZombieBossMovement.SetupMoveToPlayer();
            this._ZombieBossAnimation.CheckWalking();
        }

        public void MoveToPlayer () {
            this._ZombieBossMovement.MoveToPlayer();
        }

        public void StopMovingToPlayer () {
            this._ZombieBossMovement.StopMovingToPlayer();
            this._ZombieBossAnimation.CheckWalking();
        }

        // StompAttack
        public void StartStompAttack () {
            this._ZombieBossAttack.AttackPlayer = true;
        }

        public void LookAtPlayer () {
            this._ZombieBossMovement.DoLookAtPlayer();
        }

        public void StopStompAttack () {
            this._ZombieBossAttack.ResetStompsCount();
            this._ZombieBossAttack.AttackPlayer = false;
        }

        // Death
        public void StartDeath () {
            this._ZombieBossAnimation.DisableAnimator();
            this._ZombieBossMovement.DisableNavAgent();

            this._ZombieBossAnimation.enabled = false;
            this._ZombieBossMovement.enabled = false;
            this._ZombieBossAttack.enabled = false;
            this._ZombieBossHealth.enabled = false;

            this.enabled = false;
        }
    }
}
