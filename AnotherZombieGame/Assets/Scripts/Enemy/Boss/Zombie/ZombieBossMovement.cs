using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class ZombieBossMovement : EnemyMovement {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float movementSpeed;

    private Vector3 positionAtOrigin;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Update () { }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Init () {
        this.hasTarget = false;
        this.navAgent.speed = this.movementSpeed;
        this.positionAtOrigin = new Vector3(
            this.transform.position.x,
            0.0f,
            this.transform.position.z
        );

        this.DisableNavAgent();
    }

    public void SetupMoveToPlayer () {
        this.EnableNavAgent();
        this.MoveToPlayer();
        this.hasTarget = true;
    }

    public void MoveToPlayer () {
        this.navAgent.SetDestination(
            this.playerTransform.position
        );
    }

    public void StopMovingToPlayer () {
        this.DisableNavAgent();
        this.hasTarget = false;
    }

    public void DoLookAtPlayer () {
        this.transform.LookAt(this.playerTransform);
    }

    public bool ReachedPlayer () {
        return Util.NavAgentReachedDestination(this.navAgent, this.navAgent.stoppingDistance);
    }

    public void SetOriginPosition () {
        this.positionAtOrigin.Set(
            this.transform.position.x,
            0.0f,
            this.transform.position.z
        );

        this.transform.position = this.positionAtOrigin;
        Debug.Log("Setting Origin Position!");
    }
}
