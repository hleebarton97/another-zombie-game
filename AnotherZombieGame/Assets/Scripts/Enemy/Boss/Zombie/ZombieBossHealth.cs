using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class ZombieBossHealth : EnemyHealth, IHealthBar {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Zombie Boss")]
    [SerializeField] private HealthBar _HealthBar;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void OnDisable () {
        if (!Util.IsNull(this._HealthBar)) {
            this._HealthBar.Hide();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void DeathEvent () {
        EnemyEventHandler.EnemyBossDeath(this.gameObject);
    }

    public override void TakeDamage (float damageMultiplier, RaycastHit hit) {
        base.TakeDamage(damageMultiplier, hit);
        this.UpdateHealthBar();
    }

    public override void TakeDamage (float totalDamage) {
        base.TakeDamage(totalDamage);
        this.UpdateHealthBar();
    }

    public override void TakeExplosionDamage (float damageMultiplier, float distanceFromExplosion, float explosionRadius) {
        base.TakeExplosionDamage(damageMultiplier, distanceFromExplosion, explosionRadius);
        this.UpdateHealthBar();
    }

    public override void TakeExplosionDamage (float damageMultiplier, float distanceFromExplosion, float explosionRadius, GameObject objectInRange) {
        base.TakeExplosionDamage(damageMultiplier, distanceFromExplosion, explosionRadius, objectInRange);
        this.UpdateHealthBar();
    }

    protected override void DoRagdoll (RaycastHit hit) {
        base.DoRagdoll(hit);
        this._HealthBar.Hide();
    }

    protected override void DoRagdoll () {
        base.DoRagdoll();
        this._HealthBar.Hide();
    }

    public void UpdateHealthBar () {
        if (!Util.IsNull(this._HealthBar)) {
            this._HealthBar.UpdateHealthBarPercentage(
                this.health / this.maxHealth
            );
        }
    }

    public void HideHealthBar () {
        if (!Util.IsNull(this._HealthBar)) {
            this._HealthBar.Hide();
        }
    }
}
