using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class BossAnimation : EnemyAnimation {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool SpawnAnimationComplete { get; protected set; } = false;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    // @TODO figure out how to sync up Awake() with StateMachine
    // Possibly OnEnter in Spawn call SetupReferences
    protected bool referencesSetup = false;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        if (!referencesSetup) {
            SetupReferences();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////
    
}
