using UnityEngine;

// Lee Barton
public class SpiderLegHealth : EnemyHealth {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public float Health { get { return this.health; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private SpiderLegPA _SpiderLegPA;
    private IK _IK;
    private SpiderBossHealth _SpiderBossHealth;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        base.Awake();
        this.Init();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {}

    protected override void SetupReferences () {
        base.SetupReferences();
        this._SpiderLegPA = this.GetComponent<SpiderLegPA>();
        this._IK = this.GetComponentInChildren<IK>();
        this._SpiderBossHealth = this.GetComponentInParent<SpiderBossHealth>();
    }

    public bool CanTakeDamage (float damageMultiplier, RaycastHit hit) {
        if (this.isDead) {
            return false;
        }
        base.TakeDamage(damageMultiplier, hit);
        //Debug.Log("SPIDER BOSS :: LEG :: HEALTH(" + this.maxHealth + "/" + this.health + ")");
        return true;
    }

    public bool CanTakeExplosionDamage (out float totalDamage, float damageMultiplier, float distanceFromExplosion, float explosionRadius) {
        if (!this.isDead) {
            totalDamage = Enemy.CalculateExplosionDamage(
                this.damagePerShot,
                damageMultiplier,
                distanceFromExplosion,
                explosionRadius
            );

            this.TakeExplosionDamage(totalDamage);

            return true;
        }

        totalDamage = 0.0f;
        return false;
    }

    private void TakeExplosionDamage (float explosionDamage) {
        this.health = Enemy.HealthAfterDamage(
                this.health,
                explosionDamage
        );

        if (Enemy.IsDead(this.health)) {
            this.Die();
        }
    }

    public override void TakeExplosionDamage (float damageMultiplier, float distanceFromExplosion, float explosionRadius, GameObject objectInRange) {
        if (this.CanTakeExplosionDamage(out float totalDamage, damageMultiplier, distanceFromExplosion, explosionRadius)) {
            if (!Util.IsNull(this._SpiderBossHealth)) {
                this._SpiderBossHealth.TakeDamage(totalDamage);
            }
        }
    }

    protected override void DoRagdoll (RaycastHit hit) {
        // Remove leg from hierarchy
        if (this.ragdollOnDeath) {
            this._SpiderLegPA.enabled = false;
            this._IK.enabled = false;
            this.transform.parent = null;
        }

        base.DoRagdoll(hit);
    }
}
