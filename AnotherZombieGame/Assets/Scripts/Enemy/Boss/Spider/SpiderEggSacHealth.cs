using UnityEngine;

// Lee Barton
public class SpiderEggSacHealth : EnemyHealth {

    private const string ANIM_TAKE_DAMAGE = "TakeDamage";

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Egg Sac Properties")]
    [SerializeField] private GameObject idleParticleEffects;
    [SerializeField] private GameObject deathParticleEffects;

    private Animator animator;

    public float Health { get { return this.health; } }

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        base.Awake();
        this.Init();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.ragdollOnDeath = false;
    }

    protected override void SetupReferences () {
        base.SetupReferences();
        animator = this.GetComponent<Animator>();
    }

    public override void Die (RaycastHit hit) {
        this.DoEffectsOnDeath();
        base.Die(hit);
    }

    public override void Die () {
        this.DoEffectsOnDeath();
        base.Die();
    }

    public override void TakeDamage (float damageMultiplier, RaycastHit hit) {
        base.TakeDamage(damageMultiplier, hit);

        if (!this.isDead) {
            animator.SetTrigger(ANIM_TAKE_DAMAGE);
        }
    }

    private void DoEffectsOnDeath () {
        GameObject effects = Instantiate(this.deathParticleEffects);
        effects.transform.position = this.transform.position;
    }

    public void DoEffectsAfterSpawn () {
        this.idleParticleEffects.SetActive(true);
    }
}
