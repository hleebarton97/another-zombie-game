using UnityEngine;
using Novasloth;

// Lee Barton
public class SpiderEggSac : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject miniSpiderPrefab;
    [SerializeField] private int minSpawnCount = 1;
    [SerializeField] private int maxSpawnCount = 5;
    [SerializeField] private float spawnRadius = 2.5f;
    [SerializeField] private float timeToExplode = 5.0f;

    private SpiderEggSacHealth _SpiderEggSacHealth;

    private Timer timer;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this._SpiderEggSacHealth = this.GetComponent<SpiderEggSacHealth>();

        this.timer = new Timer(this.timeToExplode, () => {
            this.SpawnMiniSpiders();
            _SpiderEggSacHealth.Die();
        });
    }

    private void Start () {
        if (this.minSpawnCount > this.maxSpawnCount) {
            Debug.LogError("INFO :: SpiderEggSac :: Minimum spawn count is greater than Maximum spawn count!");
        }

        this.timer.Start();
    }

    private void Update () {
        this.timer.Tick();
    }

    private void OnDrawGizmos () {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, this.spawnRadius);
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SpawnMiniSpiders () {
        int spawnCount = Random.Range(this.minSpawnCount, this.maxSpawnCount + 1);

        GameObject miniSpider;
        Vector3 spawnPosition;
        for (int i = 0; i < spawnCount; i++) {
            miniSpider = Instantiate(this.miniSpiderPrefab);

            spawnPosition = Random.insideUnitCircle * this.spawnRadius;
            spawnPosition.x += this.transform.position.x;
            spawnPosition.z = spawnPosition.y + this.transform.position.z;
            spawnPosition.y = this.transform.position.y;

            miniSpider.transform.position = spawnPosition;
        }
    }

}
