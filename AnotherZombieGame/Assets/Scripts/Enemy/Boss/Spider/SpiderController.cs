using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class SpiderController : MonoBehaviour {

    private const float MARGIN_OF_ERROR = 0.001f;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private bool doMovement;
    [SerializeField] private Transform bodyTransform;
    [SerializeField] private Transform[] legTransforms;
    [SerializeField] private float bodyRotationMultiplier = 100.0f;
    [SerializeField] private float bodyRotationSpeed = 2.5f;
    [SerializeField] private float bodyMovementSpeed = 1.0f;
    private Vector3 newBodyPosition;
    private float bodyOffset;
    private float totalYValues;
    private float averageYValue;

    [SerializeField] private Transform target;
    [SerializeField] private float speed = 20.0f;
    // [SerializeField] private float rotationSpeed = 50.0f;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.bodyOffset = this.transform.position.y;
        this.GetLegPositionAverage();
        this.bodyOffset -= this.averageYValue;
    }

    private void Update () {
        if (this.target != null) {
            this.transform.position = Vector3.MoveTowards(
                this.transform.position,
                this.target.position,
                (this.speed * Time.deltaTime)
            );
        }

        if (this.doMovement && this.bodyTransform != null) {
            this.HandleBodyPosition();
            this.HandleBodyRotation();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void HandleBodyPosition () {
        // Get average leg position
        this.GetLegPositionAverage();

        // Set new body position
        this.newBodyPosition.Set(
            this.transform.position.x,
            this.bodyOffset + this.averageYValue,
            this.transform.position.z
        );

        bool bodyNearNewPosition = Vector3.Distance(this.transform.position, this.newBodyPosition) > MARGIN_OF_ERROR;
        if (bodyNearNewPosition) {
            this.transform.position = Vector3.MoveTowards(
                this.transform.position,
                this.newBodyPosition,
                (this.bodyMovementSpeed * Time.deltaTime)
            );
        }
    }

    private void HandleBodyRotation () {
        float yDiff = this.legTransforms[0].position.y - this.legTransforms[1].position.y;
        this.bodyTransform.rotation = Quaternion.Lerp(
            this.bodyTransform.rotation,
            Quaternion.Euler(0.0f, 0.0f, (yDiff * this.bodyRotationMultiplier)),
            (this.bodyRotationSpeed * Time.deltaTime)
        );
    }

    private void GetLegPositionAverage () {
        this.totalYValues = 0.0f;
        foreach (Transform legTransform in this.legTransforms) {
            totalYValues += legTransform.position.y;
        }
        this.averageYValue = totalYValues / this.legTransforms.Length;
    }
}
