using UnityEngine;

// @TODO clean up comments after confirm refactor is okay

// Lee Barton
public class SpiderBossAttack : EnemyAttack {

    private const string DEBUG_SPIT_TARGET_NAME = "[Debug]SpitAttackTarget";

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool PlayerInRange { get { return this.inRangeToAtack; } }
    public bool AttackPlayer { get; set; } = false;
    public bool PassiveAttack { get; set; } = false;
    public bool DoingSpitAttack { get; private set; } = false;
    public bool DoingPassiveAttack { get; private set; } = false;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Spider Attack Properties")]
    [SerializeField] private GameObject spitProjectile;
    [SerializeField] private GameObject spitPoisonGasPrefab;
    [SerializeField] private GameObject spitPoisonGasNoSplatPrefab;
    [SerializeField] private GameObject eggSacPrefab;
    [SerializeField] private float spitAttackSpeed = 5.0f;
    [SerializeField] private float spitPoisonGasTime = 15.0f;

    [Header("Spider Properties")]
    [SerializeField] private Transform spiderMouthTransform;
    [SerializeField] private Transform spiderSpitTransform;
    [SerializeField] private Transform spiderEggSacSpawnTransform;
    [SerializeField] private float[] potentialDelay = { 0.5f, 2.0f, 4.0f, 8.0f };
    [SerializeField] private bool debug = false;

    private Transform playerTransform;
    private Animator spiderAnimator;
    private EnemyHealth _Health;

    // Player props for spit attack
    //private Vector3 playerFuturePosition;
    //private Vector3 playerCurrentPosition;
    //private Vector3 playerPreviousPosition;
    //private Vector3 playerVelocity;
    private PlayerVectors _PlayerVectors;
    private Vector3 spiderSpitTarget;
    private RaycastHit spitHit;
    private bool splat = true;

    // Debug
    private GameObject debugSphere = null;
    private Renderer debugSphereRenderer;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        SetupReferences();
        Init();
    }

    protected override void Start () {
        attackTimer.Start();
    }

    private void FixedUpdate () {
        CalculateSpitAttackPosition();
    }

    protected override void Update () {
        attackTimer.Tick();
        DoSpitAttack();
    }

    private void LateUpdate () {
        RenderDebugGizmos();
    }

    protected override void OnTriggerEnter (Collider other) {
        if (Util.IsPlayer(other)) {
            inRangeToAtack = true;
        }
    }

    private void OnTriggerStay (Collider other) {
        if (!inRangeToAtack && Util.IsPlayer(other)) {
            inRangeToAtack = true;
        }
    }

    protected override void OnTriggerExit (Collider other) {
        if (Util.IsPlayer(other)) {
            inRangeToAtack = false;
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        GameObject player = Util.GetPlayerByTag();
        playerTransform = player.transform;
        spiderAnimator = GetComponentInParent<Animator>();
        _Health = GetComponentInParent<EnemyHealth>();
    }

    protected override void Init () {
        //this.playerFuturePosition = this.playerTransform.position;
        //this.playerCurrentPosition = this.playerTransform.position;
        //this.playerPreviousPosition = this.playerTransform.position;
        //this.playerVelocity = Vector3.zero;
        _PlayerVectors = new PlayerVectors(playerTransform.position);

        ResetTimer();
        InitDebug();
    }

    protected override void CheckAttack () {
        bool isAlive = !_Health.IsDead();
        bool canAttack = AttackPlayer && timeToAttack && isAlive && inRangeToAtack && !DoingSpitAttack;
        bool canPassiveAttack = PassiveAttack && isAlive && !inRangeToAtack && !DoingPassiveAttack;

        if (canAttack) {
            DoingSpitAttack = true;
            GetSpiderSpitTarget(); // this.spiderSpitTarget = this.playerFuturePosition;
            spiderSpitTransform.parent = null;
            spitProjectile.SetActive(true);
            ResetTimer();
        }
        else if (canPassiveAttack) {
            DoingPassiveAttack = true;
            DoPassiveAttack();
        }
    }

    public override void DoAttack () {}

    private void GetSpiderSpitTarget () {
        splat = true;
        //this.spiderSpitTarget = this.playerFuturePosition;
        spiderSpitTarget = _PlayerVectors.FuturePosition;

        Vector3 direction = _PlayerVectors.FuturePosition - spiderMouthTransform.position;
        if (Physics.Raycast(
            spiderMouthTransform.position,
            direction,
            out spitHit,
            100.0f,
            Physics.AllLayers,
            QueryTriggerInteraction.Ignore
        )) {
            string tag = spitHit.transform.tag;

            if (tag != Tag.PLAYER && tag != Tag.GROUND_MASK) {
                spiderSpitTarget = spitHit.point;
                splat = false;
            }
        }
    }

    private void DoSpitAttack () {
        if (DoingSpitAttack) {
            if (Util.ReachedTarget(spiderSpitTarget, spiderSpitTransform.position)) {
                SpawnPoisonGas();
                ResetSpitProjectile();
            } else {
                spiderSpitTransform.position = Vector3.MoveTowards(spiderSpitTransform.position, spiderSpitTarget, spitAttackSpeed * Time.deltaTime);
            }
        }
    }

    public void StopSpitAttack () {
        AttackPlayer = false;
        DoingSpitAttack = false;
        ResetSpitProjectile();
    }

    private void DoPassiveAttack () {
        if (DoingPassiveAttack) {
            //Debug.Log("TEMP_INFO :: SpiderBossAttack :: Doing Birth Attack!");
            spiderAnimator.SetBool("DoPassiveAttack", true);
            StopPassiveAttack();
        }
    }

    public void DoSpawnAttack () {
        GameObject spiderSac = Instantiate(eggSacPrefab);
        spiderSac.transform.position = spiderEggSacSpawnTransform.position;
    }

    public void StopPassiveAttack () {
        PassiveAttack = false;
        DoingPassiveAttack = false;
    }

    private void SpawnPoisonGas () {
        GameObject poisonGas = Instantiate(
            (splat ? spitPoisonGasPrefab : spitPoisonGasNoSplatPrefab),
            spiderSpitTarget,
            spitPoisonGasPrefab.transform.rotation
        );

        Destroy(poisonGas, spitPoisonGasTime + 5.0f);
    }

    public void ResetSpitProjectile () {
        DoingSpitAttack = false;
        spiderSpitTransform.position = spiderMouthTransform.position;
        spiderSpitTransform.parent = spiderMouthTransform;
        spitProjectile.SetActive(false);
    }

    // @TODO remove comments after testing Util code
    private void CalculateSpitAttackPosition () {
        //this.playerCurrentPosition = this.playerTransform.position;
        _PlayerVectors.CurrentPosition = playerTransform.position;

        //this.playerVelocity = (this.playerCurrentPosition - this.playerPreviousPosition) / Time.fixedDeltaTime;
        //this.playerFuturePosition = this.playerCurrentPosition + (
        //    this.playerVelocity * (
        //        (this.spiderMouthTransform.position - this.playerCurrentPosition).magnitude / this.spitAttackSpeed
        //    )
        //);
        _PlayerVectors.CalculateFuturePosition(
            spiderMouthTransform.position,
            spitAttackSpeed
        );

        //this.playerPreviousPosition = this.playerCurrentPosition;
        _PlayerVectors.PreviousPosition = _PlayerVectors.CurrentPosition;
    }

    protected override void ResetTimer () {
        base.ResetTimer();

        int random = Random.Range(0, potentialDelay.Length);
        //this.attackDelay = this.potentialDelay[random];
        attackTimer.TimerDelay = potentialDelay[random];
    }

    /////////////////////////////////////////////////////////////////
    // A N I M A T I O N   E V E N T S
    /////////////////////////////////////////////////////////////////

    public void PassiveAttackComplete () {
        Debug.Log("TEMP_INFO :: SpiderBossAttack :: Passive attack complete!");
        SpiderBossEventHandler.PassiveAttackComplete();
    }

    /////////////////////////////////////////////////////////////////
    //
    /////////////////////////////////////////////////////////////////

    // Initialize debug objects & data
    private void InitDebug () {
        debugSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(debugSphere.GetComponent<SphereCollider>());
        debugSphere.transform.parent = transform;
        debugSphere.name = DEBUG_SPIT_TARGET_NAME;
        debugSphereRenderer = debugSphere.GetComponent<Renderer>();
    }

    // Render debug gizmos from late update
    private void RenderDebugGizmos () {
        if (debug) {
            // Mouth to player's current position
            Debug.DrawLine(
                spiderMouthTransform.position,
                playerTransform.position,
                Color.white
            );

            // Player's current position to player's future position
            Debug.DrawLine(
                _PlayerVectors.CurrentPosition,
                _PlayerVectors.FuturePosition,
                Color.white
            );

            Color debugRangeColor = (inRangeToAtack ? Color.green : Color.red);

            // Mouth to player's future position
            Debug.DrawLine(
                spiderMouthTransform.position,
                _PlayerVectors.FuturePosition,
                debugRangeColor
            );

            // Draw sphere at target future position
            if (!debugSphere.activeInHierarchy) {
                debugSphere.SetActive(true);
            }

            debugSphere.transform.position = _PlayerVectors.FuturePosition;
            debugSphereRenderer.material.color = debugRangeColor;
        } else {
            debugSphere.SetActive(false);
        }
    }
}
