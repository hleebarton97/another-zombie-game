using UnityEngine;
using Novasloth.SpiderBoss;

// Lee Barton
public class SpiderBossMovement : EnemyMovement {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Transform spiderTransform;
    [SerializeField] private float movementSpeed = 15.0f;

    private SpiderBossAI _SpiderBossAI;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        base.Awake();
    }

    protected override void Update () {}

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Init () {
        this.hasTarget = false;
        this.navAgent.speed = this.movementSpeed;
        this.DisableNavAgent();
    }

    protected override void SetupReferences () {
        base.SetupReferences();

        this._SpiderBossAI = this.GetComponent<SpiderBossAI>();

        if (Util.IsNull(spiderTransform)) {
            Debug.Log("INFO :: SpiderBossMovement :: Spider Transform is NULL!");
        }
    }

    public void DoLookAtPlayer () {
        this.spiderTransform.LookAt(this.playerTransform);
    }

    public void SetupRetreat () {
        this.EnableNavAgent();
        this.hasTarget = true;
    }

    public void DoRetreat () {
        this.navAgent.SetDestination(
            this._SpiderBossAI.GetRetreatPoint().position
        );
    }

    public void StopRetreat () {
        this.DisableNavAgent();
        this.hasTarget = false;
    }

    public bool ReachedRetreatDestination () {
        return Util.NavAgentReachedDestination(this.navAgent);
    }
}
