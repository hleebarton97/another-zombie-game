using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class SpiderBossHealth : EnemyHealth, IHealthBar {

    /////////////////////////////////////////////////////////////////
    // I N T E R F A C E   M E T H O D S
    /////////////////////////////////////////////////////////////////

    [Header("Spider")]
    [SerializeField] private Transform legsTransform;
    [SerializeField] private HealthBar _HealthBar;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private SpiderController _SpiderController;

    private bool quarterDamageTaken;
    private bool halfDamageTaken;
    private bool threeQuartersDamageTaken;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Start () {
        this.SetMaxHealthBasedOnLegsHealth();
    }

    private void OnDisable () {
        if (!Util.IsNull(this._HealthBar)) {
            this._HealthBar.Hide();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        base.SetupReferences();
        this._SpiderController = this.GetComponentInChildren<SpiderController>();
    }

    private void SetMaxHealthBasedOnLegsHealth () {
        float totalHealth = 0;
        foreach (Transform legHealth in this.legsTransform) {
            if (legHealth.tag == Tag.ENEMY) {
                SpiderLegHealth SpiderLegHealth = legHealth.GetComponent<SpiderLegHealth>();
                totalHealth += SpiderLegHealth.Health;
            }
        }

        this.maxHealth = totalHealth / 2;
        this.health = this.maxHealth;
        //Debug.Log("SPIDER BOSS :: HEALTH(" + this.maxHealth + "/" + this.health + ")");
    }

    protected override void DeathEvent () {
        EnemyEventHandler.EnemyBossDeath(this.gameObject);
    }

    protected override void DoRagdoll (RaycastHit hit) {
        this.DoLegsRagdoll();
        base.DoRagdoll(hit);
    }

    protected override void DoRagdoll () {
        this.DoLegsRagdoll();
        base.DoRagdoll();
    }

    private void DoLegsRagdoll () {
        this._HealthBar.Hide();
        // Disable controller, procedural animation, and inverse kinematics
        if (this.ragdollOnDeath) {
            this._SpiderController.enabled = false;
            SpiderLegPA[] spiderLegPAList = this.GetComponentsInChildren<SpiderLegPA>();
            IK[] IKList = this.GetComponentsInChildren<IK>();

            foreach (SpiderLegPA spiderLegPA in spiderLegPAList) {
                spiderLegPA.enabled = false;
            }

            foreach (IK ik in IKList) {
                ik.enabled = false;
            }

            this.enabled = false;
        }
    }

    // @PPI
    // Could possibly setup references at start to each SpiderLegHealth script
    // instead of .GetComponent<>()
    // Will have to worry about the dynamic legs when they lose all health
    // Could use .GetSiblingIndex with a private List<>
    public override void TakeDamage (float damageMultiplier, RaycastHit hit) {
        if (!this.isDead) {
            Transform firstHit = hit.collider.gameObject.transform;

            if (this.IsLeg(firstHit, out Transform spiderLegHit)) {
                SpiderLegHealth _SpiderLegHealth = spiderLegHit.GetComponent<SpiderLegHealth>();

                if (_SpiderLegHealth.CanTakeDamage(damageMultiplier, hit)) {
                    base.TakeDamage(damageMultiplier, hit);
                    this.UpdateHealthBar();
                }
            }

            this.CheckPercentDamageTaken();
        }
        //Debug.Log("SPIDER BOSS :: HEALTH(" + this.maxHealth + "/" + this.health + ")");
    }

    public override void TakeDamage (float totalDamage) {
        base.TakeDamage(totalDamage);
        this.UpdateHealthBar();
        this.CheckPercentDamageTaken();
    }

    public override void TakeExplosionDamage (float damageMultiplier, float distanceFromExplosion, float explosionRadius, GameObject objectInRange) {
        if (!this.isDead) {
            Transform firstHit = objectInRange.transform;

            if (this.IsLeg(firstHit, out Transform spiderLegHit)) {
                SpiderLegHealth _SpiderLegHealth = spiderLegHit.GetComponent<SpiderLegHealth>();

                if (_SpiderLegHealth.CanTakeExplosionDamage(out float totalDamage, damageMultiplier, distanceFromExplosion, explosionRadius)) {
                    base.TakeDamage(totalDamage);
                    this.UpdateHealthBar();
                }
            }
        }
    }

    private bool IsLeg (Transform firstHit, out Transform hitParentTransform) {
        hitParentTransform = Util.GetParentByTag(firstHit, Tag.ENEMY, 0, 2);  // @PPI

        if (!Util.IsNull(hitParentTransform) && !Util.IsNull(hitParentTransform.parent)) {
            return (hitParentTransform.parent.name == this.legsTransform.name);
        }

        return false;
    }

    public void CheckPercentDamageTaken () {
        float percentDamage = this.PercentDamageTaken();

        if ((percentDamage >= 0.25 && percentDamage < 0.50) && !this.quarterDamageTaken) { // 25%
            this.quarterDamageTaken = true;
            SpiderBossEventHandler.QuarterDamageTaken(this.health);
        } else if ((percentDamage >= 0.50 && percentDamage < 0.75) && !this.halfDamageTaken) { // 50%
            this.halfDamageTaken = true;
            SpiderBossEventHandler.QuarterDamageTaken(this.health);
        } else if ((percentDamage >= 0.75 && percentDamage < 1.00) && !this.threeQuartersDamageTaken) { // 75%
            this.threeQuartersDamageTaken = true;
            SpiderBossEventHandler.QuarterDamageTaken(this.health);
        }
    }

    public float PercentDamageTaken () {
        return ((this.maxHealth - this.health) / this.maxHealth);
    }

    public void UpdateHealthBar () {
        if (!Util.IsNull(this._HealthBar)) {
            this._HealthBar.UpdateHealthBarPercentage(
                this.health / this.maxHealth
            );
        }
    }

    public void HideHealthBar () {
        if (!Util.IsNull(this._HealthBar)) {
            this._HealthBar.Hide();
        }
    }
}
