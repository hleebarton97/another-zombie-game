using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class MiniSpiderHealth : EnemyHealth {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private MiniSpiderExplode _MiniSpiderExplode;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        this._MiniSpiderExplode = this.GetComponentInChildren<MiniSpiderExplode>();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public override void TakeDamage (float damageMultiplier, RaycastHit hit) {
        this.KillOnAnyDamage();
    }

    public override void TakeExplosionDamage (float damageMultiplier, float distanceFromExplosion, float explosionRadius) {
        this.KillOnAnyDamage();
    }

    private void KillOnAnyDamage () {
        this.isDead = true;
        this._MiniSpiderExplode.Explode();
    }

}
