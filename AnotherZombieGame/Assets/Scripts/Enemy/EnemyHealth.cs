﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class EnemyHealth : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // I N T E R F A C E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] protected float maxHealth = 100.0f;
    [SerializeField] protected float damagePerShot = 50.0f;
    [SerializeField] protected float timeUntilDestroyed = 5.0f;

    [SerializeField] protected bool ragdollOnDeath = true;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    protected float health;
    protected bool isDead = false;

    protected RagdollManager _RagdollManager;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Start is called before the first frame update
    protected virtual void Awake () {
        this.SetupReferences();
        this.health = this.maxHealth;
    }

	/////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void SetupReferences () {
        this._RagdollManager = this.GetComponent<RagdollManager>();
    }

    // Enemy has died with hit.
    public virtual void Die (RaycastHit hit) {
        if (this.InitDie()) {
            this.DoRagdoll(hit);
        }
    }

    // Enemy has died.
    public virtual void Die () {
        if (this.InitDie()) {
            this.DoRagdoll();
        }
    }

    // Check if should handle ragdoll.
    private bool InitDie () {
        this.isDead = true;
        this.DeathEvent();

        if (!this.ragdollOnDeath) {
            Destroy(this.gameObject, this.timeUntilDestroyed);
            return false;
        }

        return true;
    }

    protected virtual void DeathEvent () {
        EnemyEventHandler.EnemyZombieDeath(this.gameObject);
    }

    // Enemy takes damage after getting shot.
    public virtual void TakeDamage (float damageMultiplier, RaycastHit hit) {
        if (!this.isDead) {
            this.health = Enemy.HealthAfterDamage(
                this.health,
                this.damagePerShot,
                damageMultiplier
            );
            // Debug.Log($"HEALTH :: {this.health}");

            if (Enemy.IsDead(this.health)) {
                this.Die(hit);
            }
        }
    }

    public virtual void TakeDamage (float totalDamage) {
        if (!this.isDead) {
            this.health = Enemy.HealthAfterDamage(this.health, totalDamage);

            if (Enemy.IsDead(this.health)) {
                this.Die();
            }
        }
    }

    // Enemy takes damage being in range of an explosion.
    public virtual void TakeExplosionDamage (float damageMultiplier, float distanceFromExplosion, float explosionRadius, GameObject objectInRange) {
        this.TakeExplosionDamage(damageMultiplier, distanceFromExplosion, explosionRadius);
    }

    public virtual void TakeExplosionDamage (float damageMultiplier, float distanceFromExplosion, float explosionRadius) {
        //Debug.Log("BASE!");
        if (!this.isDead) {
            Debug.Log($"EnemyHealth :: TakeExplosionDamage :: damageMultiplier:{damageMultiplier}, distanceFromExplosion:{distanceFromExplosion}, explosionRadius:{explosionRadius}");

            float explosionDamage = Enemy.CalculateExplosionDamage(
                this.damagePerShot,
                damageMultiplier,
                distanceFromExplosion,
                explosionRadius
            );
            Debug.Log($"EnemyHealth :: TakeExplosionDamage :: explosionDamage:{explosionDamage}");
            this.health = Enemy.HealthAfterDamage(
                this.health,
                explosionDamage
            );
            Debug.Log($"EnemyHealth :: TakeExplosionDamage :: enemy health:{health}");
            if (Enemy.IsDead(this.health)) {
                this.Die();
            }
        }
    }

    protected virtual void DoRagdoll (RaycastHit hit) {
        if (this.ragdollOnDeath) {
            this._RagdollManager.DoRagdoll(
                this.gameObject,
                hit,
                this.timeUntilDestroyed
            );
        }
    }

    protected virtual void DoRagdoll () {
        if (this.ragdollOnDeath) {
            this._RagdollManager.DoRagdoll(
                this.gameObject,
                this.timeUntilDestroyed
            );
        }
    }

    /////////////////////////////////////////////////////////////////
    // G E T T E R S
    /////////////////////////////////////////////////////////////////

    public bool IsDead () {
        return this.isDead;
    }
}
