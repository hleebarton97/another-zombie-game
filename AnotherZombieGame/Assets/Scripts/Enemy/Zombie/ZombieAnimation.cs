// Novasloth Games LLC
// Lee Barton
public abstract class ZombieAnimation : EnemyAnimation {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private ZombieAttack _ZombieAttack;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        SetupReferences();
    }

    protected virtual void Start () {
        CheckIdle();
        SetAnimationAndNavSpeed();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        base.SetupReferences();
        _ZombieAttack = GetComponentInParent<ZombieAttack>();
    }

    public virtual void SetIsAttacking (bool isAttacking) {
        animator.SetBool(MyAnimation.IS_ATTACKING, isAttacking);
    }

    protected virtual void CheckIdle () {
        bool hasTarget = _EnemyMovement.HasTarget;
        animator.SetBool(MyAnimation.HAS_TARGET, hasTarget);
    }

    protected abstract void SetAnimationAndNavSpeed ();

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void OnAttackingAnimationEnd () {
        SetIsAttacking(false);
    }

    public void OnAttackingAnimationHit () {
        _ZombieAttack.DoAttack();
    }
}
