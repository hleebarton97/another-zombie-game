﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class WalkerAnimation : ZombieAnimation {

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    public static float WALK_SPEED = 0.9f;
    public static float WALK_SLOW_SPEED = 0.4f;

    public static float WALK_SLOW_CHANCE = 0.3f; // 30%

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Start () {
        base.Start();
        this.SetAnimationAndNavSpeed();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetAnimationAndNavSpeed () {
        bool isSlow = Util.GetChance(WALK_SLOW_CHANCE);
        this.animator.SetBool(MyAnimation.IS_SLOW, isSlow);
        this._EnemyMovement.SetNavMeshSpeed(isSlow ? WALK_SLOW_SPEED : WALK_SPEED);
    }
}
