using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class ZombieAttack : EnemyAttack, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private ZombieAnimation _Animation;
    private EnemyHealth _Health;
    private IAttackable _AttackableObject;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        RegisterEventHandlers();
        SetupReferences();
        Init();
    }

    protected override void Start () {
        attackTimer.Start();
    }

    protected override void Update () {
        attackTimer.Tick();
        CheckAttack();
    }

    private void OnDestroy () {
        UnregisterEventHandlers();
    }

    protected override void OnTriggerEnter (Collider other) {
        if (other.tag == tag) return;

        IAttackable attackable = other.GetComponent<IAttackable>();
        if (!Util.IsNull(attackable)) {
            inRangeToAtack = true;
            _AttackableObject = attackable;
        }
    }

    protected override void OnTriggerExit (Collider other) {
        if (other.tag == tag) return;

        if (Util.IsEqual(_AttackableObject?.GetGameObject(), other.gameObject)) {
            inRangeToAtack = false;
            _AttackableObject = null;
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        _Health = GetComponent<EnemyHealth>();
        _Animation = GetComponent<ZombieAnimation>();

        if (Util.IsNull(_Animation)) {
            _Animation = GetComponentInChildren<ZombieAnimation>();
        }
    }

    protected override void Init () {
        attackTimer = new Novasloth.Timer(attackDelay, () => {
            timeToAttack = true;
        });
    }

    protected override void CheckAttack () {
        bool stillAlive = !_Health.IsDead();
        if (timeToAttack && inRangeToAtack && stillAlive) {
            _Animation.SetIsAttacking(true);
            ResetTimer();
        }
    }

    public override void DoAttack () {
        bool stillAlive = !_Health.IsDead();

        if (inRangeToAtack && stillAlive) {
            _AttackableObject?.Attacked(attackDamage);
        }
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        PerkAbilityEventHandler.PerkAbilityCamoDeactivatedEvent +=
                OnPerkAbilityCamoDeactivated;
    }

    public void UnregisterEventHandlers () {
        PerkAbilityEventHandler.PerkAbilityCamoDeactivatedEvent -=
                OnPerkAbilityCamoDeactivated;
    }

    private void OnPerkAbilityCamoDeactivated (Transform baitTransform) {
        if (Util.IsEqual(_AttackableObject?.GetGameObject(), baitTransform.gameObject)) {
            inRangeToAtack = false;
            _AttackableObject = null;
        }
    }
}
