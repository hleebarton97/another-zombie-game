﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public abstract class EnemyAnimation : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    protected Animator animator;
    protected EnemyMovement _EnemyMovement;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected abstract void Awake ();

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void SetupReferences () {
        animator = GetComponentInChildren<Animator>();
        _EnemyMovement = GetComponent<EnemyMovement>();

        if (Util.IsNull(_EnemyMovement)) {
            _EnemyMovement = GetComponentInParent<EnemyMovement>();
        }
    }
}
