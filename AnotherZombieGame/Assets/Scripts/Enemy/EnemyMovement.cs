﻿using UnityEngine;
using UnityEngine.AI;

// Novasloth Games LLC
// Lee Barton
public class EnemyMovement : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool HasTarget { get { return hasTarget; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    protected NavMeshAgent navAgent;
    protected Transform playerTransform;

    protected EnemyHealth _EnemyHealth;

    protected bool hasTarget;
    protected Transform targetTransform = null;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void Awake () {
        RegisterEventHandlers();
        SetupReferences();
        Init();
    }

    // Override if you don't want enemy to move to player on spawn
    // @TODO - I can move this to another method that update calls and that can be overriden.
    protected virtual void Update () {
        if (!IsDead() && NavAgentNotNullAndEnabled()) {

            if (!Util.IsNull(targetTransform)) {
                navAgent.SetDestination(targetTransform.position);
                return;
            }
            
            navAgent.enabled = false;
            hasTarget = false;
            Debug.LogWarning("EnemyMovement :: Update() :: targetObject is NULL -> disabling enemy navAgent!");
        }
    }

    protected void OnDestroy () {
        UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void SetupReferences () {
        navAgent = GetComponent<NavMeshAgent>();
        _EnemyHealth = GetComponent<EnemyHealth>();

        if (Util.IsNull(navAgent)) {
            navAgent = GetComponentInChildren<NavMeshAgent>();
        }

        if (Util.IsNull(navAgent)) {
            Debug.LogWarning($"INFO :: EnemyMovement {gameObject.name} :: NavAgent is NULL!");
        }

        GameObject player = Util.GetPlayerByTag();
        if (!Util.IsNull(player)) {
            playerTransform = player.transform;
        }
        else {
            hasTarget = false;
            Debug.LogWarning("Player is not in scene!");
        }
    }

    protected virtual void Init () {
        hasTarget = true;

        if (!Util.IsNull(playerTransform)) {
            targetTransform = playerTransform;
        }
    }

    private bool IsDead () {
        if (!Util.IsNull(_EnemyHealth)) {
            return _EnemyHealth.IsDead();
        }

        return false;
    }

    protected virtual bool NavAgentNotNullAndEnabled () {
        if (!Util.IsNull(navAgent)) {
            return navAgent.enabled;
        }

        return false;
    }

    // @TODO refactor
    public void DisableNavAgent () {
        navAgent.enabled = false;
    }

    public void EnableNavAgent () {
        navAgent.enabled = true;
    }

    public void ToggleNavAgent (bool toggle) {
        navAgent.enabled = toggle;
    }

    public void SetNavMeshSpeed (float speed) {
        navAgent.speed = speed;
    }

    public bool HasNavMeshAgent () {
        return !Util.IsNull(navAgent);
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public virtual void RegisterEventHandlers () {
        PerkAbilityEventHandler.PerkAbilityCamoActivatedEvent +=
            OnPerkAbilityCamoActivatedEvent;
        PerkAbilityEventHandler.PerkAbilityCamoDeactivatedEvent +=
            OnPerkAbilityCamoDeactivatedEvent;
    }

    public virtual void UnregisterEventHandlers () {
        PerkAbilityEventHandler.PerkAbilityCamoActivatedEvent -=
            OnPerkAbilityCamoActivatedEvent;
        PerkAbilityEventHandler.PerkAbilityCamoDeactivatedEvent -=
            OnPerkAbilityCamoDeactivatedEvent;
    }

    protected virtual void OnPerkAbilityCamoActivatedEvent (Transform baitTransform) {
        targetTransform = baitTransform;
    }

    protected virtual void OnPerkAbilityCamoDeactivatedEvent (Transform baitTransform) {
        targetTransform = playerTransform ?? null;
    }
}
