﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public static class Enemy {

    /////////////////////////////////////////////////////////////////
    // E N U M
    /////////////////////////////////////////////////////////////////

    public enum Zombie {
        WALKER,
        RUNNER,
        RANDOM
    }

    public enum Boss {
        ZOMBIE,
        SPIDER,
        RANDOM
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Check if provided enemy health is below or equal to 0.
    public static bool IsDead (float health) {
        return (health <= 0);
    }

    // Calculate the amount of damage to deal to the enemy.
    public static float CalculateDamage (float damage, float multiplier) {
        return (multiplier * damage);
    }

    // Calculate the amount of damage from an explosion to deal to the enemy
    // @TODO: This does not work - adding health to enemies when exposion occurs ON the zombies.
    public static float CalculateExplosionDamage (float damage, float multiplier, float distanceFromExplosion, float radius) {
        float adjustedMultiplier = ((-1 / (radius / 2)) * distanceFromExplosion) + 2;

        if (distanceFromExplosion <= (radius / 2)) {
            adjustedMultiplier = 1.0f;
        }

        float adjustedDamage = CalculateDamage(damage, (multiplier * adjustedMultiplier));

        Debug.Log($"Enemy :: CalculateExplosionDamage :: multiplier:{multiplier}, damage:{damage}, adjustedMultiplier:{adjustedMultiplier}, adjustedDamage:{adjustedDamage}, radius:{radius}, distanceFrom:{distanceFromExplosion}");

        return adjustedDamage;
    }

    // Calculate enemy health after taking damage.
    public static float HealthAfterDamage (float health, float damage, float multiplier) {
        return HealthAfterDamage(health, CalculateDamage(damage, multiplier));
    }

    public static float HealthAfterDamage (float health, float totalDamage) {
        return (health - totalDamage);
    }

    // Calculate rotation for enemy to rotate towards the player.
    public static Quaternion LookAtPlayer (Vector3 position) {
        Transform playerTransform = Util.GetPlayerByTag().transform;
        Vector3 lookVector = playerTransform.position - position;
        lookVector.y = position.y;
        return Quaternion.LookRotation(lookVector);
    }
}
