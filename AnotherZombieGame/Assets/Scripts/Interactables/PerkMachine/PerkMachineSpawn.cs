using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class PerkMachineSpawn : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject spawnDirtPE;
    
    private Animator animator;
    private ParticleSystem spawnDirtPS;
    private PerkMachine _PerkMachine;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
         this.SetupReferences();
    }

    private void Start () {
        this.TriggerSpawnStart();
    }

    private void LateUpdate () {
        if (!Util.IsNull(this.spawnDirtPE)) {
            if (this.spawnDirtPS.isStopped) {
                Destroy(this.spawnDirtPE);
                this.TogglePerkMachine(true);
            }
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        this.animator = GetComponent<Animator>();
        this.spawnDirtPS = this.spawnDirtPE.GetComponent<ParticleSystem>();

        this._PerkMachine = this.GetComponent<PerkMachine>();
    }

    // Move perk machine from under ground, play particle effects
    private void TriggerSpawnStart () {
        this.spawnDirtPE.transform.parent = null;
        this.TogglePerkMachine(false);
        this.animator.SetTrigger(MyAnimation.SPAWN_START);
    }

    public void TogglePerkMachine (bool toggle) {
        // Debug.Log("Perk Machine: " + (toggle ? "Enabled" : "Disabled"));
        this._PerkMachine.enabled = toggle;
    }
}
