using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class PerkMachineDestruct : Destruct {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject[] ObjectsToHide;

    private PerkMachine _PerkMachine;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.SetupReferences();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        this._PerkMachine = this.GetComponent<PerkMachine>();
    }

    public override void DoBreak () {
        foreach (GameObject obj in ObjectsToHide) {
            obj.SetActive(false);
        }

        Debug.Log("Disabling Perk Machine");
        this._PerkMachine.enabled = false;

        base.DoBreak();
    }

}
