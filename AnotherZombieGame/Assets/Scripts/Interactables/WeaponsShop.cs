﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// Novasloth Games LLC
// Lee Barton
public class WeaponsShop : Interactable, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private const int BUY_WEAPON_INDEX = 0;
    private const int MATERIAL_LIST_COUNT = 3;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Weapons Shop Crate")]
    [SerializeField] private float openSpeed = 2.5f;
    [SerializeField] private float openRotationSpeed = 2.5f;
    [SerializeField] private float weaponMoveSpeed = 2.5f;
    #pragma warning disable 0649
    [SerializeField] private Transform crateTop;
    [SerializeField] private Transform crateTopTarget;

    [Header("UI")]
    [SerializeField] private GameObject priceParent;
    [SerializeField] private GameObject stateParent;
    [SerializeField] private TextMeshProUGUI priceText;
    [SerializeField] private GameObject buyButton;
    [SerializeField] private GameObject upgradeUIContainer;
    [SerializeField] private Button[] upgradeButtons;
    [SerializeField] private TextMeshProUGUI[] upgradePriceTexts;
    [SerializeField] private Color upgradeImageColorLocked;
    [SerializeField] private Color upgradeImageColorPurchasable;
    [SerializeField] private Color upgradeImageColorPurchased;

    [Header("Weapons Display")]
    [SerializeField] private Transform displayTransform;
    [SerializeField] private Transform weaponModels;
    #pragma warning disable 0649
    [SerializeField] private List<Material> lockedMaterials;
    [SerializeField] private List<Material> purchasableMaterials;
    [SerializeField] private List<Material> purchasedMaterials;

    [Header("Weapon Upgrades")]
    [SerializeField] private List<Sprite> upgradeIcons;
    private Dictionary<WeaponUpgrade.Enum, Sprite> upgradeIconLookup = new Dictionary<WeaponUpgrade.Enum, Sprite>();

    private Dictionary<Purchasing.State, List<Material>> materialListLookup = new Dictionary<Purchasing.State, List<Material>>();
    private Dictionary<Weapons.Enum, GameObject> weaponModelsLookup = new Dictionary<Weapons.Enum, GameObject>();
    private Transform viewingWeaponTransform;

    private int prevViewingWeaponIndex;
    private int viewingWeaponIndex;

    private Vector3 crateTopClosedPosition;
    private Quaternion crateTopClosedRotation;

    private WeaponsPurchasing _WeaponsPurchasing;
    private WeaponsUpgradePurchasing _UpgradePurchasing;

    private int setupCompleteCount = 0;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        base.Awake();
        this.RegisterEventHandlers();
        this.SetupReferences();
        this.Init();
    }

    protected override void Update () {
        base.Update();

        if (this.IsInteracting) {
            this.DisplayWeapons();
            this.ListenForSelection();
        } else {
            this.HideWeapons();
        }
    }

    private void LateUpdate () {
        if (this.IsInteracting) {
            this.OpenCrate();
        } else {
            this.CloseCrate();
        }
    }

    private void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.crateTopClosedPosition = this.crateTop.position;
        this.crateTopClosedRotation = this.crateTop.rotation;

        this.weaponModels.gameObject.SetActive(false);
        this.displayTransform.gameObject.SetActive(false);

        // Initialize dictionary
        for (int i = 0; i < this.weaponModels.childCount; i++) {
            this.weaponModelsLookup.Add(
                this.GetWeaponEnumFromIndex(i),
                this.weaponModels.GetChild(i).gameObject
            );
        }

        this.materialListLookup.Add(Purchasing.State.LOCKED, this.lockedMaterials);
        this.materialListLookup.Add(Purchasing.State.PURCHASABLE, this.purchasableMaterials);
        this.materialListLookup.Add(Purchasing.State.PURCHASED, this.purchasedMaterials);

        for (int i = 0; i < this.upgradeIcons.Count; i++) {
            this.upgradeIconLookup.Add(
                (WeaponUpgrade.Enum)(i),
                this.upgradeIcons[i]
            );
        }
    }

    private void SetupReferences () {
        GameObject player = Util.GetPlayerByTag();
        this._WeaponsPurchasing = player.GetComponentInChildren<WeaponsPurchasing>();
        this._UpgradePurchasing = player.GetComponentInChildren<WeaponsUpgradePurchasing>();
    }

    private void ListenForSelection () {
        float delta = Input.GetAxis("Mouse ScrollWheel");
        bool scrolledUp = delta > 0f;
        bool scrolledDown = delta < 0f;

        if (scrolledUp) { // Scroll up
            this.DecreaseWeaponIndex();
        } else if (scrolledDown) { // Scroll down
            this.IncreaseWeaponIndex();
        }
    }

    private Weapons.Enum GetWeaponEnumFromIndex (int index) {
        return (Weapons.Enum)(index);
    }

    private void IncreaseWeaponIndex () {
        if (Weapons.IndexLessThanLastPurchasableWeapon(this.viewingWeaponIndex + 1)) {
            this.prevViewingWeaponIndex = this.viewingWeaponIndex;
            this.viewingWeaponIndex++;
            this.SetViewWeapon();
        }
    }

    private void DecreaseWeaponIndex () {
        if (Weapons.IndexGreaterThanFirstPurchasableWeapon(this.viewingWeaponIndex + 1)) {
            this.prevViewingWeaponIndex = this.viewingWeaponIndex;
            this.viewingWeaponIndex--;
            this.SetViewWeapon();
        }
    }

    private void SetViewWeapon () {
        Transform buyWeaponParent = this.displayTransform.GetChild(BUY_WEAPON_INDEX);
        Transform currentBuyWeapon = Util.TryAndGetFirstChild(buyWeaponParent);

        // Cleanup displayed weapons
        if (!Util.IsNull(currentBuyWeapon)) {
            currentBuyWeapon.SetParent(this.weaponModels);
            currentBuyWeapon.SetSiblingIndex(this.prevViewingWeaponIndex);
        }

        Vector3 prevPosition = Vector3.zero;
        Weapons.Enum currentWeapon = this.GetWeaponEnumFromIndex(this.viewingWeaponIndex);
        bool weaponExist = !Util.IsNull(this.viewingWeaponTransform);
        if (weaponExist) { prevPosition = this.viewingWeaponTransform.position; }
        this.viewingWeaponTransform = this.weaponModelsLookup[currentWeapon].transform;
        if (weaponExist) { this.viewingWeaponTransform.position = prevPosition; }
        this.viewingWeaponTransform.SetParent(buyWeaponParent);

        this.SetText(currentWeapon);
    }

    private void SetText (Weapons.Enum weapon) {
        if (this._WeaponsPurchasing.IsWeaponPurchased(weapon)) {
            // Weapon price text, buy button
            this.stateParent.SetActive(false);
            this.priceParent.SetActive(false);
            this.buyButton.SetActive(false);

            // Weapon upgrades
            this.ShowWeaponUpgrades(weapon);
        }
        else if (this._WeaponsPurchasing.IsWeaponPurchasable(weapon)) {
            // Weapon price text, buy button
            this.stateParent.SetActive(false);
            this.priceText.text = Weapons.GetWeaponPrice(weapon) + "";
            this.priceParent.SetActive(true);
            this.buyButton.SetActive(true);

            // Weapon upgrades
            this.HideWeaponUpgrades();
        }
        else {
            // Weapon price text, buy button
            this.priceParent.SetActive(false);
            this.buyButton.SetActive(false);
            this.stateParent.SetActive(true);

            // Weapon upgrades
            this.HideWeaponUpgrades();
        }
    }

    private void ShowWeaponUpgrades (Weapons.Enum weapon) {
        List<WeaponUpgrade.Enum> upgrades = this._UpgradePurchasing.GetWeaponUpgradeTypes(weapon);

        Button button;
        Image image;
        TextMeshProUGUI upgradePrice;
        WeaponUpgrade.Enum upgrade;
        for (int i = 0; i < upgrades.Count; i++) {
            button = this.upgradeButtons[i];
            image = button.image;
            upgradePrice = this.upgradePriceTexts[i];
            upgrade = upgrades[i];

            bool isLocked = this._UpgradePurchasing.IsUpgradeLocked(weapon, upgrade);
            bool isPurchasable = this._UpgradePurchasing.IsUpgradePurchasable(weapon, upgrade);

            image.color = this.upgradeImageColorPurchased;
            button.interactable = true;
            upgradePrice.text = "";
            upgradePrice.gameObject.SetActive(false);
            if (isLocked) {
                image.color = this.upgradeImageColorLocked;
                button.interactable = false;
            } else if (isPurchasable) {
                image.color = this.upgradeImageColorPurchasable;
                upgradePrice.text = this._UpgradePurchasing.GetWeaponUpgradePrice(weapon, upgrade) + "";
                upgradePrice.gameObject.SetActive(true);
            }

            image.sprite = this.upgradeIconLookup[upgrade];
        }

        this.upgradeUIContainer.SetActive(true);
    }

    private void HideWeaponUpgrades () {
        this.upgradeUIContainer.SetActive(false);
    }

    private void DisplayWeapons () {
        if (!this.displayTransform.gameObject.activeInHierarchy) {
            this.displayTransform.gameObject.SetActive(true);
        }

        if (this.displayTransform.gameObject.activeInHierarchy) {
            if (this.viewingWeaponTransform != null) {
                this.MoveDisplayedWeaponsToShow();
            }
        }
    }

    private void HideWeapons () {
        if (this.displayTransform.gameObject.activeInHierarchy) {
            this.displayTransform.gameObject.SetActive(false);
        }

        if (!this.displayTransform.gameObject.activeInHierarchy) {
            if (this.viewingWeaponTransform != null) {
                this.MoveDisplayedWeaponsToHide();
            }
        }
    }

    /////////////////////////////////////////////////////////////////
    // P R O C E D U R A L   A N I M A T I O N
    /////////////////////////////////////////////////////////////////

    // Move crate's top to target transform position and rotation.
    private void OpenCrate () {
        crateTop.position = Vector3.Lerp(
                crateTop.position,
                this.crateTopTarget.position,
                (Time.deltaTime * this.openSpeed)
            );

        crateTop.rotation = Quaternion.Lerp(
            crateTop.rotation,
            this.crateTopTarget.rotation,
            (Time.deltaTime * this.openRotationSpeed)
        );
    }

    // Move crate's top to original position and rotation.
    private void CloseCrate () {
        crateTop.position = Vector3.Lerp(
                crateTop.position,
                this.crateTopClosedPosition,
                (Time.deltaTime * this.openSpeed)
            );

        crateTop.rotation = Quaternion.Lerp(
            crateTop.rotation,
            this.crateTopClosedRotation,
            (Time.deltaTime * this.openRotationSpeed)
        );
    }

    private void MoveDisplayedWeaponsToShow () {
        this.viewingWeaponTransform.position = Vector3.Lerp(
            this.viewingWeaponTransform.position,
            this.displayTransform.GetChild(BUY_WEAPON_INDEX).position,
            (Time.deltaTime * this.weaponMoveSpeed)
        );
    }

    private void MoveDisplayedWeaponsToHide () {
        this.viewingWeaponTransform.position = Vector3.Lerp(
            this.viewingWeaponTransform.position,
            this.displayTransform.position,
            (Time.deltaTime * this.weaponMoveSpeed)
        );
    }

    private bool SetupIsComplete () => this.setupCompleteCount > 1;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        // Debug.Log("INFO :: WeaponsShop :: RegisterEventHandlers()");
        PurchasingEventHandler.WeaponPurchasingStateUpdatedEvent +=
            this.OnWeaponPurchasingStateUpdated;
        PurchasingEventHandler.UpgradePurchasingStateUpdatedEvent +=
            this.OnUpgradePurchasingStateUpdated;
        PurchasingEventHandler.PurchasingSetupCompleteEvent +=
            this.OnPurchasingSetupCompleteEvent;
    }

    public void UnregisterEventHandlers () {
        // Debug.Log("INFO :: WeaponsShop :: UnregisterEventHandlers()");
        PurchasingEventHandler.WeaponPurchasingStateUpdatedEvent -=
            this.OnWeaponPurchasingStateUpdated;
        PurchasingEventHandler.UpgradePurchasingStateUpdatedEvent -=
            this.OnUpgradePurchasingStateUpdated;
        PurchasingEventHandler.PurchasingSetupCompleteEvent -=
            this.OnPurchasingSetupCompleteEvent;
    }

    private void OnPurchasingSetupCompleteEvent () {
        this.setupCompleteCount++;
        // Debug.Log($"INFO :: WeaponsShop :: OnPurchasingSetupCompleteEvent -> Update setupCompleteCount:{this.setupCompleteCount}");

        if (this.SetupIsComplete()) {
            // Debug.Log("INFO :: WeaponsPurchasing :: OnPurchasingSetupCompleteEvent -> Setup Completed... -> SetViewWeapon()");
            this.SetViewWeapon();
        }
    }

    private void OnWeaponPurchasingStateUpdated (Weapons.Enum weapon, Purchasing.State state) {
        GameObject weaponObject = this.weaponModelsLookup[weapon];
        Renderer renderer = weaponObject.GetComponentInChildren<Renderer>();

        if(!Util.IsNull(renderer)) {
            List<Material> materials = this.materialListLookup[state];
            if (state == Purchasing.State.PURCHASED) {
                renderer.material = materials[(int)weapon];
            } else {
                renderer.material = materials[0];
            }
        }

        if (weapon == this.GetWeaponEnumFromIndex(this.viewingWeaponIndex)) {
            this.SetText(weapon);
        }
    }

    private void OnUpgradePurchasingStateUpdated (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade, Purchasing.State state) {
        if (weapon == this.GetWeaponEnumFromIndex(this.viewingWeaponIndex) && this.SetupIsComplete()) {
            this.ShowWeaponUpgrades(weapon);
        }
    }

    public void OnBuyButtonClick () {
        Weapons.Enum weapon = this.GetWeaponEnumFromIndex(this.viewingWeaponIndex);
        this._WeaponsPurchasing.PurchaseWeapon(weapon);
        InteractableEventHandler.PlayerInteracting(this.IsInteracting);
        this.CursorReset();
    }

    public void OnBuyUpgradeButtonClick (int index) {
        Weapons.Enum weapon = this.GetWeaponEnumFromIndex(this.viewingWeaponIndex);
        WeaponUpgrade.Enum upgrade = this._UpgradePurchasing.GetWeaponUpgradeTypes(weapon)[index];

        this._UpgradePurchasing.PurchaseUpgrade(weapon, upgrade);
    }
}
