﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class Interactable : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    private const KeyCode INTERACT_KEY = KeyCode.E;

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool IsInteracting { get; set; }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Interactable")]
    [SerializeField] protected bool doCameraZoom = true;
    [SerializeField] protected bool doCursorReset = true;

    [SerializeField] protected GameObject interactableUI;

    [SerializeField] protected Transform cameraZoomTarget;

    private bool canInteract = false;

    private GameObject _Player;
    private Rigidbody _PlayerRigidbody;
    private WeaponCursor _WeaponCursor;
    private CameraInteraction _CameraInteraction;

    private RigidbodyConstraints interactingConstraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    private RigidbodyConstraints defaultConstraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void Awake () {
        this.Init();
        this.SetupReferences();
    }

    protected virtual void Update () {
        this.ListenForKeyPress();
    }

    protected virtual void OnTriggerEnter (Collider collider) {
        if (Util.IsPlayer(collider)) {
            this.canInteract = true;
            this.ToggleText(true);
        }
    }

    protected virtual void OnTriggerExit (Collider collider) {
        if (Util.IsPlayer(collider)) {
            this.canInteract = false;

            if (this.IsInteracting) {
                this.ToggleInteraction();
            }
            this.ToggleText(false);
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.interactableUI.SetActive(false);
    }

    private void SetupReferences () {
        this._Player = Util.GetPlayerByTag();
        this._CameraInteraction = Util.GetMainCameraByTag().GetComponent<CameraInteraction>();
        this._PlayerRigidbody = this._Player.GetComponent<Rigidbody>();
    }

    private void ListenForKeyPress () {
        if (Input.GetKeyDown(INTERACT_KEY) && this.canInteract) {
            this.ToggleInteraction();
        }
    }

    protected virtual void ToggleInteraction () {
        this.IsInteracting = !this.IsInteracting;

        InteractableEventHandler.PlayerInteracting(this.IsInteracting);

        this.ToggleText(!this.IsInteracting);
        this.CursorReset();
        this.CameraZoom();
        this.LockPlayerPosition();
    }

    protected void CursorReset () {
        if (this.doCursorReset) {
            _WeaponCursor = this.GetCurrentWeaponCursor();
            if (this.IsInteracting) {
                this._WeaponCursor.ResetCursor();
            } else {
                this._WeaponCursor.SetCrosshair();
            }
        }
    }

    protected void CameraZoom () {
        if (this.doCameraZoom) {
            if (this.IsInteracting) {
                this._CameraInteraction.DoCameraZoomIn(this.gameObject, this.cameraZoomTarget);
            } else {
                this._CameraInteraction.ResetCamera();
            }
        }
    }

    protected void LockPlayerPosition () {
        this._PlayerRigidbody.constraints = (this.IsInteracting)
            ? this.interactingConstraints
            : this.defaultConstraints;
    }

    protected void ToggleText (bool showText) {
        this.interactableUI.SetActive(showText);
    }

    private WeaponCursor GetCurrentWeaponCursor () {
        return this._Player.GetComponentInChildren<WeaponCursor>();
    }
}
