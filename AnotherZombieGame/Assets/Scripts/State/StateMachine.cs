using System;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
// Credit: Jason Weimann - https://www.youtube.com/watch?v=V75hgcsCGOM&t=1105s
public class StateMachine {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private IState currentState;

    private Dictionary<Type, List<Transition>> transitions = new Dictionary<Type, List<Transition>>();

    // Transitions for the current state
    private List<Transition> currentTransitions = new List<Transition>();

    // Transitions coming from any state (do not have a 'from' state)
    private List<Transition> anyTransitions = new List<Transition>();

    private static List<Transition> EmptyTransitions = new List<Transition>(0);

    /////////////////////////////////////////////////////////////////
    // S T A T E
    /////////////////////////////////////////////////////////////////

    public void Tick () {
        Transition transition = this.GetTransition();

        if (!Util.IsNull(transition)) {
            Debug.Log($"INFO :: StateMachine :: Transition state from: {this.currentState.ToString()}, to: {transition.To.ToString()}");
            this.SetState(transition.To);
        }

        this.currentState?.Tick();
    }

    public void SetState (IState state) {
        if (state != this.currentState) {
            this.currentState?.OnExit(); // Do cleanup if necessary
            this.currentState = state;

            this.transitions.TryGetValue(
                this.currentState.GetType(),
                out this.currentTransitions
            );

            if (Util.IsNull(this.currentTransitions)) { // Don't allocate extra memory
                this.currentTransitions = EmptyTransitions;
            }

            this.currentState.OnEnter(); // Do setup stuffs
        }
    }

    /////////////////////////////////////////////////////////////////
    // T R A N S I T I O N
    /////////////////////////////////////////////////////////////////

    public void AddTransition (IState from, IState to, Func<bool> predicate) {
        if (!this.transitions.TryGetValue(from.GetType(), out List<Transition> fromTransitions)) {
            fromTransitions = new List<Transition>();
            this.transitions[from.GetType()] = fromTransitions;
        }

        fromTransitions.Add(new Transition(to, predicate));
    }

    public void AddAnyTransition (IState state, Func<bool> predicate) {
        this.anyTransitions.Add(new Transition(state, predicate));
    }

    private Transition GetTransition () {
        // Transitions without 'from' state (coming from any state)
        foreach (Transition transition in this.anyTransitions) {
            if (transition.Condition()) {
                return transition;
            }
        }

        // Transitions for the current state
        foreach (Transition transition in this.currentTransitions) {
            if (transition.Condition()) {
                return transition;
            }
        }

        return null;
    }

    private class Transition {
        public Func<bool> Condition { get; }
        public IState To { get; }

        public Transition (IState to, Func<bool> condition) {
            this.To = to;
            this.Condition = condition;
        }
    }
}
