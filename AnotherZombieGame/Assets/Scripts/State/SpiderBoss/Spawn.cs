using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton

namespace Novasloth.SpiderBoss {
    public class Spawn : IState {

        /////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        /////////////////////////////////////////////////////////////////

        private readonly SpiderBossAI _SpiderBossAI;

        /////////////////////////////////////////////////////////////////
        // C O N S T R U C T O R
        /////////////////////////////////////////////////////////////////

        public Spawn (SpiderBossAI spiderBossAI) {
            this._SpiderBossAI = spiderBossAI;
        }

        /////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        public void OnEnter () {
            this._SpiderBossAI.StartSpawn();
        }

        public void Tick () { }

        public void OnExit () {
            this._SpiderBossAI.StopSpawn();
        }
    }
}
