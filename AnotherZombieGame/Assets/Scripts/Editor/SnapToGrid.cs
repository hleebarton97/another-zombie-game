using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class SnapToGrid : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Vector3 gridSize = new Vector3(1.6f, 0, 1.6f);

    private bool disabled = false;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void OnDrawGizmos () {
        this.CheckDisableSnap();
        if (!Application.isPlaying && !this.disabled) {
            this.DoSnap();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void DoSnap () {
        // Avoid dividing by 0
        float x = (this.gridSize.x == 0 ? this.transform.position.x : (this.transform.position.x / this.gridSize.x));
        float y = (this.gridSize.y == 0 ? this.transform.position.y : (this.transform.position.y / this.gridSize.y));
        float z = (this.gridSize.z == 0 ? this.transform.position.z : (this.transform.position.z / this.gridSize.z));

        Vector3 snapPosition = new Vector3(
            Mathf.Round(x) * this.gridSize.x,
            Mathf.Round(y) * this.gridSize.y,
            Mathf.Round(z) * this.gridSize.z
        );

        this.transform.position = snapPosition;
    }

    private void CheckDisableSnap () {
        string parentTag = "";
        string parentsParentTag = "";

        if (!Util.IsNull(this.transform.parent)) {
            parentTag = this.transform.parent.tag;
            if (!Util.IsNull(this.transform.parent.transform.parent)) {
                parentsParentTag = this.transform.parent.transform.parent.tag;
            }
        }

        this.disabled = (parentTag == Tag.NO_SNAP || parentsParentTag == Tag.NO_SNAP);
    }
}
