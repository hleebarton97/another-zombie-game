using UnityEngine;
using UnityEditor;
using Novasloth.SpiderBoss;

// Lee Barton
[CustomEditor(typeof(SpiderBossAI))]
public class SpiderBossEditor : Editor {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public override void OnInspectorGUI () {
        this.DrawDefaultInspector();

        SpiderBossAI _SpiderBossAI = (SpiderBossAI)this.target;
        //if (GUILayout.Button("LookAt Player")) {
        //    _SpiderBossAI.LookAtPlayer(true);
        //}

        //if (GUILayout.Button("Stop LookAt Player")) {
        //    _SpiderBossAI.LookAtPlayer(false);
        //}

        //if (GUILayout.Button("Attack Player")) {
        //    _SpiderBossAI.AttackPlayer(true);
        //}

        //if (GUILayout.Button("Stop Attacking Player")) {
        //    _SpiderBossAI.AttackPlayer(false);
        //}

        //if (GUILayout.Button("Retreat")) {
        //    _SpiderBossAI.Retreate(true);
        //}

        //if (GUILayout.Button("Stop Retreat")) {
        //    _SpiderBossAI.Retreate(false);
        //}
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

}
